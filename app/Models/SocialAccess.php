<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialAccess extends Model
{
    use HasFactory;
    protected $primaryKey = 'access_id';
    protected $table = 'user_social_access';
      
}