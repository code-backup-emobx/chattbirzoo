<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Beacon extends Model
{
    use HasFactory;
    protected $primaryKey = 'beacon_id';
    protected $table = 'beacons';

    public function getSectionMedia(){

        return $this->hasMany('App\Models\SectionMedia','section_id');
    }
    
    public function getAnimals(){

    $a=$this->hasMany('App\Models\Animals','beacon_id');
    if(request()->login_token){
         $a=$a->where('status','active');
    }
    return $a;
    }

}