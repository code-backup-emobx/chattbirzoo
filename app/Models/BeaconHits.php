<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BeaconHits extends Model
{
    // use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'beacon_hits';

    public function get_user(){

    	return $this->belongsTo(User::class,'user_id','id');
    }
    public function get_beacon(){

    	return $this->belongsTo(Beacon::class,'beacon_id');
    }
      
}