<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FeedbackComment extends Model
{
    use SoftDeletes;
    protected $table = 'user_feedback_comments';

     public function getUser(){ return $this->hasOne('App\Models\User','id','user_id');} 

    public function getUserFeedback(){ return $this->hasMany('App\Models\Feedback','feedback_comment_id','id');}
   
    
      
}