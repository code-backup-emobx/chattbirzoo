<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class UserRecentFeedback extends Model
{
    use SoftDeletes;
       protected $table = 'user_recent_feedback';

     public function getUser(){ return $this->hasOne('App\Models\User','id','user_id');}
     public function getFeedbackComment(){ return $this->hasMany('App\Models\FeedbackComment','recent_feedback_id','id');}
      
}