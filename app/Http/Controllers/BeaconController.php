<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Beacon;
use App\Models\Section;
use App\Models\SectionMedia;
use App\Models\BeaconHits;
use DB;
use Validator;
class BeaconController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {  
        $response['lists']=Beacon::orderBy('created_at', 'DESC')->paginate(15);
	   
            return view('beacons.list',$response);
        
    }

    public function view(Request $request , $id)
    {   
        $response['details']=Beacon::where('beacon_id',$id)->first();
        
        return view('beacons.view',$response);
        
    }

     public function update(Request $request)
    {   
       $validator = Validator::make($request->all(), [
                    
            'name' => 'required',
           // 'major_id' => 'required|integer|gt:0',
           
           // 'minor_id' => 'required|integer|gt:0',
           'unique_id' => 'required|alpha_num',
                         
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        $id=$request->input('id');
        $beacon_name=$request->input('name');
        // $major_id=$request->input('major_id');
        // $minor_id=$request->input('minor_id');
         $unique_id=$request->input('unique_id');
        
        
        $update=Beacon::where('beacon_id', $id)->first();
        $update->beacon_name=$beacon_name;
        // $update->major_id=$major_id;
        // $update->minor_id=$minor_id;
        $update->unique_id=$unique_id;

        $update->save();
        
        return redirect()->route('beacon')->with('success','Successfully updated');
        
    }



    public function create(Request $request)
    {   
        // return $request->all();
        $validator = Validator::make($request->all(), [
                    
                    'name' => 'required',
                   // 'major_id' => 'required|integer|gt:0',
                   
                   // 'minor_id' => 'required|integer|gt:0',
                   
                   'unique_id' => 'required|alpha_num',
                   
                    
                   
                    
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }

        // return $request->all();
        // return $request->media;
        $beacon_name=$request->input('name');
        // $major_id=$request->input('major_id');
        // $minor_id=$request->input('minor_id');
        $unique_id=$request->input('unique_id');
        

        $code = generateRandomString(4); 
        $create=new Beacon();
        // $create->major_id=$major_id;
        // $create->minor_id=$minor_id;
        $create->unique_id=$unique_id;
        $create->beacon_name=$beacon_name;
        $create->save();

        return back()->with('success','Beacon added Successfully');
        
    }

    public function status($id ,$status)
    {

        DB::enableQueryLog();
        $beacon=Beacon::where('beacon_id', $id)->first();
        if($status=='active')
        {
            
          $beacon->status='inactive';
        }
        else
        {
          $beacon->status='active';
        }
        $beacon->save();
       //return DB::getQueryLog();
        return redirect()->back()->with('success', 'Status Changed Successfully.');
    }

      public function beaconsHits(Request $request,$user_id=''){

       $list = BeaconHits::with('get_user','get_beacon');
       if($request->date_from){
       $list=  $list->whereDate('created_at','>=',$request->date_from);
       }
       if($request->date_to){
       $list=  $list->whereDate('created_at','<=',$request->date_to);
       }
       if($user_id){
       $list=  $list->where('user_id',$user_id);
       }  
      if($request->beacon_id){
       $list =  $list->where('beacon_id',$request->beacon_id);
       }
    
       $list=  $list->orderBy('id', 'DESC')->paginate();
       $response['list']=$list;
      
      return view('beacons.beacons_list',$response);
    } 
  public function userBeaconsHits(Request $request){

     $list = BeaconHits:: with(['get_beacon','get_user' => fn($q) => $q->withTrashed()]); 
 
      if($request->date_from){
       $list=  $list->where('created_at','>=',$request->date_from);
       }
       if($request->date_to){
       $list= $list->where('created_at','<=',$request->date_to);
       }
     
      
    
      $list=  $list->select('beacon_hits.*',DB::RAW('count(beacon_hits.id) as visit_count'),DB::RAW('count(distinct beacon_hits.beacon_id) as beacon_visit_count'),)->groupBy('beacon_hits.user_id')->orderBy('visit_count', 'DESC')->paginate();
       $response['list']=$list;
      
      return view('beacons.beacons_user_list',$response);
    }

     public function countBeaconsHits(Request $request,$user_id){

      $list = BeaconHits:: with(['get_beacon','get_user' => fn($q) => $q->withTrashed()]); 
 
      if($request->date_from){
       $list=  $list->where('created_at','>=',$request->date_from);
       }
       if($request->date_to){
       $list= $list->where('created_at','<=',$request->date_to);
       } 
       $list =  $list->select('beacon_hits.*',DB::RAW('count(beacon_hits.id) as visit_count'))->where('user_id',$user_id)->groupBy('beacon_hits.beacon_id')->orderBy('visit_count', 'DESC')->paginate();
       $response['list']=$list;
      
      return view('beacons.count_beacons_user_list',$response);
    }

//end class
}
