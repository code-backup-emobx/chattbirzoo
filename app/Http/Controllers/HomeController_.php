<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Section;
use App\Models\Animals;
use App\Models\BeaconHits;

use App\Models\Feedback;
use App\Models\FeedbackComment;
use DB;
use Cache;
use Validator;
use Carbon\Carbon;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    function dashboradData(){
    $data=array();
     // Beacon Hits 
       $beaconAoO= \App\Models\BeaconHits::query();
      
        $beaconAoO= $beaconAoO->select(
        DB::raw('count(beacon_id) as total'),DB::raw('DATE_FORMAT(created_at,"%Y-%m-%d") as modified_on' ) )->groupBy('modified_on')->orderBy('modified_on')->get();
       $data['beaconTotal']=$beaconAoO->sum('total');
       $beaconGraphData=array();
       $sum=0;
       foreach($beaconAoO as $beaconO){
        $sum=$beaconO->total;
          $beaconGraphData[date('d M Y',strtotime($beaconO->modified_on))]=$sum;
       }
       $data['beaconGraphData']=$beaconGraphData; 
    
    // visitor 
       $visitorAoO= \App\Models\User::where('role','user');
      
        $visitorAoO= $visitorAoO->select(
        DB::raw('count(id) as total'),DB::raw('DATE_FORMAT(created_at,"%Y-%m-%d") as modified_on' ) )->groupBy('modified_on')->orderBy('modified_on')->get();
       $data['visitorTotal']=$visitorAoO->sum('total');
       $visitorGraphData=array();
       $sum=0;
       foreach($visitorAoO as $visitorO){
        $sum=$sum+$visitorO->total;
          $visitorGraphData[date('d M Y',strtotime($visitorO->modified_on))]=$sum;
       }
       $data['visitorGraphData']=$visitorGraphData;
    
    // admin 
       $adminAoO= \App\Models\User::where('role','admin');
      
        $adminAoO= $adminAoO->select(
        DB::raw('count(id) as total'),DB::raw('DATE_FORMAT(created_at,"%Y-%m-%d") as modified_on' ) )->groupBy('modified_on')->orderBy('modified_on')->get();
       $data['adminTotal']=$adminAoO->sum('total');
       $adminGraphData=array();
       $sum=0;
       foreach($adminAoO as $adminO){
        $sum=$sum+$adminO->total;
          $adminGraphData[date('d M Y',strtotime($adminO->modified_on))]=$sum;
       }
       $data['adminGraphData']=$adminGraphData;
    
     // feedback 
       $feedbackAoO= \App\Models\Feedback::query();
      
        $feedbackAoO= $feedbackAoO->select(
        DB::raw('count(feedback_id) as total'),DB::raw('DATE_FORMAT(created_at,"%Y-%m-%d") as modified_on' ),DB::raw('sum(rating) as today_rating' ))->groupBy('modified_on')->orderBy('modified_on')->get();
       //$data['feedbackTotal']=$feedbackAoO->sum('total');
       $feedbackGraphData=array();
       $sum=$total_rating=$total_records=$totrate=0;
       foreach($feedbackAoO as $feedbackO){
       $total_records=$total_records+$feedbackO->total;
       $total_rating=$total_rating+$feedbackO->today_rating;
       
        $sum=round($feedbackO->today_rating/$feedbackO->total,2);
          $feedbackGraphData[date('d M Y',strtotime($feedbackO->modified_on))]=$sum;
       }
    if($total_records){
    $totrate=round($total_rating/$total_records,2);
    }
    
       $data['feedbackTotal']=$total_records;
       $data['overall_rating']=$totrate;
       $data['feedbackGraphData']=$feedbackGraphData;
       
      // FeedbackComment

    return $data;
    }
    public function index(Request $request)
    {  
    if(!$request->view11){
      //return $value = Cache::get('dashbord_data');
         DB::enableQueryLog();
     
       $cachetime=DB::table('last_changes')->where('module','dashboard');
       $getCachetime = $cachetime ->first();
       $cd=$getCachetime->last_change;
    if($request->updatedashboard){
       Cache::forget('dashbord_data');
       $dashbord_data = Cache::remember('dashbord_data',60*60, function () {
                  
                     $cd=date('Y-m-d H:i:s');
                     DB::table('last_changes')->where('module','dashboard')->update(['last_change'=>$cd]);
                   return $this->dashboradData();
                });
       $cd=date('Y-m-d H:i:s');
       DB::table('last_changes')->where('module','dashboard')->update(['last_change'=>$cd]);
    }
    else{
        $dashbord_data = Cache::remember('dashbord_data',60*60, function () {
                  
                     $cd=date('Y-m-d H:i:s');
                     DB::table('last_changes')->where('module','dashboard')->update(['last_change'=>$cd]);
                   return $this->dashboradData();
                });
    }
     
       
        
    
    $dashbord_data['cd']= \Carbon\Carbon::parse(strtotime($cd))->diffForHumans();
    

     return view('home', $dashbord_data);
   // return $dashbord_data;
   // return array('a'=> DB::getQueryLog(),'s'=>$dashbord_data,'cd'=>$cd);
    }else{
        $lists=User::whereRole('user');
        // $response['allusers_wd']=$lists_->withTrashed()->count();    
        $response['allusers']=$lists->count();
        $lists=$lists->whereStatus('active');
        $response['activeusers']=$lists->count();
        $response['lists']=$lists->get();
        $response['tendays']= $this->UserCountLastTenDays();
      // return $last_30_days = User::where('role','user')->where('created_at','>=',Carbon::now()->subdays(30))->count();
        // return $response;
       $currentdate = date('Y-m-d');
       $response['total_hit']=BeaconHits::sum('hit_count');
        $check_hit=BeaconHits::whereDate('created_at',  $currentdate)->get(['hit_count','created_at']);
       if(isset($check_hit)){
       		$check_hit[]=["hit_count"=>"0", "current_date"=> date('d M Y')];
       		$response['today_beacons_hit']=$check_hit;
       		$response['current_date']=date('d M Y');
       }else{
       $response['today_beacons_hit']=$check_hit;
       $response['current_date']=date('d M Y');
       }
    // return $response['today_beacons_hit'];
  	   // $get_seven_days = Carbon::now()->subDays(7);
  	   // $response['last_seven_days_hit'] = BeaconHits::whereDate('created_at', '>=', $get_seven_days)->get(['hit_count','created_at']);
      $response['last_seven_days_hit'] = $this->CountLastSevenDays();
      $response['four_weeks_hit'] = $this->CountLastFourWeeks();
        return view('home(1)', $response);
   }
    }

    public function UserCountLastTenDays(){
        $lastTenDates[]= $currentdate = date('Y-m-d');
        $lastTenDates[] = date('Y-m-d', strtotime("-1 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-2 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-3 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-4 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-5 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-6 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-7 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-8 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-9 day", strtotime($currentdate)));
       
        foreach($lastTenDates as $date_value ){
            $d=array();
             $d['date']=$date_value;
             $d['ucount']=User::where('role','user')->whereDate('created_at',$date_value)->count();
             
             $dd[]=$d;
        }
      return $dd;
    }
    
    public function CountLastSevenDays(){
        $lastDates[]= $currentdate = date('Y-m-d');
        $lastDates[] = date('Y-m-d', strtotime("-1 day", strtotime($currentdate)));
        $lastDates[] = date('Y-m-d', strtotime("-2 day", strtotime($currentdate)));
        $lastDates[] = date('Y-m-d', strtotime("-3 day", strtotime($currentdate)));
        $lastDates[] = date('Y-m-d', strtotime("-4 day", strtotime($currentdate)));
        $lastDates[] = date('Y-m-d', strtotime("-5 day", strtotime($currentdate)));
        $lastDates[] = date('Y-m-d', strtotime("-6 day", strtotime($currentdate)));
       
        foreach($lastDates as $date_value ){
            $d=array();
             $d['date']=$date_value;
             $get_data=BeaconHits::whereDate('created_at',$date_value)->first(['hit_count']);
             if(!empty($get_data->hit_count))
             {
             $d['count']=$get_data->hit_count;
             }else{
             $d['count']='0';
             }
             $dd[]=$d;
        }
      return $dd;
    }

     public function CountLastFourWeeks(){
        $lastTenDates[]= $currentdate = date('Y-m-d');
        $lastTenDates[] = date('Y-m-d', strtotime("-1 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-2 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-3 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-4 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-5 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-6 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-7 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-8 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-9 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-10 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-11 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-12 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-13 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-14 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-15 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-16 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-17 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-18 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-19 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-20 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-21 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-22 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-23 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-24 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-25 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-26 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-27 day", strtotime($currentdate)));
        // $lastTenDates[] = date('Y-m-d', strtotime("-9 day", strtotime($currentdate)));
       
        foreach($lastTenDates as $date_value ){
            $d=array();
             $d['date']=$date_value;
             $get_data=BeaconHits::whereDate('created_at',$date_value)->first(['hit_count']);
             if(!empty($get_data->hit_count))
             {
             $d['count']=$get_data->hit_count;
             }else{
             $d['count']='0';
             }
             $dd[]=$d;
        }
      return $dd;
    }
}
