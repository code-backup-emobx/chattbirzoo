<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Feedback;
use App\Models\UserRecentFeedback;
use App\Models\FeedbackComment;
use DB;
class FeedbackController extends Controller
{
    
    public function index(Request $request)
    {  
        $lists=Feedback::with(['getAnimal','getUser' => fn($q) => $q->withTrashed()]); 
    
       if($request->user_id){
       $lists=  $lists->where('user_id',$request->user_id);
       }
       if($request->animal_id){
       $lists=  $lists->where('animal_id',$request->animal_id);
       } 
       if($request->rating){
       $lists=  $lists->where('rating',$request->rating);
       }
       if($request->date_from){
       $lists=  $lists->where('created_at','>=',$request->date_from);
       }
       if($request->date_to){
       $lists=  $lists->where('created_at','<=',$request->date_to);
       }
     
      $lists=  $lists->orderBy('feedback_id', 'DESC')->paginate();
	    $response['lists']=$lists;
      return view('feedback.list',$response);
        
    }

    public function feedbackList(Request $request){

      $list = UserRecentFeedback::with(['getFeedbackComment','getUser' => fn($q) => $q->withTrashed()]); 
    // $lists=Feedback::with(['getAnimal','getUser' => fn($q) => $q->withTrashed()]); with('getUser','getAnimal');
      
      // DB::enableQueryLog();

       if($request->date_from){
       $list=  $list->whereDate('created_at','>=',$request->date_from);
       }


      if($request->date_to){
       $list=  $list->whereDate('created_at','<=',$request->date_to);
       }
     
      $list=  $list->orderBy('id', 'DESC')->paginate(10);
      $response['list']=$list;
       

      // return $response['list'];
      return view('feedback.feedback_list',$response);
    }

    public function feedbackVisitorList(Request $request, $feedback_recent_id){

      // return $request->date_from;
      // return $request->date_to;
      // DB::enableQueryLog();

      $lists = FeedbackComment::with(['getUserFeedback.getAnimal','getUser' => fn($q) => $q->withTrashed()]);

      if($request->date_from){
        
       $lists=  $lists->whereDate('created_at','>=',$request->date_from);
      }
      if($request->date_to){
       $lists=  $lists->whereDate('created_at','<=',$request->date_to);
      }
     
     

      $lists=  $lists->where('recent_feedback_id',$feedback_recent_id)->orderBy('id', 'DESC')->paginate(10);
      $response['lists'] = $lists;
      $response['feedback_recent_id'] = $feedback_recent_id;
       
      // return $response['lists'];
      return view('feedback.feedback_visitors_list',$response);
    }
// end class
}

  
