<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
// use App\Models\Section;
use App\Models\Animals;
use App\Models\Beacon;
use App\Models\SectionMedia;
use DB;
use Validator;
use Session;
class SectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {  
            $response['lists']=Animals::with('getSectionMedia')->orderBy('created_at', 'DESC')->paginate(15);
	        $response['beacon_list']=Beacon::where('status','active')->get();
            return view('section.list',$response);
        
    }

    public function view(Request $request , $id)
    {   
        $response['details']=Animals::with('getSectionMedia')->where('animal_id',$id)->first();
        
        return view('section.view',$response);
        
    }

    public function gallery_list(Request $request , $id)
    {   
      $response['details']=Animals::with('getSectionMedia')->where('animal_id',$id)->first();
        
        return view('section.gallery',$response);
        
    }
     
    public function gallery_create(Request $request, $id)
    {   
        $details = Animals::with('getSectionMedia')->where('animal_id',$id)->first();
         
          $exist_video = SectionMedia::where('animal_id',$id)->where('media_type','video')->first();
         $exist_audio = SectionMedia::where('animal_id',$id)->where('media_type','audio')->first();
         $exist_images = SectionMedia::where('animal_id',$id)->where('media_type','image')->get();
         // return $exist_audio;
        return view('section.create_gallery',compact('details','id','exist_video','exist_audio','exist_images'));
        
    }

    public function gallery_store(Request $request)
    {   
        // return $request->all();
        $validator = Validator::make($request->all(), [
                    
                   'media.*' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
       $delete_file=array();
     	$animal_id=$request->input('animal_id');
     	$media_file=$request->file('media');
     	$delete_file=$request->input('delete_file');
        
        $media=array();
        $media_type=array();
        if ($request->hasFile('media') != "") {
            foreach($request->file('media') as $key=>$file)
            {
                
                $name=$file->getClientOriginalName();  
                if(!empty($delete_file)){
                if (!in_array($name ,$delete_file )) {
  						// No values from array1 are in array 2
                $extension=$file->getClientOriginalExtension();  
                // $ext = pathinfo($file, PATHINFO_EXTENSION);  
                $file->move(public_path().'/section_media/', $name);      
                $media[] = '/section_media/'.$name;  
                $media_type[] = $extension;  
            	}
                }else{
                	$extension=$file->getClientOriginalExtension();  
                	// $ext = pathinfo($file, PATHINFO_EXTENSION);  
                	$file->move(public_path().'/section_media/', $name);      
                	$media[] = '/section_media/'.$name;  
                	$media_type[] = $extension;
                }
        	}
        }

        // return $media;
       if(empty($media)){
       		return array("msg"=>"No media found"); 
       }else{
    	for($i=0;$i<count($media);$i++)
        {
            $add = new SectionMedia();
            $add->animal_id=$animal_id;
            $add->media_url=$media[$i];
        	if($media_type[$i] == 'mp4' || $media_type[$i] == 'mp3')
            {
            	$add->media_type='video';
            }else{
            	$add->media_type='image';
            }
        	$add->save();
        }
       }
        
        return array("msg"=>"Media added Successfully"); 
        // return back()->with('success','Media added Successfully');
        
    }

	 public function gallery_edit(Request $request , $id)
    {   
        $response['details']=SectionMedia::where('media_id',$id)->first();
        
        return view('section.gallery_edit',$response);
        
    }

     public function edit_media(Request $request)
    {   
     	$validator = Validator::make($request->all(), [
                    
                   'media' => 'max:2048',
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
      $media_id=$request->input('media_id');
      $animal_id=$request->input('animal_id');
     	$text=$request->input('text');
        $media='';
        $media_type='';

      $update=SectionMedia::where('media_id',$media_id)->first();
            if ($request->hasFile('media') != "")
            {   
            	$file=$request->file('media');
                $name=$file->getClientOriginalName();  
                $extension=$file->getClientOriginalExtension();  
                // $ext = pathinfo($file, PATHINFO_EXTENSION);  
                $file->move(public_path().'/section_media/', $name);      
                $media = '/section_media/'.$name;  
                $media_type= $extension; 

                $update->media_url=$media;
            }
     // return $media_type;
        // if($media_type== 'mp4' || $media_type == 'mp3')
        //     {
        //     	$update->media_type='video';
       

        //     }else{
        //     	$update->media_type='image';
        //     }
        $update->text=$text;
        $update->save();
        
        return redirect('/section/gallery/'.$animal_id)->with('success','Successfully updated');
        
    }
      
      
    public function gallery_delete(Request $request , $id)
    {   
        $details=SectionMedia::where('media_id',$id)->delete();
        
        return redirect()->back()->with('success','Successfully deleted');
        
    }

     public function update(Request $request)
    {   
     
     // return $request->all();
     $validator = Validator::make($request->all(), [
                    
                    'animal_breed' => 'required',
                    'animal_name' => 'required',
                   'description' => 'required',
                   'start_time' => 'required',
                   'end_time' => 'required',
                   // 'beacon' => 'required',
                   
                   // 'media.*' => 'required',    
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
     
     
        $id=$request->input('animal_id');
        $animal_breed=$request->input('animal_breed');
        $animal_name=$request->input('animal_name');
        $description=$request->input('description');
        $start_time=$request->input('start_time');
        $end_time=$request->input('end_time');
        
        $update=Animals::where('animal_id', $id)->first();
        $update->animal_breed=$animal_breed;
        $update->animal_name=$animal_name;
        $update->description=$description;
        $update->feeding_time_start=$start_time;
        $update->feeding_time_end=$end_time;
        $update->save();
     	return redirect('/section/view/'.$id)->with('success','Successfully updated');
        
    }



   public function create(Request $request)
    {   
        // return $request->all();
        $validator = Validator::make($request->all(), [
                    
                    'animal_breed' => 'required|max:200',
                    'animal_name' => 'required|max:200',
                   'description' => 'required',
                   'start_time' => 'required',
                   'end_time' => 'required',
                   // 'beacon' => 'required',
                   
                  'video' => 'mimes:mp4,3gp',
                   'audio' => 'mimes:mp3,mp4',
                   // 'media' => 'required|max:2048',
                   // 'text' => 'required',
                   'thumbnail' => 'mimes:jpeg,jpg,png,gif',

                      
                    
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }

        // return $request->all();
        // return $request->media;
        $animal_breed=$request->input('animal_breed');
        $animal_name=$request->input('animal_name');
        $description=$request->input('description');
        // $beacon_id=$request->input('beacon');
        $start_time=$request->input('start_time');
        $end_time=$request->input('end_time');
        $video = $request->file('video');
        $audio = $request->file('audio');
        $thumbnail = $request->file('thumbnail');
        $text = $request->input('text');
        // $feeding_time=$start_time.'-'.$end_time;
        $media=array();
        $media_type=array();

            foreach($request->file('media') as $key=>$file)
            {
                $name=$file->getClientOriginalName();  
                $extension=$file->getClientOriginalExtension();  
                // $ext = pathinfo($file, PATHINFO_EXTENSION);  
                $file->move(public_path().'/section_media/', $name);      
                $media[] = '/section_media/'.$name;  
                $media_type[] = $extension;  
            }
        

        // return $media_type;
        $create=new Animals();
        $create->animal_breed=$animal_breed;
        $create->animal_name=$animal_name;
        $create->description=$description;
        // $create->beacon_id=$beacon_id;
        $create->feeding_time_start=$start_time;
        $create->feeding_time_end=$end_time;
        $create->save();

        for($i=0;$i<count($media);$i++)
        {
            $add = new SectionMedia();
            $add->animal_id=$create->animal_id;
            $add->media_url=$media[$i];
            $add->text=$text[$i];
        
            	$add->media_type='image';
            
        	$add->save();
        }
        
         if($request->hasFile('audio') != "") {
            $audio = $request->file('audio');
            $filename =$audio->getClientOriginalName();
            $destinationPath = public_path('/section_media');
            $audio->move($destinationPath, $filename);
            $audio = '/section_media/' . $filename;

           
                 $add_media = new SectionMedia();
                $add_media->media_type = 'audio';
                $add_media->media_url = $audio;
                $add_media->animal_id = $create->animal_id;
                $add_media->save();
           
        }
         if($request->hasFile('video') != "") {
            $video = $request->file('video');
            $filename =$video->getClientOriginalName();
            $destinationPath = public_path('/section_media');
            $video->move($destinationPath, $filename);
            $video = '/section_media/' . $filename;

        }

        if($request->hasFile('thumbnail') != "") {
            $thumbnail = $request->file('thumbnail');
            $filename =$thumbnail->getClientOriginalName();
            $destinationPath = public_path('/section_media');
            $thumbnail->move($destinationPath, $filename);
            $thumbnail = '/section_media/' . $filename;

        }

          $add_media = new SectionMedia();
                $add_media->media_type = 'video';
                $add_media->media_url = $video;
                $add_media->thumbnail = $thumbnail;
                $add_media->animal_id = $create->animal_id;
                $add_media->save();
        
        return redirect('/section')->with('success','Animal added Successfully');
        
    }

    public function status($id ,$status)
    {

        DB::enableQueryLog();
        $update=Animals::where('animal_id', $id)->first();
        if($status=='active')
        {
            
          $update->status='inactive';
        }
        else
        {
          $update->status='active';
        }
        $update->save();
       //return DB::getQueryLog();
        return redirect()->back()->with('success', 'Status Changed Successfully.');
    }

    public function mapping(Request $request)
    {   
       $all=Beacon::with('getAnimals')->get();
        $mapped_value=array();
        $get_mapped=Animals::whereNotNull('beacon_id')->distinct()->get(['beacon_id']);
        foreach($get_mapped as $list)
        {
        	$mapped_value[]=$list->beacon_id;
        }
    // return $mapped_value;
        $mapped=Beacon::with('getAnimals')->WhereIn('beacon_id', $mapped_value)->get();
        $unmapped=Beacon::with('getAnimals')->whereNotIn('beacon_id', $mapped_value)->get();
    	// $animals=Animals::all();
        $response['all']=$all;
        $response['mapped']=$mapped;
        $response['unmapped']=$unmapped;
        // $response['animals']=$animals;
        // foreach($animals as $animal_list)
        // {
        // 	return $animal_list->animal_breed;
        // }
        return view('mapping.list',$response);
        
    }

	public function unmapped(Request $request)
    {   
       
        $lists=Animals::with('getSectionMedia')->where('beacon_id','0')->paginate(10);
        $response['beacon_list']=Beacon::where('status','active')->get();
        foreach($lists as $list){
            // return $list->animal_id;
            $list->get_images_count = SectionMedia::where('animal_id',$list->animal_id)->where('media_type','image')->whereNull('deleted_at')->count();
        }
        $response['lists'] = $lists;
        // return $response['lists'];
        return view('mapping.unmapped',$response);
        
    }

	public function beacon_map(Request $request)
    {   
       // return $request->all();
       $validator = Validator::make($request->all(), [
                    
                    'animal_id' => 'required',
                    'beacon_id' => 'required',
      ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
       $animal_id=$request->input('animal_id');
       $beacon_id=$request->input('beacon_id');
       $get_animal=Animals::where('animal_id', $animal_id)->first();
       $get_animal->beacon_id=$beacon_id;
       $get_animal->save();
       return redirect()->back()->with('success', 'Animal mapped Successfully.');
        
    }

    public function map_beacon(Request $request)
    {   
       // return $request->all();
       $validator = Validator::make($request->all(), [
                    
                    //'animal_id.*' => 'required',
                    'beacon_id' => 'required',
      ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
       
       $animal_id=$request->input('animal_id');
       $beacon_id=$request->input('beacon_id');
       $update_beacon=Animals::where('beacon_id', $beacon_id)->update(["beacon_id"=>0]);
       if($animal_id){
       foreach($animal_id as $list)
       {
       		$update=Animals::where('animal_id', $list)->first();
            $update->beacon_id=$beacon_id;
            $update->save();
       }
       }
       
       return redirect()->back()->with('success', 'Animal mapped Successfully.');
    }

    public function get_data(Request $request)
    {   
       $beacon_id=$request->input('beacon_id');
    	// return $beacon_id;
       $data=Animals::where('status', 'active')->get();
       $html='';
       foreach($data as $list)
       {
       		if($list->beacon_id == $beacon_id)
            {
       			$html.='<li><input type="checkbox" class="checkSingle" name="animal_id[]" value="'.$list->animal_id.'" checked/>'.$list->animal_breed.' </li>';
            }else{
            	$html.='<li><input type="checkbox" class="checkSingle" name="animal_id[]" value="'.$list->animal_id.'"/>'.$list->animal_breed.' </li>';
            }
       		
       }
        
       return array('html'=>$html);
        
    }

    public function saveMedia(Request $request,$animal_id){

        // return $request->all();die;
        $animal_id = $request->input('animal_id');
        $audio = $request->file('audio');
        $video = $request->file('video');
        $image = $request->file('image');
        $text = $request->input('text');

       $video_exist = $audio_exist = false;
        $image_data=array();
        if($request->hasFile('image') != "")
        {
            foreach($request->file('image') as $key=>$file)
            {
                // $image = $file->getClientOriginalName();
                $image = $file->getClientOriginalName();
                $file->move(public_path() . '/section_media/', $image);
                $image_data[] = '/section_media/'.$image;
            }
        }
        for($i=0;$i<count($image_data);$i++)
        {    $add_media = new SectionMedia();
             $add_media->media_type = 'image';
            $add_media->text = $text[$i];
            $add_media->media_url = $image_data[$i] ;
            $add_media->animal_id = $animal_id;
            $add_media->save();
            
        }


        if($request->hasFile('audio') != "") {
            $audio = $request->file('audio');
            $filename =$audio->getClientOriginalName();
            $destinationPath = public_path('/section_media');
            $audio->move($destinationPath, $filename);
            $audio = '/section_media/' . $filename;

            if(SectionMedia::where('animal_id',$animal_id)->where('media_type','audio')->count()>0){
                // dd('working');
                // return redirect('/section/gallery/'.$animal_id)->with('Fail','You cant add the more audio file because audio file for this animal is already exist..Please delete first then add new file');
                $audio_exist = true;
            }
            else{
                 $add_media = new SectionMedia();
                $add_media->media_type = 'audio';
                $add_media->media_url = $audio;
                $add_media->animal_id = $animal_id;
                $add_media->save();
            }
        }
        if($request->hasFile('video') != "") {
            $video = $request->file('video');
            $filename =$video->getClientOriginalName();
            $destinationPath = public_path('/section_media');
            $video->move($destinationPath, $filename);
            $video = '/section_media/' . $filename;

            if(SectionMedia::where('animal_id',$animal_id)->where('media_type','video')->count()>0){

                // return redirect('/section/gallery/'.$animal_id)->with('Fail','You cant add the more video file because video file for this animal is already exist..Please delete first then add new file');
                $video_exist = true;
            }
            else{
                $add_media = new SectionMedia();
                $add_media->animal_id = $animal_id;
                $add_media->media_type = 'video';
                $add_media->media_url = $video;
                $add_media->save();
            }
        }
       
            $msg = "";
            if($audio_exist){
                $msg= $msg. " You cant add the more audio file because audio file for this animal is already exist..Please delete first then add new file ";
            }
           if($video_exist){
                $msg= $msg. " You cant add the more video file because video file for this animal is already exist..Please delete first then add new file ";
            }
          // return $add_media;die;
        return redirect('/section/gallery/'.$animal_id)->with('success','Media Save Successfully '.$msg);
    }

    public function saveimages(Request $request,$animal_id){

      // return $request->all();
      $image = $request->file('image');
      $text = $request->input('text');
       if($request->hasFile('image') != "") {
            $image = $request->file('image');
            $filename =$image->getClientOriginalName();
            $destinationPath = public_path('/section_media');
            $image->move($destinationPath, $filename);
            $image = '/section_media/' . $filename;
        }
      $add_images = new SectionMedia();
      $add_images->animal_id = $request->animal__id;
      $add_images->media_type = 'image';
      $add_images->media_url = $image;
      $add_images->text = $text;
      $add_images->save();
      
          return redirect('/section/gallery/add/'.$animal_id)->with('success','Image and Text Added Successfully');
     

    }

    public function deleteImageRow($media_id,$animal_id){

      // return $media_id;
      $delete_row = SectionMedia::where('media_id',$media_id)->delete();
      return redirect('/section/gallery/add/'.$animal_id)->with('fail','Image is deleted');
    }

    public function saveVideo(Request $request,$animal_id){

       // return $request->all();
      $video = $request->file('video');
      if($request->hasFile('video') != "") {
        $video = $request->file('video');
        $filename =$video->getClientOriginalName();
        $destinationPath = public_path('/section_media');
        $video->move($destinationPath, $filename);
        $video = '/section_media/' . $filename;
      }
      if(SectionMedia::where('animal_id',$animal_id)->where('media_type','video')->first()){
           $add_video = SectionMedia::where('animal_id',$animal_id)->where('media_type','video')->update(["media_url" =>$video]);
      }else{
         $add_video = SectionMedia::insert(["media_url" =>$video,"animal_id"=>$animal_id,"media_type"=>"video"]);
      }
     
      
      
          return redirect('/section/gallery/add/'.$animal_id)->with('success','Video Added Successfully');
    }

     public function saveAudio(Request $request,$animal_id){

       // return $request->all();
      $audio = $request->file('audio');
      if($request->hasFile('audio') != "") {
        $audio = $request->file('audio');
        $filename =$audio->getClientOriginalName();
        $destinationPath = public_path('/section_media');
        $audio->move($destinationPath, $filename);
        $audio = '/section_media/' . $filename;
      }
      if(SectionMedia::where('animal_id',$animal_id)->where('media_type','audio')->first()){

         $add_audio = SectionMedia::where('animal_id',$animal_id)->where('media_type','audio')->update(["media_url" =>$audio]);
      }else{
         $add_audio = new SectionMedia();
        $add_audio->animal_id = $animal_id;
        $add_audio->media_type = 'audio';
        $add_audio->media_url = $audio;
        $add_audio->save();
      }
       
        return redirect('/section/gallery/add/'.$animal_id)->with('success','Audio Added Successfully');
    }
     public function deleteVideoRow($media_id,$animal_id){

      // return $media_id;
      $delete_row = SectionMedia::where('media_id',$media_id)->delete();
      return redirect('/section/gallery/add/'.$animal_id)->with('fail','Video is deleted');
    }

     public function deleteAudioRow($media_id,$animal_id){

      // return $media_id;
      $delete_row = SectionMedia::where('media_id',$media_id)->delete();
      return redirect('/section/gallery/add/'.$animal_id)->with('fail','Audio file is deleted');
    }

    // public function finalSubmit(Request $request){

    //   $animal_id = $request->input('video_animal_id');
    //   $check = SectionMedia::where('animal_id',$animal_id)->where('media_type','image')->first();
    //   if($check){
    //     // return redirect('/section/gallery/'.$animal_id);
    //      return array("status"=>"200","msg"=>"Already Have a data");
    //   }else{

    //       return array("status"=>"400","msg"=>"Please add atleast one image for animal"); 
    //   }
    // }

      public function saveThumbnail(Request $request,$animal_id){

       // return $request->all();
      $thumbnail = $request->file('thumbnail');
      if($request->hasFile('thumbnail') != "") {
        $thumbnail = $request->file('thumbnail');
        $filename =$thumbnail->getClientOriginalName();
        $destinationPath = public_path('/section_media');
        $thumbnail->move($destinationPath, $filename);
        $thumbnail = '/section_media/' . $filename;
      }
      if(SectionMedia::where('animal_id',$animal_id)->where('media_type','video')->first()){

         $add_thumbnail = SectionMedia::where('animal_id',$animal_id)->where('media_type','video')->update(["thumbnail" =>$thumbnail]);
      }else{
         $add_thumbnail = new SectionMedia();
        $add_thumbnail->animal_id = $animal_id;
        $add_thumbnail->thumbnail = $thumbnail;
        $add_thumbnail->save();
      }
       
        return redirect('/section/gallery/add/'.$animal_id)->with('success','Thumbnail Of Video Added Successfully');
    }

    public function view_media($id){

        $response['section_media'] = SectionMedia::where('animal_id',$id)->where('media_type','image')->whereNull('deleted_at')->paginate(10);
        //return $response['section_media'];
        return view('mapping.view_media',$response);
    }


// end class
}
