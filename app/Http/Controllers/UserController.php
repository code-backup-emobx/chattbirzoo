<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Section;
use DB;
use Validator;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {  
        // return $request->all();
        $view=$request->view;
        $name=$request->name;
        $from = $request->from;
        $to = $request->to;
        $lists=User::whereRole('user');
        if ($from != '' OR $to != '')
	    {
            $response['lists']=$lists->whereDate('created_at','>=',$from)->whereDate('created_at','<=', $to)->orderBy('created_at', 'DESC')->paginate(15);
	    }elseif($name !='')
	    {
            $response['lists']=$lists->where('username', 'like', '%'.$name.'%')->orderBy('created_at', 'DESC')->paginate(15);
	    	
	    }
	    else{

		    $response['lists']=$lists->orderBy('created_at', 'DESC')->withTrashed()->paginate(15);
	    }
        return view('users.list',$response);
        
    }

    public function admin_list(Request $request)
    {  
        // return $request->all();
        $view=$request->view;
        $name=$request->name;
        $from = $request->from;
        $to = $request->to;
        $lists=User::whereRole('admin');
        if ($from != '' OR $to != '')
        {
            $response['lists']=$lists->whereDate('created_at','>=',$from)->whereDate('created_at','<=', $to)->orderBy('created_at', 'DESC')->paginate(15);
        }elseif($name !='')
        {
            $response['lists']=$lists->where('username', 'like', '%'.$name.'%')->orderBy('created_at', 'DESC')->paginate(15);
            
        }
        else{

            $response['lists']=$lists->orderBy('created_at', 'DESC')->paginate(15);
        }
        return view('admin.list',$response);
        
    }

    public function view(Request $request , $id)
    {   
        $response['details']=User::whereRole('admin')->where('id',$id)->first();
        
        return view('admin.view',$response);
        
    }

    public function user_view(Request $request , $id)
    {   
        $response['details']=User::whereRole('user')->where('id',$id)->first();
        
        return view('users.user_view',$response);
        
    }

     public function update(Request $request)
    {   
     	// return $request->all();
        $validator = Validator::make($request->all(), [
                    
                    'email' => 'required|string|email',
                   'username' => 'required',
                   
                           
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        $id=$request->input('id');
        $username=$request->input('username');
        $email=$request->input('email');
        
        $update=User::where('id', $id)->first();
        $update->username=$username;
        $update->email=$email;
        $update->save();
        
        
        return back()->with('success','Successfully added');
        
    }

    public function password()
    {   
      
        return view('admin.password');
    }

    public function change_password(Request $request)
    {   
        $validator = Validator::make($request->all(), [
                    
                    // 'old_password' => 'required',
                   'new_password' => 'required',
                   
                   'confirm_password' => 'required',
                       
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        $old_password=$request->input('old_password');
        $new_password=$request->input('new_password');
        $confirm_password=$request->input('confirm_password');
        $user_id = Auth::user()->id;
        $check_user = User::find($user_id);
    	if (Hash::check($old_password, $check_user->password) OR $check_user->role == 'superadmin') {
            if($new_password == $confirm_password)
            {
                $check_user->password = bcrypt($new_password);
                $check_user->save();
                return redirect('/admin/password')->with('success','Password change Successfully');
            }else{
            	return redirect('/admin/password')->with('fail','Password mismatch');
            }
        }else{
            	return redirect('/admin/password')->with('fail','Old password mismatch');
            }
    }

    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(), [
                    
                    'email' => 'required|string|email',
                   'username' => 'required',
                   
                   'password' => 'required',
                   
                        
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        $username=$request->input('username');
        $email=$request->input('email');
        $password=$request->input('password');
        $role=$request->input('role');
        // $status=$request->input('status');

        
        $create=new User();
        $create->username=$username;
        $create->email=$email;
        $create->password=bcrypt($password); 
        $create->role=$role;
        // $create->status=$status;
        $create->save();
        
        
        return back()->with('success','Admin User added Successfully');
        
    }

    public function change_status($id ,$status)
    {

        DB::enableQueryLog();
        $user=User::find($id);
        if($status=='active')
        {
            
          $user->status='inactive';
        }
        else
        {
          $user->status='active';
        }
        $user->save();
       //return DB::getQueryLog();
        return redirect()->back()->with('success', 'User Status Changed Successfully.');
    }

    public function changePassword($user_id){

        return view('admin.change_password',compact('user_id')); 
    }

    public function savechangePassword(Request $request , $user_id){

        // return $request->all();
        $validator = Validator::make($request->all(), [
                    
                  
                   'new_password' => 'required',
                   
                   'confirm_password' => 'required',
                       
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
       
        $new_password=$request->input('new_password');
        $confirm_password=$request->input('confirm_password');
      
        $check_user = User::find($user_id);
        
        if($new_password == $confirm_password)
        {
            $check_user->password = bcrypt($new_password);
            $check_user->save();
            return redirect('/admin')->with('success','Password change Successfully');
        }else{
            return redirect('/admin/change_pswd/'.$user_id)->with('fail','New Password and Confirm Password mismatch');
        }
       
    }
//end class 
}
