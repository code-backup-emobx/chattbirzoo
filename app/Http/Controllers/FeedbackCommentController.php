<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\FeedbackComment;

class FeedbackCommentController extends Controller
{
    
    public function index(Request $request)
    {  
        $lists=FeedbackComment::with('getUser') ;
    
       if($request->user_id){
       $lists=  $lists->where('user_id',$request->user_id);
       }
    
       if($request->comment){
       $lists=  $lists->where('comment','Like','%'.$request->comment.'%');
       }
       if($request->date_from){
       $lists=  $lists->where('created_at','>=',$request->date_from);
       }
       if($request->date_to){
       $lists=  $lists->where('created_at','<=',$request->date_to);
       }
     
      
    
       $lists=  $lists->orderBy('id', 'DESC')->paginate();
	   $response['lists']=$lists;
            return view('feedback.comment.list',$response);
        
    }
}
  
