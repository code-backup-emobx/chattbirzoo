<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('home');
})->middleware(['auth'])->name('home');


Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('users');
Route::get('/user/view/{id}', [App\Http\Controllers\UserController::class, 'user_view']);
Route::post('/user/edit', [App\Http\Controllers\UserController::class, 'update']);
Route::post('/user/add', [App\Http\Controllers\UserController::class, 'store']);

Route::get('/admin', [App\Http\Controllers\UserController::class, 'admin_list'])->name('admin');
Route::get('/admin/view/{id}', [App\Http\Controllers\UserController::class, 'view']);
Route::post('/admin/edit', [App\Http\Controllers\UserController::class, 'update']);
Route::get('/admin/password', [App\Http\Controllers\UserController::class, 'password']);
Route::post('/admin/password', [App\Http\Controllers\UserController::class, 'change_password']);
Route::post('/admin/add', [App\Http\Controllers\UserController::class, 'store']);

Route::get('/users/change_status/{id}/{status}', [App\Http\Controllers\UserController::class, 'change_status']);

Route::get('/section', [App\Http\Controllers\SectionController::class, 'index'])->name('section');
Route::get('/section/view/{id}', [App\Http\Controllers\SectionController::class, 'view']);
Route::post('/section/edit', [App\Http\Controllers\SectionController::class, 'update']);
Route::post('/section/add', [App\Http\Controllers\SectionController::class, 'create']);
Route::get('/section/change_status/{id}/{status}', [App\Http\Controllers\SectionController::class, 'status']);
Route::get('/mapping', [App\Http\Controllers\SectionController::class, 'mapping'])->name('mapping');
Route::post('map', [App\Http\Controllers\SectionController::class, 'beacon_map'])->name('map');
Route::get('/unmapped', [App\Http\Controllers\SectionController::class, 'unmapped']);

Route::post('/section/gallery/add', [App\Http\Controllers\SectionController::class, 'gallery_store']);
Route::get('/section/gallery/add/{id}', [App\Http\Controllers\SectionController::class, 'gallery_create']);
Route::get('/section/gallery/{id}', [App\Http\Controllers\SectionController::class, 'gallery_list']);
Route::get('/section/gallery/edit/{id}', [App\Http\Controllers\SectionController::class, 'gallery_edit']);
Route::post('/section/gallery/edit', [App\Http\Controllers\SectionController::class, 'edit_media']);
Route::get('/section/gallery/delete/{id}', [App\Http\Controllers\SectionController::class, 'gallery_delete']);



Route::get('/beacon', [App\Http\Controllers\BeaconController::class, 'index'])->name('beacon');
Route::get('/beacon/view/{id}', [App\Http\Controllers\BeaconController::class, 'view']);
Route::post('/beacon/edit', [App\Http\Controllers\BeaconController::class, 'update']);
Route::post('/beacon/add', [App\Http\Controllers\BeaconController::class, 'create']);

Route::post('map_beacon', [App\Http\Controllers\SectionController::class, 'map_beacon'])->name('map_beacon');

// Route::get('/mapping', [App\Http\Controllers\BeaconController::class, 'index'])->name('mapping');



//ajax request
Route::post('/get_data', [App\Http\Controllers\SectionController::class, 'get_data'])->name('get_data');

require __DIR__.'/auth.php';
