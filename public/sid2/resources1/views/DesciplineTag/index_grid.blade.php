@extends('layouts.base')

@section('content')

														
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Toolbar-->
						<div class="toolbar" id="kt_toolbar">
							<!--begin::Container-->
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Descipline Tag List</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start mx-4"></span>
									<!--end::Separator-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="/home" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">Descipline Tag</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark">Descipline Tag Listing</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
								<!--begin::Actions-->
								<div class="d-flex align-items-center py-1">
									<!--begin::Wrapper-->
									<div class="me-4">
										<!--begin::Menu-->
										<a href="#" class="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-flip="top-end">
										<!--begin::Svg Icon | path: icons/duotone/Text/Filter.svg-->
										<span class="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24" />
													<path d="M5,4 L19,4 C19.2761424,4 19.5,4.22385763 19.5,4.5 C19.5,4.60818511 19.4649111,4.71345191 19.4,4.8 L14,12 L14,20.190983 C14,20.4671254 13.7761424,20.690983 13.5,20.690983 C13.4223775,20.690983 13.3458209,20.6729105 13.2763932,20.6381966 L10,19 L10,12 L4.6,4.8 C4.43431458,4.5790861 4.4790861,4.26568542 4.7,4.1 C4.78654809,4.03508894 4.89181489,4 5,4 Z" fill="#000000" />
												</g>
											</svg>
										</span>
										<!--end::Svg Icon-->Filter</a>
										<!--begin::Menu 1-->
										<div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true">
											<!--begin::Header-->
											<div class="px-7 py-5">
												<div class="fs-5 text-dark fw-bolder">Filter Options</div>
											</div>
											<!--end::Header-->
											<!--begin::Menu separator-->
											<div class="separator border-gray-200"></div>
											<!--end::Menu separator-->
											<form action="" method="get">
												<!-- @csrf -->
											<!--begin::Form-->
											<div class="px-7 py-5">
												<!--begin::Input group-->
												<div class="mb-10">
													<!--begin::Label-->
													<label class="form-label fw-bold">View:</label>
													<!--end::Label-->
													<!--begin::Input-->
													<div>
														<select class="form-select form-select-solid" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true" name="view">
															<option></option>
															<option value="list_view">List View</option>
															<option value="grid_view">Grid View</option>
															
														</select>
													</div>
													<!--end::Input-->
												</div>
                                                
<!--                                                 <div class="mb-10">
    <label class="form-label fw-bold">Range Picker:</label>
    <div class="col-lg-4 col-md-9 col-sm-12">
     <div class="input-daterange input-group" id="kt_datepicker_5">
      <input type="text" class="form-control" name="start"/>
      <div class="input-group-append">
       <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
      </div>
      <input type="text" class="form-control" name="end"/>
     </div>
     <span class="form-text text-muted">Linked pickers for date range selection</span>
    </div>
   </div> -->
<!--                                             <div class="mb-10">
                                            <label class="form-label fw-bold">From Date:</label>
                        <div class="form-group">
                          <input type="date"  id="from-datepicker" class="form-control" name="from" value="" placeholder="from date" readonly="from" />
                        </div>
                      </div>
                     
                      <div class="mb-10">
                      <label class="form-label fw-bold">To Date:</label>
                        <div class="form-group">
                          <input type="date"  id="from-datepicker1" class="form-control" name="to" value="" placeholder="to date" readonly="to"/>
                        </div>  
                      </div> -->
                                                <div class="mb-10">
													<!--begin::Label-->
													<label class="form-label fw-bold">Name:</label>
													<!--end::Label-->
													<!--begin::Input-->
													<div>
														<input type="text" class="form-control" name="name"/>
													</div>
													<!--end::Input-->
												</div>
												<!--end::Input group-->
												<!--begin::Input group-->
												
												<!--end::Input group-->
												<!--begin::Input group-->
												
												<!--end::Input group-->
												<!--begin::Actions-->
												<div class="d-flex justify-content-end">
													<!-- <button type="reset" class="btn btn-sm btn-light btn-active-light-primary me-2" data-kt-menu-dismiss="true">Reset</button> -->
													<button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true">Apply</button>
												</div>
												<!--end::Actions-->
											</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Menu 1-->
										<!--end::Menu-->
									</div>
									<!--end::Wrapper-->
									<!--begin::Button-->
									<!-- <a href="#" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_create_app" id="kt_toolbar_primary_button">Create</a> -->
									<!--end::Button-->
								</div>
								<!--end::Actions-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Toolbar-->
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container">
								<!--begin::Navbar-->
								
								<!--end::Navbar-->
								<!--begin::Modals-->
								<!--begin::Modal - View Users-->
								
								<!--end::Modal - View Users-->
								<!--begin::Modal - Users Search-->
								
								<!--end::Modal - Users Search-->
								<!--begin::Modal - New Target-->
								
								<!--end::Modal - New Target-->
								<!--end::Modals-->
								<!--begin::Toolbar-->
								
								<!--end::Toolbar-->
								<!--begin::Tab Content-->
								<div class="tab-content">
									<!--begin::Tab pane-->
									<div id="kt_project_users_card_pane" class="tab-pane fade show active">
										<!--begin::Row-->
										<div class="row g-6 g-xl-9">
											<!--begin::Col-->
											@foreach($lists as $list)
											<div class="col-md-6 col-xxl-4">
												<!--begin::Card-->
												<div class="card">
													<!--begin::Card body-->
													<div class="card-body d-flex flex-center flex-column pt-12 p-9">
														<!--begin::Avatar-->
														<div class="symbol symbol-65px symbol-circle mb-5">
															@if($list->image)
																<img src="{{ url($list->image)}}" alt="image" />
															@else
																<img src="/assets/media/avatars/blank.png" alt="image" />
															@endif
															<div class="bg-success position-absolute border border-4 border-white h-15px w-15px rounded-circle translate-middle start-100 top-100 ms-n3 mt-n3"></div>
														</div>
														<!--end::Avatar-->
														<!--begin::Name-->
														<a class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">{{ ucfirst($list->name )  ?? '' }}</a>
														<!--end::Name-->
														<!--begin::Position-->
														<div class="fw-bold text-gray-400 mb-6">{{ $list->descipline_type ?? ''}}</div>
														<!--end::Position-->
														<!--begin::Info-->
														<div class="d-flex flex-center flex-wrap">
															<!--begin::Stats-->
															
															<!--end::Stats-->
															<!--begin::Stats-->
															<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
																<div class="fs-6 fw-bolder text-gray-700">@if($list->created_at)

                                                                        {{  date('d M Y h:i:a',strtotime($list->created_at))   }}
                                                                        @endif</div>
																<div class="fw-bold text-gray-400">Created At</div>
															</div>
															<!--end::Stats-->
															<!--begin::Stats-->
															<div class="border border-gray-300 border-dashed rounded min-w-80px py-3 px-4 mx-2 mb-3">
																<div class="fs-6 fw-bolder text-gray-700">
														<!-- @if($list->status =='active')
															<a href="javascript:void(0);" onclick="change_status('{{$list->id}}','{{$list->status}}');" class="btn btn-sm btn-light-success">Active</a>
														@else
															<a href="javascript:void(0);" onclick="change_status('{{$list->id}}','{{$list->status}}');" class="btn btn-sm btn-light-danger" data-kt-customer-table-filter="delete_row">Inactive</a>
														@endif -->
                                                          <a  href="{{ route('DesciplineTag.status',[$list->id,$list->status])}}"onclick="return confirm('Are you sure You want to change the status?')">
                                                            @if($list->status=='active') 
                                                                     <span class="btn btn-sm btn-success"> Block it</span>
																
															    @else 
															           <span class="btn btn-sm btn-danger">  Activate</span>
															    @endif
														    </a>
															<a  href="{{ route('DesciplineTag.soft_delete',[$list->id])}}"onclick="return confirm('Are you sure You want to delete ?')">
                                                               @if($list->deleted_at==null) 
                                                                     <span class="btn btn-sm btn-danger"> Delete</span>
															    @endif
														    </a>
															<a href="#" class="btn btn-sm btn-primary update-tag-btn" data-bs-toggle="modal" datatagid="{{ $list->id }}" dataname="{{ $list->name }}" datadescipline_text="{{ $list->descipline_text }}" datadescipline_type="{{ $list->descipline_type }}"  data-bs-target="#kt_modal_edit_api_key" id="kt_toolbar_primary_button">Edit</a> 
								
														
																</div>
																<!-- <div class="fw-bold text-gray-400">Action</div> -->
															</div>
															<!--end::Stats-->
														</div>
														<!--end::Info-->
													</div>
													<!--end::Card body-->
												</div>
												<!--end::Card-->
											</div>
											@endforeach
											<!--end::Col-->
										</div>
										<!--end::Row-->
										<!--begin::Pagination-->
										{!! $lists->appends(request()->except(['page', '_token']))->links() !!}
										<!--end::Pagination-->
									</div>
									<!--end::Tab pane-->
									<!--begin::Tab pane-->
									
									<!--end::Step 1-->
									<!--begin::Step 2-->
								</div>
							</div>
						</div>
					</div>
					<!-- updatejp -->
					<div class="modal fade" id="kt_modal_edit_api_key" tabindex="-1" aria-hidden="true">
									<!--begin::Modal dialog-->
									<div class="modal-dialog modal-dialog-centered mw-650px">
										<!--begin::Modal content-->
										<div class="modal-content">
											<!--begin::Modal header-->
											<div class="modal-header" id="kt_modal_edit_api_key_header">
												<!--begin::Modal title-->
												<h2>Update </h2>
												<!--end::Modal title-->
												<!--begin::Close-->
												<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
													<!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
													<span class="svg-icon svg-icon-1">
														<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
															<g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
																<rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
																<rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
															</g>
														</svg>
													</span>
													<!--end::Svg Icon-->
												</div>
												<!--end::Close-->
											</div>
											<!--end::Modal header-->
											<!--begin::Form-->
											<form id="kt_modal_edit_api_key_form" action="{{url('/desciplinetags/edit')}}" method="post" class="form" enctype="multipart/form-data" >
                                            @csrf
												<!--begin::Modal body-->
												<input type="hidden" name="id" id="datatagid" value="">
												<div class="modal-body py-10 px-lg-17">
													<!--begin::Scroll-->
													<div class="scroll-y me-n7 pe-7" id="kt_modal_create_api_key_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_edit_api_key_header" data-kt-scroll-wrappers="#kt_modal_edit_api_key_scroll" data-kt-scroll-offset="300px">
														<!--begin::Notice-->
														<div class="notice d-flex bg-light-warning rounded border-warning border border-dashed mb-10 p-6">
															<!--begin::Icon-->
															<!--begin::Svg Icon | path: icons/duotone/Code/Warning-1-circle.svg-->
															<span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
																<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
																	<rect fill="#000000" x="11" y="7" width="2" height="8" rx="1" />
																	<rect fill="#000000" x="11" y="16" width="2" height="2" rx="1" />
																</svg>
															</span>
															<!--end::Svg Icon-->
															<!--end::Icon-->
															
														</div>
														<!--end::Notice-->
														<!--begin::Input group-->
														<div class="mb-5 fv-row">
															<!--begin::Label-->
															<label class="required fs-5 fw-bold mb-2"> Name</label>
															<!--end::Label-->
															<!--begin::Input-->
															<input type="text" id="dataname" class="form-control form-control-solid" placeholder="Name" name="name" />
															<!--end::Input-->
														</div>
														<!--end::Input group-->
														<!--begin::Input group-->
														<div class="d-flex flex-column mb-5 fv-row">
															<!--begin::Label-->
															<label class="required fs-5 fw-bold mb-2">Short Description</label>
															<!--end::Label-->
															<!--begin::Input-->
															<textarea class="form-control form-control-solid" rows="3"  id="datadescipline_text" name="descipline_text" placeholder="Describe"></textarea>
															<!--end::Input-->
														</div>
														<!--end::Input group-->
														<!--begin::Input group-->
														<div class="d-flex flex-column mb-10 fv-row">
															<!--begin::Label-->
															<label class="required fs-5 fw-bold mb-2">Descipline Type </label>
															<!--end::Label-->
															<!--begin::Select-->
															<input type="file" name='image' accept="image/*"/>
															<!--end::Select-->
														</div>
														<!--end::Input group-->
														<!--begin::Input group-->
														<div class="mb-10">
															<!--begin::Heading-->
															<div class="mb-3">
																<!--begin::Label-->
																<label class="d-flex align-items-center fs-5 fw-bold">
																	<span class="required">Descipline Type </span>
																	<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title=""></i>
																</label>
																<!--end::Label-->
																<!--begin::Description-->
																<div class="fs-7 fw-bold text-muted"></div>
																<!--end::Description-->
															</div>
															<!--end::Heading-->
															<!--begin::Row-->
															<div class="fv-row">
																<!--begin::Radio group-->
																 <div class="btn-group w-100" data-kt-buttons="true" data-kt-buttons-target="[data-kt-button]">
																	<!--begin::Radio-->
																	<label class="btn btn-outline-secondary text-muted text-hover-white text-active-white btn-outline btn-active-success" data-kt-button="true">
																	<!--begin::Input-->
																	<input class="btn-check" type="radio" name="method" value="sports" id="datadescipline_type_sports" name="descipline_type" />
																	<!--end::Input-->
																	Sports</label>
																	<!--end::Radio-->

																	<!--begin::Radio-->
																	<label class="btn btn-outline-secondary text-muted text-hover-white text-active-white btn-outline btn-active-success active" data-kt-button="true">
																	<!--begin::Input-->
																	<input class="btn-check" type="radio" name="method" checked="checked" id="datadescipline_type_lifestyle" value="lifestyle" name="descipline_type" />
																	<!--end::Input-->
																	lifestyle</label>
																	<!--end::Radio-->

																	<!--begin::Radio-->
																	<label class="btn btn-outline-secondary text-muted text-hover-white text-active-white btn-outline btn-active-success" data-kt-button="true">
																	<!--begin::Input-->
																	<input class="btn-check" type="radio" name="method" value="" id="datadescipline_type"  name="descipline_type" />
																	<!--end::Input-->
																	Name It</label>
																	<!--end::Radio-->
																	<!--begin::Radio-->
																	
																	<!--end::Radio-->
																</div>
																<!--end::Radio group-->
															</div>
															<!--end::Row-->
														</div>
														<!--end::Input group-->
													</div>
													<!--end::Scroll-->
												</div>
												<!--end::Modal body-->
												<!--begin::Modal footer-->
												<div class="modal-footer flex-center">
													<!--begin::Button-->
													<button type="reset" id="kt_modal_create_api_key_cancel" class="btn btn-light me-3">Discard</button>
													<!--end::Button-->
													<!--begin::Button-->
													<button type="submit" id="kt_modal_create_api_key_submit" class="btn btn-primary">
														<span class="indicator-label">Submit</span>
														<span class="indicator-progress">Please wait...
														<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
													</button>
													<!--end::Button-->
												</div>
												<!--end::Modal footer-->
											</form>
											<!--end::Form-->
										</div>
										<!--end::Modal content-->
									</div>
									<!--end::Modal dialog-->
								</div>
								
    
					<!-- jpend -->
                    

									
@endsection                

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">
    function change_status(id,status)
  {
     // alert(status);
     var token = '{{ csrf_token() }}';
     if(status == 'active')
     {
       if(confirm("Are you sure you want to inactive."))
       { 
        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                type: "POST",
                url: "/change_status",
                data: {id:id,status:'inactive'},
                success: function(result){
                        console.log(result);  
                
               $('#show_status_'+id).html('<a href="javascript:void(0);" onclick="change_status("'+id+'","'+result.status+'");" class="label label-lg font-weight-bold label-inline label-light-danger">Inactive</a>');
                }
          });
        }
      }
      else
      {
      	if(confirm("Are you sure you want to active."))
        { 
           $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                type: "POST",
                url: "/change_status",
                data: {id:id,status:'active'},
                success: function(result){
                     console.log(result);    
$('#show_status_'+id).html('<a href="javascript:void(0);" onclick="change_status("'+id+'","'+result.status+'");" class="label label-lg font-weight-bold label-inline label-success">Active </a>');                }
          });
        }
      }
    
    }
$(document).ready(function (e) {
	$('.update-tag-btn').on("click",function(){
     var datatagid = $(this).attr('datatagid');
	 $("#datatagid").val(datatagid);
	 var dataname = $(this).attr('dataname');
	 $("#dataname").val(dataname);
	 var datadescipline_text = $(this).attr('datadescipline_text');
	 $("#datadescipline_text").val(datadescipline_text);
	 var datadescipline_type = $(this).attr('datadescipline_type');
	 if(datadescipline_type =='lifestyle'){
		$("#datadescipline_type").prop('checked', false);
		$("#datadescipline_type_lifestyle").prop('checked', true);
		$("#datadescipline_type_sports").prop('checked', false);
	 } else if(datadescipline_type =='sports'){
		$("#datadescipline_type").prop('checked', false);
		$("#datadescipline_type_lifestyle").prop('checked', false);
		$("#datadescipline_type_sports").prop('checked', true);
	 } else {
		$("#datadescipline_type").prop('checked', true);
		$("#datadescipline_type_lifestyle").prop('checked', false);
		$("#datadescipline_type_sports").prop('checked', false);
	 }
	});
});   

</script>