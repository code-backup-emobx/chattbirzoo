<?php

return [
    

    "email_registration_success" => "Account created. An OTP has been sent to your email address",
    "email_registration_email_exist" => "Email is already linked with another account please use any another email",

    "sms_registration_success" => "Account created. An OTP has been sent to your phone number",
    "sms_registration_email_exist" => "Phone number is already linked with another account please use any another phone number",
    "otp_verification_success" => "Your account is varified successfully.",
    "otp_expire" => "OTP is expired.",
    "otp_invalid" => "Invalid OTP.",
    "email_not_verified" => "Kindly verify your email address before login. Please check your email address.",
    "phone_not_verified" => "Kindly verify your phone number before login. Please check your phone.",
    "password_mismatch" => "Password mismatch.",
    "no_record_found" => "No record found .",
    "update_success" => "Updated successfully.",
    "delete_success" => "Deleted successfully.",
    "retrive_success" => "Retrieved successfully.",
    "add_success" => "Added successfully.",
    "logout" => "Logout successfully.",
    "session_expire" => "Your Session has expired. Please login again.",
    "welcome_user" => "Welcome :value ",
    "email_exist" => "Email is alredy been taken ",
    "number_exist" => "Phone number is alredy been taken ",
    "link_send_email" => "An email has been sent on your email address to reset password .",
    "link_send_phone" => "An SMS has been sent on your phone number to reset password.",
    "not_verified" => "Kindly verify your account before proceed",
    "finished_success" => "Finished successfully",
    "unfinished_success" => "Unfinished successfully",
    "check_email" => "The message has been sent to your e-mail address",
    "check_input" => "Check your input",
    "wait_for_allow" => "Since this is your first event, we'll need to confirm it. Don't worry, it won't be long!",
   
     


];
