@extends('layouts.base')

@section('content')

														
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Toolbar-->
						<div class="toolbar" id="kt_toolbar">
							<!--begin::Container-->
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Mapping List</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start mx-4"></span>
									<!--end::Separator-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="/" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
                                          <a href="/mapping" class="text-muted text-hover-primary">Mapping</a>
                                        </li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark">Mapping List</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
								<!--begin::Actions-->
								<div class="d-flex align-items-center py-1">
									
									<!--end::Wrapper-->
									<!--begin::Button-->
									<a href="/unmapped" class="btn btn-sm btn-light-primary">Show Unmapped Animals</a>
									<!--end::Button-->
								</div>
								<!--end::Actions-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Toolbar-->
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container">
								<!--begin::Card-->
								<div class="card">
									<!--begin::Card header-->
									<div class="card-header border-0 pt-6">
										<!--begin::Card title-->
										<div class="card-title">
											<!--begin::Search-->
											<div class="d-flex align-items-center position-relative my-1">
												<!--begin::Svg Icon | path: icons/duotone/General/Search.svg-->
												<!-- <span class="svg-icon svg-icon-1 position-absolute ms-6">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24" />
															<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
															<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
														</g>
													</svg>
												</span> -->
												<!--end::Svg Icon-->
												<!-- <input type="text" data-kt-customer-table-filter="search" class="form-control form-control-solid w-250px ps-15" placeholder="Search Customers" /> -->
											</div>
											<!--end::Search-->
										</div>
										<!--begin::Card title-->
										<!--begin::Card toolbar-->
										<div class="card-toolbar">
											<!--begin::Toolbar-->
											
											<!--end::Toolbar-->
											<!--begin::Group actions-->
											
											<!--end::Group actions-->
										</div>
										<!--end::Card toolbar-->
									</div>
									<!--end::Card header-->
									<!--begin::Card body-->
									<div class="card-body pt-0">
										<!--begin::Table-->
                                        <div class="container">
                             <div class="m-bottom m-top1">
<!--                               <h2 class="text-center">Gallery</h2>-->
                                 <button class="tablink" onclick="openPage('All', this, '#082c45')" id="defaultOpen" style="color:white;">All</button>
                                    <button class="tablink" onclick="openPage('Mapped', this, '#082c45')" style="color:white;">Mapped</button>
                                    <button class="tablink" onclick="openPage('Unmapped', this, '#082c45')" style="color:white;">Unmapped</button>


                                <div id="All" class="tabcontent" style="display:block;">
                                  <div class="row m-bottom1">
                                        <table class="table table-bordered align-middle table-row-dashed fs-6 gy-5" id="kt_customers_table">
											<!--begin::Table head-->
											<thead>
												<!--begin::Table row-->
												<tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
													<!-- <th class="w-10px pe-2">
														<div class="form-check me-3">
															<input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_customers_table .form-check-input" value="1" />
														</div>
													</th> -->
													
													<th class="min-w-125px">Id</th>
<!-- 													<th class="min-w-125px">Profile Picture</th> -->
													<th class="min-w-125px">Beacon Name</th>
													<th class="min-w-125px">Animal Name</th>
													<th class="min-w-125px">Action</th>
													
													
													
													<!-- <th class="min-w-125px">Created Date</th> -->
<!-- 													<th class="text-end min-w-70px">Actions</th> -->
												</tr>
												<!--end::Table row-->
											</thead>
											<!--end::Table head-->
											<!--begin::Table body-->
											<tbody class="fw-bold text-gray-600">
												@foreach($all as $list)
												<tr>
													<!--begin::Checkbox-->
<!-- 													<td>
														<div class="form-check form-check-sm form-check-custom form-check-solid">
															<input class="form-check-input" type="checkbox" value="1" />
														</div>
													</td> -->
													<!--end::Checkbox-->
                                                <td>
														<a href="" class="text-gray-600 text-hover-primary mb-1">{{ $list->beacon_id ?? ''}}</a>
													</td>
													<td>
														<a href="" class="text-gray-600 text-hover-primary mb-1">{{ $list->beacon_name ?? ''}}</a>
													</td>
                                                @php $str_animal='';
                                                $animal=array();
                                                @endphp
                                                @if(!empty($list->getAnimals))
                                                	@foreach($list->getAnimals as $animals)
                                                     
                                                		@php
                                                		$animal[]=$animals->animal_breed;
                                                        $str_animal=implode(',', $animal);
                                                		@endphp
                                                	@endforeach
                                                @endif   
													<td>
                                                    	
														<a href="#" class="text-gray-800 text-hover-primary mb-1">{{ $str_animal }}</a>
													</td>
													<td>
                                                    	
														<a href="javascript:void(0)" class="btn btn-sm btn-light-info" onclick="show_modal({{$list->beacon_id}});">Map</a>
													</td>
										
													<!--end::Action=-->
												</tr>
                         
												@endforeach
											</tbody>
											<!--end::Table body-->

											
										</table>
                                    
                               </div>
                             </div>

                 <div id="Mapped" class="tabcontent" style="display:none;">
                        <div class="row">
                        	<table class="table table-bordered align-middle table-row-dashed fs-6 gy-5" id="kt_customers_table">
											<!--begin::Table head-->
											<thead>
												<!--begin::Table row-->
												<tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
													<!-- <th class="w-10px pe-2">
														<div class="form-check me-3">
															<input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_customers_table .form-check-input" value="1" />
														</div>
													</th> -->
													
													<th class="min-w-125px">Id</th>
<!-- 													<th class="min-w-125px">Profile Picture</th> -->
													<th class="min-w-125px">Beacon Name</th>
													<th class="min-w-125px">Animal Name</th>
													<th class="min-w-125px">Action</th>
													
													
													<!-- <th class="min-w-125px">Created Date</th> -->
<!-- 													<th class="text-end min-w-70px">Actions</th> -->
												</tr>
												<!--end::Table row-->
											</thead>
											<!--end::Table head-->
											<!--begin::Table body-->
											<tbody class="fw-bold text-gray-600">
												@foreach($mapped as $list)
                                                  
												<tr>
													<!--begin::Checkbox-->
<!-- 													<td>
														<div class="form-check form-check-sm form-check-custom form-check-solid">
															<input class="form-check-input" type="checkbox" value="1" />
														</div>
													</td> -->
													<!--end::Checkbox-->
                                                <td>
														<a href="" class="text-gray-600 text-hover-primary mb-1">{{ $list->beacon_id ?? ''}}</a>
													</td>
													<td>
														<a href="" class="text-gray-600 text-hover-primary mb-1">{{ $list->beacon_name ?? ''}}</a>
													</td>
                                                @php $str_animal='';
                                                $animal=array();
                                                @endphp
                                                @if(!empty($list->getAnimals))
                                                	@foreach($list->getAnimals as $animals)
                                                     
                                                		@php
                                                		$animal[]=$animals->animal_breed;
                                                        $str_animal=implode(',', $animal);
                                                		@endphp
                                                	@endforeach
                                                @endif   
													<td>
                                                    	
														<a href="#" class="text-gray-800 text-hover-primary mb-1">{{ $str_animal }}</a>
													</td>
													
										<td>
                                                    	
														<a href="javascript:void(0)" class="btn btn-sm btn-light-info" onclick="show_modal({{$list->beacon_id}});">Map</a>
													</td>
										
													<!--end::Action=-->
												</tr>
         
												@endforeach
											</tbody>
											<!--end::Table body-->

											
										</table>
                         </div> 
                  </div>
                                
                 <div id="Unmapped" class="tabcontent" style="display:none;">
                        <div class="row">
                            
                        	<table class="table table-bordered align-middle table-row-dashed fs-6 gy-5" id="kt_customers_table">
											<!--begin::Table head-->
											<thead>
												<!--begin::Table row-->
												<tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
													<!-- <th class="w-10px pe-2">
														<div class="form-check me-3">
															<input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_customers_table .form-check-input" value="1" />
														</div>
													</th> -->
													
													<th class="min-w-125px">Id</th>
<!-- 													<th class="min-w-125px">Profile Picture</th> -->
													<th class="min-w-125px">Beacon Name</th>
													<th class="min-w-125px">Action</th>
<!-- 													<th class="min-w-125px">Animal Name</th> -->
													
													
													
													<!-- <th class="min-w-125px">Created Date</th> -->
<!-- 													<th class="text-end min-w-70px">Actions</th> -->
												</tr>
												<!--end::Table row-->
											</thead>
											<!--end::Table head-->
											<!--begin::Table body-->
											<tbody class="fw-bold text-gray-600">
												@foreach($unmapped as $list)
												<tr>
													<!--begin::Checkbox-->
<!-- 													<td>
														<div class="form-check form-check-sm form-check-custom form-check-solid">
															<input class="form-check-input" type="checkbox" value="1" />
														</div>
													</td> -->
													<!--end::Checkbox-->
                                                <td>
														<a href="" class="text-gray-600 text-hover-primary mb-1">{{ $list->beacon_id ?? ''}}</a>
													</td>
													<td>
														<a href="" class="text-gray-600 text-hover-primary mb-1">{{ $list->beacon_name ?? ''}}</a>
													</td>
                                                
													
										<td>
                                                    	
														<a href="javascript:void(0)" class="btn btn-sm btn-light-info" onclick="show_modal({{$list->beacon_id}});">Map</a>
													</td>
										
													<!--end::Action=-->
												</tr>
                                           
												@endforeach
											</tbody>
											<!--end::Table body-->

											
										</table>
                         </div> 
                  </div>
                             
                                    
                            
                                 
                                </div>
                             
                         </div>
										
										<!--end::Table-->
										
									</div>
									<!--end::Card body-->
								</div>
								<!--end::Card-->
								  </div>

                                                <!--end::Table container-->
                              </div>
                                            <!--begin::Body-->

                    </div>
                                        <!--end::Tables Widget 9-->

                   
                                            <div class="modal fade" id="kt_modal_create_app" tabindex="-1" aria-hidden="true">
			<!--begin::Modal dialog-->
			<div class="modal-dialog modal-dialog-centered mw-900px">
				<!--begin::Modal content-->
				<div class="modal-content">
					<!--begin::Modal header-->
					<div class="modal-header">
						<!--begin::Modal title-->
						<h2>Mapped with Animals</h2>
						<!--end::Modal title-->
						<!--begin::Close-->
						<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
							<!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
							<span class="svg-icon svg-icon-1">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
										<rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
										<rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
									</g>
								</svg>
							</span>
							<!--end::Svg Icon-->
						</div>
						<!--end::Close-->
					</div>
					<!--end::Modal header-->
                
					<!--begin::Modal body-->
					<div class="modal-body py-lg-10 px-lg-10">
						<!--begin::Stepper-->
						<div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_modal_create_app_stepper">
							<!--begin::Aside-->
							<form action="{{url('map_beacon')}}" class="form" method="post" >
                                @csrf
							<!--begin::Aside-->
							<!--begin::Content-->
							<div class="flex-row-fluid py-lg-5 px-lg-15">
								<!--begin::Form-->
								
									<!--begin::Step 1-->
                                <input type="hidden" name="beacon_id" id="beacon_id" value="">
									<div class="current" data-kt-stepper-element="content">
										<div class="w-100">
											<!--begin::Input group-->
											
											<div class="fv-row mb-10">
												
												<label class="d-flex align-items-center fs-5 fw-bold mb-2">
													<span class="required">Animals</span>
													<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify beacon name"></i>
												</label>
                                            
                                                <div id="list" class="dropdown-check-list">
  													<span class="anchor form-control form-control-lg form-control-solid " style="width: 460px;" >Select Animals</span>
  													<ul class="items form-control form-control-lg form-control-solid" id="show_animals">
<!--                                                         @php
                                                $animal= App\Models\Animals::all();
                                                @endphp
                                                @foreach($animal as $animal_list)
                                                 <li><input type="checkbox" class="checkSingle" onchange="save_value();" value="{{$animal_list->animal_id}}"/>{{$animal_list->animal_breed}} </li>
                                                 
                                                @endforeach -->
    												</ul>
												</div>
												
<!--                                                 <select class="form-control form-control-lg form-control-solid" name="animal_id[]" required multiple >
                                                <option>Choose Animals</option>
                                                @php
                                                $animal= App\Models\Animals::all();
                                                @endphp
                                                @foreach($animal as $animal_list)
                                                 @if($animal_list->beacon_id == $list->beacon_id)
                                                    <input type="checkbox" checked>
                                                	<option value="{{$animal_list->animal_id}}" selected>{{$animal_list->animal_breed}}</option>
                                                 @else
                                                    <input type="checkbox" >
                                                	<option value="{{$animal_list->animal_id}}">{{$animal_list->animal_breed}}</option>
                                                 @endif
                                                @endforeach
                                            	</select> -->
												
											</div>
										</div>
									</div>
									<!--end::Step 1-->
									
									
									
									
									<!--begin::Actions-->
									<div class="d-flex flex-stack pt-10">
										<!--begin::Wrapper-->
										
										<!--end::Wrapper-->
										<!--begin::Wrapper-->
										<div>
											
											<button type="submit" class="btn btn-lg btn-primary" >Save
											<!--begin::Svg Icon | path: icons/duotone/Navigation/Right-2.svg-->
											<span class="svg-icon svg-icon-3 ms-1 me-0">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24" />
														<rect fill="#000000" opacity="0.5" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1" />
														<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
													</g>
												</svg>
											</span>
											</button>
										</div>
										<!--end::Wrapper-->
									</div>
									<!--end::Actions-->
								
								
							</div>
							<!--end::Content-->
                            </form>
                        <!--end::Form-->
						</div>
						<!--end::Stepper-->
					</div>
					<!--end::Modal body-->
				</div>
				<!--end::Modal content-->
			</div>
			<!--end::Modal dialog-->
		</div>


                               	 

@endsection       


@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script> -->

<script>
function openPage(pageName,elmnt,color) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
    tablinks[i].style.color = color;
  }
  document.getElementById(pageName).style.display = "block";
  elmnt.style.backgroundColor = color;
  elmnt.style.color = 'white';
}

function show_list(input)
{
var checkList = document.getElementById('list');
checkList.getElementsByClassName('anchor')[0].onclick = function(evt) {
  if (checkList.classList.contains('visible'))
    checkList.classList.remove('visible');
  else
    checkList.classList.add('visible');
}
}

function show_modal(input)
{
	$('#kt_modal_create_app').modal('show');
	$('#beacon_id').val(input);
    show_list(input);
    $.ajax({
          method: 'POST', // Type of response and matches what we said in the route
          headers: {
                'X-CSRF-TOKEN': "{!! csrf_token() !!}"
            },
          url: "{{route('get_data')}}",
         // This is the url we gave in the route
          data: {'beacon_id' : input}, // a JSON object to send back
          success: function(response){ // What to do if we succeed
              console.log(response); 
            
            $('#show_animals').html(response.html);
            
          },
          error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
              console.log(JSON.stringify(jqXHR));
              console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
          }
        });
}

function save_value(input, beacon_input)
{
	// alert(input);
	if ($('.checkSingle').is(":checked")) {
            // var isAllChecked = 0;
        // $(".form_"+beacon_input).append('<input type="hidden" name="animals[]" value="'+$('.checkSingle_'+input).val()+'">');
//             $('.checkSingle_'+input).each(function() {
//                 if (!this.checked)
//                     isAllChecked = 1;
                    
//             });

            // if (isAllChecked == 0) {
            //     $("#checkedAll").prop("checked", true);
            // }     
        }
        else {
            $('.checkSingle').prop("checked", false);
        }
}

// Get the element with id="defaultOpen" and click on it
// window.onload=function(){
//   document.getElementById("defaultOpen").click();
// }

</script>
    <script>
$(document).ready(function() {
    // alert('hlo');
document.getElementById("defaultOpen").click();
// $('select').on('change', function() {
//   alert( this.value );
// });

});


</script>
 
<!-- <script type="text/javascript">
    function change_status(id,status)
  {
     // alert(status);
     var token = '{{ csrf_token() }}';
     if(status == 'active')
     {
       if(confirm("Are you sure you want to inactive."))
       { 
        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                type: "POST",
                url: "/change_status",
                data: {id:id,status:'inactive'},
                success: function(result){
                        console.log(result);  
                
               $('#show_status_'+id).html('<a href="javascript:void(0);" onclick="change_status("'+id+'","'+result.status+'");" class="label label-lg font-weight-bold label-inline label-light-danger">Inactive</a>');
                }
          });
        }
      }
      else
      {
      	if(confirm("Are you sure you want to active."))
        { 
           $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                type: "POST",
                url: "/change_status",
                data: {id:id,status:'active'},
                success: function(result){
                     console.log(result);    
$('#show_status_'+id).html('<a href="javascript:void(0);" onclick="change_status("'+id+'","'+result.status+'");" class="label label-lg font-weight-bold label-inline label-success">Active </a>');                }
          });
        }
      }
    
    }
</script> -->