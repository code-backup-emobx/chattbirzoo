@extends('layouts.base')

@section('content')

					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Toolbar-->
						<div class="toolbar" id="kt_toolbar">
							<!--begin::Container-->
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Admin Details</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start mx-4"></span>
									<!--end::Separator-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="{{route('home')}}" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">Admin</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark">Change Password</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
								<!--begin::Actions-->
								
								<!--end::Actions-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Toolbar-->
                    
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
                        
							<!--begin::Container-->
							<div id="kt_content_container" class="container">
								<!--begin::Layout-->
								<div class="d-flex flex-column ">
                                  @if(session()->has('success'))
                                            <div class="alert alert-success alert-dismissible fade show">
                                                {{ session()->get('success') }}
                                                 <button type="button" class="close" data-dismiss="alert" aria-label="close">
                                                    <span aria-hidden="true">&times;</span>
                                                 </button>
                                            </div>
                                             @endif
                                              @if(session()->has('failed'))
                                            <div class="alert alert-danger alert-dismissible fade show">
                                                {{ session()->get('failed') }}
                                                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                 </button>
                                            </div>
                                             @endif
                                              @if(session()->has('fail'))
                                            <div class="alert alert-danger alert-dismissible fade show">
                                                {{ session()->get('fail') }}
                                                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                 </button>
                                            </div>
                                             @endif
                                              @foreach($errors->all() as $error)
                                                <div class="alert alert-danger alert-dismissible fade show">
                                                   {{$error}}
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                           @endforeach
                                <!--begin::Sidebar-->
<!-- 									<div class="flex-column flex-lg-row-auto w-100 w-xl-400px mb-10"> -->
										<!--begin::Card-->
<!-- 										<div class="card mb-5 mb-xl-8"> -->
											<!--begin::Card body-->
<!-- 											<div class="card-body pt-15"> -->
												<!--begin::Summary-->
<!-- 												<div class="d-flex flex-center flex-column mb-5"> -->
                                
                                <div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<h3 class="card-title">
				Change password
			</h3>
		</div>
	</div>
	<!--begin::Form-->
	<form action="{{url('/admin/password')}}" class="form" method="post">
                                @csrf
									
                                
		<div class="card-body">
			<div class="form-group row mb-5">
				<label class="col-form-label text-right col-lg-3 col-sm-12">Old Password</label>
				<div class="col-lg-4 col-md-9 col-sm-12">
					<input class="form-control text-right" placeholder="Enter old password" type="password" value="" name="old_password" required/>
				</div>
			</div>
			
		</div>
        <div class="card-body">
			<div class="form-group row mb-5">
				<label class="col-form-label text-right col-lg-3 col-sm-12">New Password</label>
				<div class="col-lg-4 col-md-9 col-sm-12">
					<input class="form-control text-right" placeholder="Enter new password" type="password" value="" name="new_password" required/>
				</div>
			</div>
			
		</div>
        <div class="card-body">
			<div class="form-group row mb-5">
				<label class="col-form-label text-right col-lg-3 col-sm-12">Confirm Password</label>
				<div class="col-lg-4 col-md-9 col-sm-12">
					<input class="form-control text-right" placeholder="Re-enter new password" type="password" value="" name="confirm_password" required/>
				</div>
			</div>
			
		</div>
		<div class="card-footer">
			<div class="row">
				<div class="col-lg-9 ml-lg-auto">
					<button type="submit" class="btn btn-primary mr-2">Submit</button>
					<a onclick="history.go(-1)" class="btn btn-secondary">Cancel</a>
				</div>
			</div>
		</div>
	</form>
	<!--end::Form-->
</div>
													
<!-- 												</div> -->
												<!--end::Summary-->
												<!--begin::Details toggle-->
												
												<!--end::Details toggle-->
												
												<!--end::Details content-->
<!-- 											</div> -->
											<!--end::Card body-->
<!-- 										</div> -->
										<!--end::Card-->
										<!--begin::Connected Accounts-->
										
											
									<!--end::Connected Accounts-->
<!-- 									</div> -->
									<!--end::Sidebar-->
									<!--begin::Content-->
									
									<!--end::Content-->
                                    
								</div>
								<!--end::Layout-->
								<!--begin::Modals-->
								
								<!--end::Modals-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Post-->
					</div>
					<!--end::Content-->
                    
					@endsection                

@section('scripts')

									