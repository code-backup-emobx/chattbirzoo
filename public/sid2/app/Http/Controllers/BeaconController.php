<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Beacon;
use App\Models\Section;
use App\Models\SectionMedia;
use DB;
use Validator;
class BeaconController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {  
        $response['lists']=Beacon::where('status','active')->orderBy('created_at', 'DESC')->paginate(15);
	   
            return view('beacons.list',$response);
        
    }

    public function view(Request $request , $id)
    {   
        $response['details']=Beacon::where('beacon_id',$id)->first();
        
        return view('beacons.view',$response);
        
    }

     public function update(Request $request)
    {   
       $validator = Validator::make($request->all(), [
                    
                    'name' => 'required',
                   'major_id' => 'required|integer|gt:0',
                   
                   'minor_id' => 'required|integer|gt:0',
                   
                   
                    
                   
                    
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        $id=$request->input('id');
        $beacon_name=$request->input('name');
        $major_id=$request->input('major_id');
        $minor_id=$request->input('minor_id');
        
        
        $update=Beacon::where('beacon_id', $id)->first();
        $update->beacon_name=$beacon_name;
        $update->major_id=$major_id;
        $update->minor_id=$minor_id;
        $update->save();
        
        return redirect()->back()->with('success','Successfully updated');
        
    }



    public function create(Request $request)
    {   
        // return $request->all();
        $validator = Validator::make($request->all(), [
                    
                    'name' => 'required',
                   'major_id' => 'required|integer|gt:0',
                   
                   'minor_id' => 'required|integer|gt:0',
                   
                   
                    
                   
                    
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }

        // return $request->all();
        // return $request->media;
        $beacon_name=$request->input('name');
        $major_id=$request->input('major_id');
        $minor_id=$request->input('minor_id');
        

        $code = generateRandomString(4); 
        $create=new Beacon();
        $create->major_id=$major_id;
        $create->minor_id=$minor_id;
        $create->beacon_name=$beacon_name.$code;
        $create->save();

        return back()->with('success','Beacon added Successfully');
        
    }

    public function status($id ,$status)
    {

        DB::enableQueryLog();
        $beacon=Beacon::where('beacon_id', $id)->first();
        if($status=='active')
        {
            
          $beacon->status='inactive';
        }
        else
        {
          $beacon->status='active';
        }
        $beacon->save();
       //return DB::getQueryLog();
        return redirect()->back()->with('success', 'Status Changed Successfully.');
    }
}
