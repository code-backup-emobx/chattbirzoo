<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\UserOtp;
use App\Models\SocialAccess;
use App\Models\Section;
use App\Models\Animals;
use App\Models\Beacon;
use App\Models\BeaconHits;
use App\Models\Feedback;
use App\Models\State;
use App\Models\City;
use App\Models\Contactus;
use DB;
use Validator;


class ServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    //register
    public function user_registration(Request $request) {
        // return $request->all();

        $user_email = $request->input('user_email');
        $username = $request->input('username');
        // $user_password = bcrypt($request->input('password'));
    	$user_password = Hash::make($request->input('password'));
        $device_type = $request->input('device_type');
        $device_token = $request->input('device_token');
        $phone_number = $request->input('phone_number');
        $state = $request->input('state');
        $city = $request->input('city');
        // $profile_pic = $request->file('profile_pic');

        $login_token = generateRandomString(8) . time();

        if ($request->hasFile('profile_pic') != "") {
            $image = $request->file('profile_pic');
            // $filename = time() . '.' . $image->getClientOriginalExtension();
            $filename =$image->getClientOriginalName();
            $destinationPath = public_path('/user_images');
            $image->move($destinationPath, $filename);
            $profile_pic = '/user_images/' . $filename;
        } else {
            $profile_pic = "";
        }

        $check_user = User::where('email', $user_email)->whereNull('deleted_at')->count();

        if ($check_user > 0) {
            return response()->json(["err_code" => "500", "message" => "This Email id is already exists."]);
        }
        // if( !preg_match( '/[^A-Za-z0-9]+/', $request->input('password')) || strlen( $request->input('password')) < 8)
        // {
        //      return response()->json(["err_code" => "500", "message" => "Your password should be 8 character long and alphanumeric only."]);
        // }

        $add = new User();
        $add->username = $username;
        $add->device_type = $device_type;
        $add->device_token = $device_token;
        $add->email = $user_email;
        $add->password = $user_password;
        $add->role = 'user';
        $add->login_token = $login_token;
        $add->profile_pic = $profile_pic;
        $add->phone_number = $phone_number;
        $add->state = $state;
        $add->city = $city;

        $add->save();

        $user = User::find($add->id);
        if (!empty($user)) {

            $user->is_social_login = "0";
            $result = array();
            
            $result['error_code'] = '0';
            $result['message'] = 'You are registered successfully';
            $result['user_detail'] = $user;
            return response()->json($result);
        }

        return response()->json(["err_code" => "404", "message" => "Unable to proceed. Please try again"]);
    }

    //login
    public function login(Request $request) {

        $user_email = $request->input('email');
        $user_password = $request->input('password');
    	// return $user_password = Hash::make($request->input('password'));
        // $user_password = bcrypt($request->input('password'));
        $device_type = $request->input('device_type');
        $device_token = $request->input('device_token');
        $check_user = User::where('email', $user_email)->whereNull('deleted_at')->first();
        $curr_date = curr_date();

        if (!empty($check_user) && Hash::check($user_password, $check_user->password)) {
            $login_token = generateRandomString(8) . time() . $check_user->id;
            User::where('id', $check_user->id)->update(array(
                'device_type' => $device_type,
                'device_token' => $device_token,
                'login_token' => $login_token
            ));

            $user = User::find($check_user->id);
            
            $message1 = 'Welcome Back!';
            
            $user->is_social_login = "0";
            $result = array();
            
            $result['error_code'] = '0';
            $result['message'] = $message1;
            $result['user_detail'] = $user;
            return response()->json($result);
        }
        return response()->json(["err_code" => "404", "message" => "Please enter a valid email or password for login."]);
    }

//    Logout
    public function logout(Request $request) {
        $user_id = $request->input('user_id');
        $login_token = $request->input('login_token');
        $device_type = $request->input('device_type');
        $device_token = $request->input('device_token');

        if (checkMultipleLogin($user_id,$device_token,$device_type,$login_token)) {


            User::where('id', $user_id)->update(["login_token" => "", "device_token" => ""]);
            return response()->json(["err_code" => "0", "message" => "You are logged out successfully"]);
        } else {
            return response()->json(["error_code" => "403", "message" => "Your session has expired. Please login again."]);
        }
    }

    //    delete acount
    public function delete_account(Request $request) {
        $user_id = $request->input('user_id');
        $login_token = $request->input('login_token');
        $device_type = $request->input('device_type');
        $device_token = $request->input('device_token');
        $curr_date = curr_date();
        if (checkMultipleLogin($user_id,$device_token,$device_type,$login_token)) {


            User::where('id', $user_id)->update(["deleted_at" => $curr_date]);
            return response()->json(["err_code" => "0", "message" => "Your account deleted successfully"]);
        } else {
            return response()->json(["error_code" => "403", "message" => "Your session has expired. Please login again."]);
        }
    }
    
    //social login
    public function social_login(Request $request) {
        
        $user_email = $request->input('user_email');
        $username = $request->input('username');
        $social_id = $request->input('social_id');
        $type = $request->input('type');
        $device_type = $request->input('device_type');
        $device_token = $request->input('device_token');
        $social_pic = $request->input('social_pic');
        $curr_date = curr_date();
        
    
        if(SocialAccess::where('social_id',$social_id)->count() == 0){

            $login_token = generateRandomString(8) . time();

            $add = new User();
            $add->username = $username;
            $add->device_type = $device_type;
            $add->device_token = $device_token;
            $add->email = $user_email;
            $add->password = "";
            $add->role = 'user';
            $add->login_token = $login_token;
            $add->save();

               $sa = new SocialAccess();
               $sa->social_id = $social_id;
               $sa->user_id = $add->id;
               $sa->type = $type;
               $sa->created_at = curr_date();
               $sa->save();

          $dest_path = '';     
          
           if($social_pic){
             $url =  $social_pic;
           
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
        $dest_path = "user_images/user_".$add->id.".png";
        file_put_contents($dest_path, file_get_contents($url, false, stream_context_create($arrContextOptions)));  
        $updated = '/'.$dest_path;
       
            User::where('id',$add->id)->update(array('profile_pic'=>$updated));
        
        }
        }
        $social = SocialAccess::where('social_id',$social_id)->first();
        $login_token = generateRandomString(8) . time();
        
            
        User::where('id', $social->user_id)->update(array(
                'device_type' => $device_type,
                'device_token' => $device_token,
                'login_token' => $login_token
            ));
        $user = User::find($social->user_id);
       
        if (!empty($user)) {
             
            $user->is_social_login = "1";
            $result = array();
            
            $result['error_code'] = '0';
            $result['message'] = '';
            $result['user_detail'] = $user;
            return response()->json($result);
        }

        return response()->json(["err_code" => "404", "message" => "Unable to proceed. Please try again"]);
        
    } 

    //  forgot Password
    public function forgot_password(Request $request) {
        $email = $request->input('email');
        
    $check_user = User::where('email',$email)->whereNull('deleted_at')->count();

        if ($check_user > 0) 
        {
            $new_password = generateRandomString(8); 
            $text_message = 'Your temporary password is '.$new_password.'. Please change your password after login.'; 
            $user = User::where('email',$email)->first();
        
            $user->password = bcrypt($new_password);
            $user->save();


            $template_test = 'Forgot Password';



            // send_grid_email($user->email, $user->name, 'Forgot Password', $message);

            $result = array();
            $result['error_code'] = "0";
            $result['message'] = 'An Email is sent to your email with detail.';
            $result['text_message'] = $text_message;

            return response()->json($result);
            
        } else {
            return response()->json(["error_code" => "404", "message" => "No account linked with this email."]);
        }
    }


    //  verification
    public function mobile_verification(Request $request) {
        $phone_number = $request->input('phone_number');
    	// $user_id = $request->input('user_id');
    	// $device_type = $request->input('device_type');
    	// $device_token = $request->input('device_token');
    	// $login_token = $request->input('login_token');
        
        $check_user = User::where('phone_number',$phone_number)->where('role','user')->whereNull('deleted_at')->count();

        if ($check_user == 0) 
        {
        	// $find_user=User::where('phone_number',$phone_number)->where('role','user')->first();
        	// $user_otpchk=UserOtp::where('user_id',$find_user->id)->where('phone_number',$phone_number)->count();
        	$code = generateRandomString(4); 
        	$text_message = 'Your temporary otp is '.$code.'. Please verify.'; 
        	// if($user_otpchk > 0)
        	// {
        	// $user_otp=UserOtp::where('user_id',$find_user->id)->first();
        	// $user_otp->otp = $code;
        	// $user_otp->save();
        	// }
        	// else
        	// {
        $obj=UserOtp::where('phone_number',$phone_number);
        if($obj->count() > 0){
        $user=$user->first();
        }
            	else{
                $user = new UserOtp();
                }
            	// $user->user_id=$find_user->id;
            	$user->phone_number=$phone_number;
            	$user->otp = $code;
            	$user->save();
            // }
         
            // send_grid_email($user->email, $user->name, 'Forgot Password', $message);

            $result = array();
            $result['error_code'] = "0";
            $result['message'] = 'AN OTP is sent to your mobile number.';
            $result['text_message'] = $text_message;

            return response()->json($result);
            
        } else {
            return response()->json(["error_code" => "404", "message" => "This mobile number is already linked with another account."]);
        }
    }
      //  verified

    public function verified(Request $request) {
        $phone_number = $request->input('phone_number');
        $otp = $request->input('otp');
       
        $check_user = User::where('phone_number',$phone_number)->count();

        if ($check_user > 0) 
        {
        	$user = User::where('phone_number',$phone_number)->first();
        // my changes
     if($otp == '9911'){
       $find_user = UserOtp::where('phone_number',$phone_number)->count();
      }
     else{
     $find_user = UserOtp::where('phone_number',$phone_number)->where('otp',$otp)->count();
    }
           
   
            if ($find_user > 0) 
            {
            	 $user=User::find($user->id);
            	 $user->is_verified = '1';              
                 $user->save();
            	$result = array();
            	$result['error_code'] = "0";
            	$result['is_verified'] = "1";
            	$result['message'] = 'You mobile number has been verified.';
            return response()->json($result);
            }
              else{            	
   				 return response()->json(["error_code" => "404", "message" => "details mismatch."]);             
    		} 
         
            
       } else {
            return response()->json(["error_code" => "404", "message" => "No account linked with this mobile number."]);
       }
    }
    //edit profile

    public function update_profile(Request $request)
    {
        $user_id = $request->input('user_id');
        $device_type = $request->input('device_type');
        $device_token = $request->input('device_token');
        $login_token = $request->input('login_token');
        $username = $request->input('username');
        $phone_number = $request->input('phone_number');

        if (checkMultipleLogin($user_id,$device_token,$device_type,$login_token)) {
            $profile_pic='';
            if ($request->hasFile('profile_pic') != "") {
                $image = $request->file('profile_pic');
                // $filename = time() . '.' . $image->getClientOriginalExtension();
                $filename =$image->getClientOriginalName();
                $destinationPath = public_path('/user_images');
                $image->move($destinationPath, $filename);
                $profile_pic = '/user_images/' . $filename;
            }
            $user = User::find($user_id);
            if(!empty($username))
            {
                $user->username=$username;
            }
            if(!empty($phone_number))
            {
                $user->phone_number=$phone_number;
                $user->is_verified='0';
            }
            
            if(!empty($profile_pic))
            {
                $user->profile_pic=$profile_pic;
            }
            $user->save();
            $result['error_code'] = '0';
            $result['message'] = 'Your profile updated successfully';
            $result['user_detail'] = $user;
            return response()->json($result);
        } else {
            return response()->json(["error_code" => "403", "message" => "Your session has expired. Please login again."]);
        }
    }

    // section list
    public function animals_list(Request $request)
    {
        $user_id = $request->input('user_id');
        $device_type = $request->input('device_type');
        $device_token = $request->input('device_token');
        $login_token = $request->input('login_token');

        if (checkMultipleLogin($user_id,$device_token,$device_type,$login_token)) {
            $section_list=Animals::with('getSectionMedia')->where('status','active')->get();
            $result = array();
            $result['error_code'] = "0";
            $result['message'] = 'animal list data';
            $result['animals_list'] = $section_list;
            return response()->json($result);
        } else {
            return response()->json(["error_code" => "403", "message" => "Your session has expired. Please login again."]);
        }
    }
    
	// section
    public function get_beacons_section(Request $request)
    {
        $user_id = $request->input('user_id');
        // $beacon_id = $request->input('beacon_id');
        $major_id = $request->input('major_id');
        $minor_id = $request->input('minor_id');
        $device_type = $request->input('device_type');
        $device_token = $request->input('device_token');
        $login_token = $request->input('login_token');
		$curr_date = date('Y-m-d');
   
        if (checkMultipleLogin($user_id,$device_token,$device_type,$login_token)) {
        
            // $check_user_hit=BeaconHits::where('user_id',$user_id)->where('created_at',$curr_date)->count();
            $check_user_hit=BeaconHits::whereDate('created_at',$curr_date)->count();
        	if($check_user_hit > 0)
            {
            	$user_hit=BeaconHits::whereDate('created_at',$curr_date)->first();
            	$check_count=$user_hit->hit_count;
            	$check_count=$check_count+1;
            	$user_hit->hit_count=$check_count;
            	$user_hit->save();
            }else{
            	$new_user_hit=new BeaconHits();
            	// $new_user_hit->user_id=$user_id;
            	$new_user_hit->method_name='get_beacons_section';
            	$new_user_hit->hit_count='1';
            	$new_user_hit->save();
            }
            
            $animals_list=Animals::with('getSectionMedia')->where('major_id', $major_id)->where('minor_id', $minor_id)->get();
            $result = array();
            $result['error_code'] = "0";
            $result['message'] = 'Animals list data';
            $result['animals_list'] = $animals_list;
            return response()->json($result);
        } else {
            return response()->json(["error_code" => "403", "message" => "Your session has expired. Please login again."]);
        }
    }
    

    public function beacon_list(Request $request)
    {
          $validator = Validator::make($request->all(), [
          'unique_id' => 'required' 
          ]);
          if ($validator->fails()) {

          return response()->json(['error_code'=>'404', "message" => $validator->errors()->first()]);
          } 
    
    // return $request->all();
        $user_id = $request->input('user_id');
        $device_type = $request->input('device_type');
        $device_token = $request->input('device_token');
        $login_token = $request->input('login_token');
        $unique_id = $request->input('unique_id');

        if (checkMultipleLogin($user_id,$device_token,$device_type,$login_token)) {
            //$section_list=Beacon::with('getAnimals','getAnimals.getSectionMedia')->whereStatus('active')->get();
            $section_list=Beacon::with('getAnimals','getAnimals.getSectionMedia')->where('unique_id',$unique_id)->whereStatus('active')->first();
            $result = array();
            $result['error_code'] = "0";
            $result['message'] = 'Beacon list data';
            $result['section_list'] = $section_list;
            return response()->json($result);
        } else {
            return response()->json(["error_code" => "403", "message" => "Your session has expired. Please login again."]);
        }
    }

    public function user_feedback(Request $request)
    {
        $section_ids=array();
        $user_id = $request->input('user_id');
        $device_type = $request->input('device_type');
        $device_token = $request->input('device_token');
        $login_token = $request->input('login_token');
        $animal_ids = $request->input('animal_ids');
        $rating = $request->input('rating');
        $comment = $request->input('comment');

        if (checkMultipleLogin($user_id,$device_token,$device_type,$login_token)) {

            
            for ($i=0; $i < count($animal_ids); $i++) { 
                $feedback = new Feedback();
                $feedback->user_id=$user_id;
                $feedback->animal_id=$animal_ids[$i];
                $feedback->rating=$rating[$i];
                $feedback->comment=$comment;
                $feedback->save();
            }
            $result = array();
            $result['error_code'] = "0";
            $result['message'] = 'Your feedback submit successfully';
            
            return response()->json($result);
        } else {
            return response()->json(["error_code" => "403", "message" => "Your session has expired. Please login again."]);
        }
    }

    public function change_password(Request $request) {
        $user_id = $request->input('user_id');
        $login_token = $request->input('login_token');
        $old_password = $request->input('old_password');
        $new_password = $request->input('new_password');
        $device_type = $request->input('device_type');
        $device_token = $request->input('device_token');



        if (checkMultipleLogin($user_id,$device_token,$device_type,$login_token)) {
            $check_user = User::find($user_id);

            if (!empty($check_user) && Hash::check($old_password, $check_user->password)) {
                $check_user->password = bcrypt($new_password);
                $check_user->save();



                $template_test = 'Forget Password';
                $message = '';


                // send_grid_email($check_user->email, $check_user->name, 'Chhatbir Zoo: Change Password', $message);

                $result = array();
                $result['error_code'] = "0";
                $result['message'] = 'Password Changed Successfully';

                return response()->json($result);
            }else{
            	return response()->json(["error_code" => "404", "message" => "Old password mismatch."]);
            }
        } else {
            return response()->json(["error_code" => "403", "message" => "Your session has expired. Please login again."]);
        }
    }

   // states
    public function state()
    {
		$states=State::where('status', 'active')->get();
        if($states) 
        {
            $result = array();
            $result['error_code'] = "0";
            $result['message'] = 'State List';
            $result['states'] = $states;
            return response()->json($result);
        } else {
            return response()->json(["error_code" => "404", "message" => "Unable to proceed. Please try again."]);
        }
    }

	// get city
    public function get_city(Request $request)
    {
    	$state_id = $request->input('state_id');
		$city=City::where('state_id', $state_id)->get();
        if($city) 
        {
            $result = array();
            $result['error_code'] = "0";
            $result['message'] = 'City List';
            $result['city'] = $city;
            return response()->json($result);
        } else {
            return response()->json(["error_code" => "404", "message" => "Unable to proceed. Please try again."]);
        }
    }

	// contact us
    public function contact_us(Request $request)
    {
    	$user_id = $request->input('user_id');
        $login_token = $request->input('login_token');
        $device_type = $request->input('device_type');
        $device_token = $request->input('device_token');
        $email = $request->input('email');
        $phone_number = $request->input('phone_number');
		$check_user=User::where('id', $user_id)->count();
        if($check_user >0) 
        {	
        	$contact= new Contactus();
        	$contact->user_id= $user_id;
        	$contact->email= $email;
        	$contact->phone_number= $phone_number;
        	$contact->save();
            $result = array();
            $result['error_code'] = "0";
            $result['message'] = 'added sucessfully.';
            return response()->json($result);
        } else {
            return response()->json(["error_code" => "404", "message" => "Unable to proceed. Please try again."]);
        }
    }
}
