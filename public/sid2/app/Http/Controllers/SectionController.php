<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
// use App\Models\Section;
use App\Models\Animals;
use App\Models\Beacon;
use App\Models\SectionMedia;
use DB;
use Validator;
use Session;
class SectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {  
            $response['lists']=Animals::with('getSectionMedia')->orderBy('created_at', 'DESC')->paginate(15);
	        $response['beacon_list']=Beacon::where('status','active')->get();
            return view('section.list',$response);
        
    }

    public function view(Request $request , $id)
    {   
        $response['details']=Animals::with('getSectionMedia')->where('animal_id',$id)->first();
        
        return view('section.view',$response);
        
    }

     public function gallery_list(Request $request , $id)
    {   
        $response['details']=Animals::with('getSectionMedia')->where('animal_id',$id)->first();
        
        return view('section.gallery',$response);
        
    }
     
    public function gallery_create(Request $request, $id)
    {   
        $response['details']=Animals::with('getSectionMedia')->where('animal_id',$id)->first();
        
        return view('section.create_gallery',$response);
        
    }

    public function gallery_store(Request $request)
    {   
        // return $request->all();
        $validator = Validator::make($request->all(), [
                    
                   'media.*' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
       $delete_file=array();
     	$animal_id=$request->input('animal_id');
     	$media_file=$request->file('media');
     	$delete_file=$request->input('delete_file');
        
        $media=array();
        $media_type=array();
        if ($request->hasFile('media') != "") {
            foreach($request->file('media') as $key=>$file)
            {
                
                $name=$file->getClientOriginalName();  
                if(!empty($delete_file)){
                if (!in_array($name ,$delete_file )) {
  						// No values from array1 are in array 2
                $extension=$file->getClientOriginalExtension();  
                // $ext = pathinfo($file, PATHINFO_EXTENSION);  
                $file->move(public_path().'/section_media/', $name);      
                $media[] = '/section_media/'.$name;  
                $media_type[] = $extension;  
            	}
                }else{
                	$extension=$file->getClientOriginalExtension();  
                	// $ext = pathinfo($file, PATHINFO_EXTENSION);  
                	$file->move(public_path().'/section_media/', $name);      
                	$media[] = '/section_media/'.$name;  
                	$media_type[] = $extension;
                }
        	}
        }

        // return $media;
       if(empty($media)){
       		return array("msg"=>"No media found"); 
       }else{
    	for($i=0;$i<count($media);$i++)
        {
            $add = new SectionMedia();
            $add->animal_id=$animal_id;
            $add->media_url=$media[$i];
        	if($media_type[$i] == 'mp4' || $media_type[$i] == 'mp3')
            {
            	$add->media_type='video';
            }else{
            	$add->media_type='image';
            }
        	$add->save();
        }
       }
        
        return array("msg"=>"Media added Successfully"); 
        // return back()->with('success','Media added Successfully');
        
    }

	 public function gallery_edit(Request $request , $id)
    {   
        $response['details']=SectionMedia::where('media_id',$id)->first();
        
        return view('section.gallery_edit',$response);
        
    }

     public function edit_media(Request $request)
    {   
     	$validator = Validator::make($request->all(), [
                    
                   'media' => 'required|max:2048',
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
     	$media_id=$request->input('media_id');
        $media='';
        $media_type='';

            if ($request->hasFile('media') != "")
            {   
            	$file=$request->file('media');
                $name=$file->getClientOriginalName();  
                $extension=$file->getClientOriginalExtension();  
                // $ext = pathinfo($file, PATHINFO_EXTENSION);  
                $file->move(public_path().'/section_media/', $name);      
                $media = '/section_media/'.$name;  
                $media_type= $extension;  
            }
     // return $media_type;
      $update=SectionMedia::where('media_id',$media_id)->first();
        if($media_type== 'mp4' || $media_type == 'mp3')
            {
            	$update->media_type='video';
            }else{
            	$update->media_type='image';
            }
        $update->media_url=$media;
        $update->save();
        
        return redirect()->back()->with('success','Successfully updated');
        
    }
      
      
    public function gallery_delete(Request $request , $id)
    {   
        $details=SectionMedia::where('media_id',$id)->delete();
        
        return redirect()->back()->with('success','Successfully deleted');
        
    }

     public function update(Request $request)
    {   
     
     // return $request->all();
     $validator = Validator::make($request->all(), [
                    
                    'animal_breed' => 'required',
                    'animal_name' => 'required',
                   'description' => 'required',
                   'start_time' => 'required',
                   'end_time' => 'required',
                   // 'beacon' => 'required',
                   
                   // 'media.*' => 'required',
                   
                   
                    
                   
                    
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
     
     
        $id=$request->input('animal_id');
        $animal_breed=$request->input('animal_breed');
        $animal_name=$request->input('animal_name');
        $description=$request->input('description');
        $start_time=$request->input('start_time');
        $end_time=$request->input('end_time');
        
        $update=Animals::where('animal_id', $id)->first();
        $update->animal_breed=$animal_breed;
        $update->animal_name=$animal_name;
        $update->description=$description;
        $update->feeding_time_start=$start_time;
        $update->feeding_time_end=$end_time;
        $update->save();
     	return redirect()->back()->with('success','Successfully updated');
        
    }



    public function create(Request $request)
    {   
        // return $request->all();
        $validator = Validator::make($request->all(), [
                    
                    'animal_breed' => 'required|max:200',
                    'animal_name' => 'required|max:200',
                   'description' => 'required',
                   'start_time' => 'required',
                   'end_time' => 'required',
                   // 'beacon' => 'required',
                   
                   'media.*' => 'required|max:2048',
                   
                   
                    
                   
                    
        ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }

        // return $request->all();
        // return $request->media;
        $animal_breed=$request->input('animal_breed');
        $animal_name=$request->input('animal_name');
        $description=$request->input('description');
        // $beacon_id=$request->input('beacon');
        $start_time=$request->input('start_time');
        $end_time=$request->input('end_time');
        // $feeding_time=$start_time.'-'.$end_time;
        $media=array();
        $media_type=array();

            foreach($request->file('media') as $key=>$file)
            {
                $name=$file->getClientOriginalName();  
                $extension=$file->getClientOriginalExtension();  
                // $ext = pathinfo($file, PATHINFO_EXTENSION);  
                $file->move(public_path().'/section_media/', $name);      
                $media[] = '/section_media/'.$name;  
                $media_type[] = $extension;  
            }
        

        // return $media_type;
        $create=new Animals();
        $create->animal_breed=$animal_breed;
        $create->animal_name=$animal_name;
        $create->description=$description;
        // $create->beacon_id=$beacon_id;
        $create->feeding_time_start=$start_time;
        $create->feeding_time_end=$end_time;
        $create->save();

        for($i=0;$i<count($media);$i++)
        {
            $add = new SectionMedia();
            $add->animal_id=$create->animal_id;
            $add->media_url=$media[$i];
        	if($media_type[$i] == 'mp4' || $media_type[$i] == 'mp3')
            {
            	$add->media_type='video';
            }else{
            	$add->media_type='image';
            }
        	$add->save();
        }
        
        
        return back()->with('success','Animal added Successfully');
        
    }

    public function status($id ,$status)
    {

        DB::enableQueryLog();
        $update=Animals::where('animal_id', $id)->first();
        if($status=='active')
        {
            
          $update->status='inactive';
        }
        else
        {
          $update->status='active';
        }
        $update->save();
       //return DB::getQueryLog();
        return redirect()->back()->with('success', 'Status Changed Successfully.');
    }

    public function mapping(Request $request)
    {   
       $all=Beacon::with('getAnimals')->get();
        $mapped_value=array();
        $get_mapped=Animals::whereNotNull('beacon_id')->distinct()->get(['beacon_id']);
        foreach($get_mapped as $list)
        {
        	$mapped_value[]=$list->beacon_id;
        }
    // return $mapped_value;
        $mapped=Beacon::with('getAnimals')->WhereIn('beacon_id', $mapped_value)->get();
        $unmapped=Beacon::with('getAnimals')->whereNotIn('beacon_id', $mapped_value)->get();
    	// $animals=Animals::all();
        $response['all']=$all;
        $response['mapped']=$mapped;
        $response['unmapped']=$unmapped;
        // $response['animals']=$animals;
        // foreach($animals as $animal_list)
        // {
        // 	return $animal_list->animal_breed;
        // }
        return view('mapping.list',$response);
        
    }

	public function unmapped(Request $request)
    {   
       
        $response['lists']=Animals::with('getSectionMedia')->WhereNull('beacon_id')->get();
        $response['beacon_list']=Beacon::where('status','active')->get();
        return view('mapping.unmapped',$response);
        
    }

	public function beacon_map(Request $request)
    {   
       // return $request->all();
       $validator = Validator::make($request->all(), [
                    
                    'animal_id' => 'required',
                    'beacon_id' => 'required',
      ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
       $animal_id=$request->input('animal_id');
       $beacon_id=$request->input('beacon_id');
       $get_animal=Animals::where('animal_id', $animal_id)->first();
       $get_animal->beacon_id=$beacon_id;
       $get_animal->save();
       return redirect()->back()->with('success', 'Animal mapped Successfully.');
        
    }

    public function map_beacon(Request $request)
    {   
       // return $request->all();
       $validator = Validator::make($request->all(), [
                    
                    //'animal_id.*' => 'required',
                    'beacon_id' => 'required',
      ]);

        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }
       
       $animal_id=$request->input('animal_id');
       $beacon_id=$request->input('beacon_id');
       $update_beacon=Animals::where('beacon_id', $beacon_id)->update(["beacon_id"=>0]);
       if($animal_id){
       foreach($animal_id as $list)
       {
       		$update=Animals::where('animal_id', $list)->first();
            $update->beacon_id=$beacon_id;
            $update->save();
       }
       }
       
       return redirect()->back()->with('success', 'Animal mapped Successfully.');
    }

    public function get_data(Request $request)
    {   
       $beacon_id=$request->input('beacon_id');
       $data=Animals::where('status', 'active')->get();
       $html='';
       foreach($data as $list)
       {
       		if($list->beacon_id == $beacon_id)
            {
       			$html.='<li><input type="checkbox" class="checkSingle" name="animal_id[]" value="'.$list->animal_id.'" checked/>'.$list->animal_breed.' </li>';
            }else{
            	$html.='<li><input type="checkbox" class="checkSingle" name="animal_id[]" value="'.$list->animal_id.'"/>'.$list->animal_breed.' </li>';
            }
       		
       }
        
       return array('html'=>$html);
        
    }
}
