<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Section;
use App\Models\Animals;
use App\Models\BeaconHits;
use DB;
use Validator;
use Carbon\Carbon;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {  
        $lists=User::whereRole('user');
        // $response['allusers_wd']=$lists_->withTrashed()->count();    
        $response['allusers']=$lists->count();
        $lists=$lists->whereStatus('active');
        $response['activeusers']=$lists->count();
        $response['lists']=$lists->get();
        $response['tendays']= $this->UserCountLastTenDays();
      // return $last_30_days = User::where('role','user')->where('created_at','>=',Carbon::now()->subdays(30))->count();
        // return $response;
       $currentdate = date('Y-m-d');
       $response['total_hit']=BeaconHits::sum('hit_count');
        $check_hit=BeaconHits::whereDate('created_at',  $currentdate)->get(['hit_count','created_at']);
       if(isset($check_hit)){
       		$check_hit[]=["hit_count"=>"0", "current_date"=> date('d M Y')];
       		$response['today_beacons_hit']=$check_hit;
       		$response['current_date']=date('d M Y');
       }else{
       $response['today_beacons_hit']=$check_hit;
       $response['current_date']=date('d M Y');
       }
    // return $response['today_beacons_hit'];
  	   // $get_seven_days = Carbon::now()->subDays(7);
  	   // $response['last_seven_days_hit'] = BeaconHits::whereDate('created_at', '>=', $get_seven_days)->get(['hit_count','created_at']);
      $response['last_seven_days_hit'] = $this->CountLastSevenDays();
      $response['four_weeks_hit'] = $this->CountLastFourWeeks();
        return view('home', $response);
    }

    public function UserCountLastTenDays(){
        $lastTenDates[]= $currentdate = date('Y-m-d');
        $lastTenDates[] = date('Y-m-d', strtotime("-1 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-2 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-3 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-4 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-5 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-6 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-7 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-8 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-9 day", strtotime($currentdate)));
       
        foreach($lastTenDates as $date_value ){
            $d=array();
             $d['date']=$date_value;
             $d['ucount']=User::where('role','user')->whereDate('created_at',$date_value)->count();
             
             $dd[]=$d;
        }
      return $dd;
    }
    
    public function CountLastSevenDays(){
        $lastDates[]= $currentdate = date('Y-m-d');
        $lastDates[] = date('Y-m-d', strtotime("-1 day", strtotime($currentdate)));
        $lastDates[] = date('Y-m-d', strtotime("-2 day", strtotime($currentdate)));
        $lastDates[] = date('Y-m-d', strtotime("-3 day", strtotime($currentdate)));
        $lastDates[] = date('Y-m-d', strtotime("-4 day", strtotime($currentdate)));
        $lastDates[] = date('Y-m-d', strtotime("-5 day", strtotime($currentdate)));
        $lastDates[] = date('Y-m-d', strtotime("-6 day", strtotime($currentdate)));
       
        foreach($lastDates as $date_value ){
            $d=array();
             $d['date']=$date_value;
             $get_data=BeaconHits::whereDate('created_at',$date_value)->first(['hit_count']);
             if(!empty($get_data->hit_count))
             {
             $d['count']=$get_data->hit_count;
             }else{
             $d['count']='0';
             }
             $dd[]=$d;
        }
      return $dd;
    }

     public function CountLastFourWeeks(){
        $lastTenDates[]= $currentdate = date('Y-m-d');
        $lastTenDates[] = date('Y-m-d', strtotime("-1 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-2 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-3 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-4 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-5 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-6 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-7 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-8 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-9 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-10 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-11 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-12 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-13 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-14 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-15 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-16 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-17 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-18 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-19 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-20 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-21 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-22 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-23 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-24 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-25 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-26 day", strtotime($currentdate)));
        $lastTenDates[] = date('Y-m-d', strtotime("-27 day", strtotime($currentdate)));
        // $lastTenDates[] = date('Y-m-d', strtotime("-9 day", strtotime($currentdate)));
       
        foreach($lastTenDates as $date_value ){
            $d=array();
             $d['date']=$date_value;
             $get_data=BeaconHits::whereDate('created_at',$date_value)->first(['hit_count']);
             if(!empty($get_data->hit_count))
             {
             $d['count']=$get_data->hit_count;
             }else{
             $d['count']='0';
             }
             $dd[]=$d;
        }
      return $dd;
    }
}
