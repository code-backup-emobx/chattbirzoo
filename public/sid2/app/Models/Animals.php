<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Animals extends Model
{
    use HasFactory;
    protected $primaryKey = 'animal_id';
    protected $table = 'animals';

    public function getSectionMedia(){

        return $this->hasMany('App\Models\SectionMedia','animal_id');
    }

//     public function getBeacon(){

//         return $this->hasOne('App\Models\Beacon','beacon_id');
//     }
      
}