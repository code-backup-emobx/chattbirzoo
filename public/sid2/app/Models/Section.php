<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use HasFactory;
    protected $primaryKey = 'section_id';
    protected $table = 'sections';

    public function getSectionMedia(){

        return $this->hasMany('App\Models\SectionMedia','section_id');
    }
      
}