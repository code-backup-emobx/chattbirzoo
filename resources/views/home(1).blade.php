@extends('layouts.base')

@section('content')
                        <!--begin::Toolbar-->
                        <div class="toolbar" id="kt_toolbar">
                            <!--begin::Container-->
                            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                                <!--begin::Page title-->
                                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                                    <!--begin::Title-->
                                    <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Dashboard
                                    <!--begin::Separator-->
                                    <span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
                                    <!--end::Separator-->
                                    <!--begin::Description-->
                                    <small class="text-muted fs-7 fw-bold my-1 ms-1"></small>
                                    <!--end::Description--></h1>
                                    <!--end::Title-->
                                </div>
                                <!--end::Page title-->
                                <!--begin::Actions-->
                             {{--    <div class="d-flex align-items-center py-1">
                                    <!--begin::Wrapper-->
                                    <div class="me-4">
                                        <!--begin::Menu-->
                                        <a href="#" class="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-flip="top-end">
                                        <!--begin::Svg Icon | path: icons/duotone/Text/Filter.svg-->
                                        <span class="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <path d="M5,4 L19,4 C19.2761424,4 19.5,4.22385763 19.5,4.5 C19.5,4.60818511 19.4649111,4.71345191 19.4,4.8 L14,12 L14,20.190983 C14,20.4671254 13.7761424,20.690983 13.5,20.690983 C13.4223775,20.690983 13.3458209,20.6729105 13.2763932,20.6381966 L10,19 L10,12 L4.6,4.8 C4.43431458,4.5790861 4.4790861,4.26568542 4.7,4.1 C4.78654809,4.03508894 4.89181489,4 5,4 Z" fill="#000000" />
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->Filter</a>
                                        <!--begin::Menu 1-->
                                        <div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true">
                                            <!--begin::Header-->
                                            <div class="px-7 py-5">
                                                <div class="fs-5 text-dark fw-bolder">Filter Options</div>
                                            </div>
                                            <!--end::Header-->
                                            <!--begin::Menu separator-->
                                            <div class="separator border-gray-200"></div>
                                            <!--end::Menu separator-->
                                            <!--begin::Form-->
                                            <div class="px-7 py-5">
                                                <!--begin::Input group-->
                                                <div class="mb-10">
                                                    <!--begin::Label-->
                                                    <label class="form-label fw-bold">Status:</label>
                                                    <!--end::Label-->
                                                    <!--begin::Input-->
                                                    <div>
                                                        <select class="form-select form-select-solid" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true">
                                                            <option></option>
                                                            <option value="1">Approved</option>
                                                            <option value="2">Pending</option>
                                                            <option value="2">In Process</option>
                                                            <option value="2">Rejected</option>
                                                        </select>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <!--end::Input group-->
                                                <!--begin::Input group-->
                                                <div class="mb-10">
                                                    <!--begin::Label-->
                                                    <label class="form-label fw-bold">Member Type:</label>
                                                    <!--end::Label-->
                                                    <!--begin::Options-->
                                                    <div class="d-flex">
                                                        <!--begin::Options-->
                                                        <label class="form-check form-check-sm form-check-custom form-check-solid me-5">
                                                            <input class="form-check-input" type="checkbox" value="1" />
                                                            <span class="form-check-label">Author</span>
                                                        </label>
                                                        <!--end::Options-->
                                                        <!--begin::Options-->
                                                        <label class="form-check form-check-sm form-check-custom form-check-solid">
                                                            <input class="form-check-input" type="checkbox" value="2" checked="checked" />
                                                            <span class="form-check-label">Customer</span>
                                                        </label>
                                                        <!--end::Options-->
                                                    </div>
                                                    <!--end::Options-->
                                                </div>
                                                <!--end::Input group-->
                                                <!--begin::Input group-->
                                                <div class="mb-10">
                                                    <!--begin::Label-->
                                                    <label class="form-label fw-bold">Notifications:</label>
                                                    <!--end::Label-->
                                                    <!--begin::Switch-->
                                                    <div class="form-check form-switch form-switch-sm form-check-custom form-check-solid">
                                                        <input class="form-check-input" type="checkbox" value="" name="notifications" checked="checked" />
                                                        <label class="form-check-label">Enabled</label>
                                                    </div>
                                                    <!--end::Switch-->
                                                </div>
                                                <!--end::Input group-->
                                                <!--begin::Actions-->
                                                <div class="d-flex justify-content-end">
                                                    <button type="reset" class="btn btn-sm btn-light btn-active-light-primary me-2" data-kt-menu-dismiss="true">Reset</button>
                                                    <button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true">Apply</button>
                                                </div>
                                                <!--end::Actions-->
                                            </div>
                                            <!--end::Form-->
                                        </div>
                                        <!--end::Menu 1-->
                                        <!--end::Menu-->
                                    </div>
                                    <!--end::Wrapper-->
                                    <!--begin::Button-->
                                    <a href="#" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_create_app" id="kt_toolbar_primary_button">Create</a>
                                    <!--end::Button-->
                                </div>  --}}
                                <!--end::Actions-->
                            </div>
                            <!--end::Container-->
                        </div>
                        <!--end::Toolbar-->
                        <!--begin::Post-->
                        <div class="post d-flex flex-column-fluid" id="kt_post">
                            <!--begin::Container-->
                            <div id="kt_content_container" class="container">
                            <!--begin::Row-->
                                <div class="row gy-5 g-xl-8">
                                    <!--begin::Col-->
                                    <div class="col-xxl-12">
                                        <!--begin::Mixed Widget 2-->
                                        <div class="card card-xxl-stretch mb-5">
                                            <!--begin::Header-->
                                            <div class="card-header border-0 bg-danger py-5">
                                                <h3 class="card-title fw-bolder text-white"> Statistics</h3>
                                                <div class="card-toolbar">
                                                    <!--begin::Menu-->
                                                    <button type="button" class="btn btn-sm btn-icon btn-color-white btn-active-white btn-active-color- border-0 me-n3" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-flip="top-end">
                                                        <!--begin::Svg Icon | path: icons/duotone/Layout/Layout-4-blocks-2.svg-->
                                                        <span class="svg-icon svg-icon-2">
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
                                                                    <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
                                                                    <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
                                                                    <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
                                                                </g>
                                                            </svg>
                                                        </span>
                                                        <!--end::Svg Icon-->
                                                    </button>
                                                    <!--begin::Menu 3-->
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3" data-kt-menu="true">
                                                        <!--begin::Heading-->
                                                        <div class="menu-item px-3">
                                                            <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Total</div>
                                                        </div>
                                                        <!--end::Heading-->
                                                      
                                                        <!--begin::Menu item-->
                                                        <div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="left-start" data-kt-menu-flip="center, top">
                                                            <a href="#" class="menu-link px-3">
                                                                <span class="menu-title">Users</span>
                                                                <span class="menu-arrow"></span>
                                                            </a>
                                                            <!--begin::Menu sub-->
                                                            <div class="menu-sub menu-sub-dropdown w-175px py-4">
                                                                 <!--begin::Menu item-->
                                                                <div class="menu-item px-3">
                                                                    <a href="#" class="menu-link px-3">All Users:  </a>
                                                                </div>
                                                                <!--end::Menu item-->
                                                                <!--begin::Menu item-->
                                                                <div class="menu-item px-3">
                                                                    <a href="#" class="menu-link px-3">Active :  </a>
                                                                </div>
                                                                <!--end::Menu item-->
                                                                <!--begin::Menu item-->
                                                                <div class="menu-item px-3">
                                                                    <a href="#" class="menu-link px-3">Blocked:  </a>
                                                                </div>
                                                                <!--end::Menu item-->
                                                                 <!--begin::Menu item-->
                                                                <div class="menu-item px-3">
                                                                    <a href="#" class="menu-link px-3">Deleted:  </a>
                                                                </div>
                                                                <!--end::Menu item-->
                                                                
                                                                <!--begin::Menu item-->
                                                             
                                                              
                                                            </div>
                                                            <!--end::Menu sub-->
                                                        </div>
                                                        <!--end::Menu item-->
                                                      
                                                    </div>
                                                    <!--end::Menu 3-->
                                                    <!--end::Menu-->
                                                </div>
                                            </div>
                                            <!--end::Header-->
                                            <!--begin::Body-->
                                            <div class="card-body p-0">
                                                <!--begin::Chart-->
                                                <div class="mixed-widget-2-chart card-rounded-bottom bg-danger" data-kt-color="danger" style="height: 200px"></div>
                                                <!--end::Chart-->
                                                <!--begin::Stats-->
                                                <div class="card-p mt-n20 position-relative">
                                                    <!--begin::Row-->
                                                    <div class="row g-0">
                                                        <!--begin::Col-->
                                                        <div class="col bg-light-warning px-6 py-8 rounded-2 me-7 mb-7">
                                                            <!--begin::Svg Icon | path: icons/duotone/Media/Equalizer.svg-->
                                                            <span class="svg-icon svg-icon-3x svg-icon-warning d-block my-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24" />
                                                                        <rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5" />
                                                                        <rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5" />
                                                                        <rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5" />
                                                                        <rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5" />
                                                                    </g>
                                                                </svg>
                                                            </span>
                                                            <!--end::Svg Icon-->
                                                            <a href="/users" class="text-warning fw-bold fs-6">All Users : {{$allusers}}</a>
                                                        </div>
                                                        <!--end::Col-->
                                                        <!--begin::Col-->
                                                        
                                                        <!--end::Col-->
                                                    </div>
                                                    <!--end::Row-->
                                                   
                                                </div>
                                                <!--end::Stats-->
                                            </div>

                                            <!--end::Body-->
                                            
                                            </div>
                                        </div>
                                        <!--end::Mixed Widget 2-->
                                        
                                        

                                            <!--end::Body-->
                                        </div>
<!--                                     </div> -->
                                    <!--end::Col-->
                               
                                    <!--end::Col-->
                        
<!--                                 </div> -->
                                <!--end::Row--> 
                            <!--begin::Row-->
                                <div class="row gy-5 g-xl-8">
                                    <!--begin::Col-->
                                  
                                    <!--end::Col-->
                                    <!--begin::Col-->
                                    <div class="col-xxl-12">
                                        <!--begin::Tables Widget 9-->
                                        <div class="card card-xxl-stretch mb-5">
                                            <!--begin::Header-->
                                            <div class="card-header border-0 pt-5">
                                                <h3 class="card-title align-items-start flex-column">
                                                    <span class="card-label fw-bolder fs-3 mb-1">New Users Registered Last 10 days</span>
                                                    <!-- <span class="text-muted mt-1 fw-bold fs-7">Last Ten users</span> -->
                                               
                                                </h3>
                                             <a href="/users" class="btn btn-sm btn-light-primary" style="margin-right: 59.2rem; height: 35px; margin-top: 9px;">{{$allusers}}</a>
                                                
                                            </div>
                                            <!--end::Header-->
                                            <!--begin::Body-->
                                            <div class="card-body py-3">
                                                <figure class="highcharts-figure">
                                                <div id="tendays"></div>
                                              
                                            </figure>
                                            </div>
                                            
                                            <!--begin::Body-->
                                        
                                        </div>
                                        <!--end::Tables Widget 9-->
                                    
                                        
                                    </div>
                                    <!--end::Col-->
                                </div>
                                <!--end::Row-->
                                <!--begin::Row-->
                                <div class="row gy-5 g-xl-8">
                                    <!--begin::Col-->
                                  
                                    <!--end::Col-->
                                    <!--begin::Col-->
                                    <div class="col-xl-12">
                                        <!--begin::Tables Widget 9-->
                                        <div class="card card-xl-stretch mb-5 mb-xl-8">
                                            <!--begin::Header-->
                                            <div class="card-header border-0 pt-5">
                                                <h3 class="card-title align-items-start flex-column">
                                                    <span class="card-label fw-bolder fs-3 mb-1">Beacon Hits</span>
                                                    <!-- <span class="text-muted mt-1 fw-bold fs-7">Last Ten users</span> -->
                                               
                                                </h3>
                                             <a class="btn btn-sm btn-light-primary" style="margin-right: 74.2rem; height: 35px; margin-top: 9px;">{{ $total_hit }}</a>
                                                
                                            </div>
                                            <!--end::Header-->
                                            <!--begin::Body-->
                                            <div class="card-body py-3">
                                            
                                                <div class="container">
                             <div class="m-bottom m-top1">
<!--                               <h2 class="text-center">Gallery</h2>-->
                                 <button class="tablink" onclick="openPage('Today', this, '#082c45')" id="defaultOpen">Today</button>
                                    <button class="tablink" onclick="openPage('This_week', this, '#082c45')">This Week</button>
                                    <button class="tablink" onclick="openPage('past_four_week', this, '#082c45')">Past Four weeks</button>


                                <div id="Today" class="tabcontent mt-5">
                                  <div class="row m-bottom1">
                                  	<figure class="highcharts-figure">
                                                <div id="today"></div>
                                              
                                            </figure>
                                   </div>
                                     
                               </div>

                 <div id="This_week" class="tabcontent mt-5">
                        <div class="row">
                            <figure class="highcharts-figure">
                                                <div id="this_week"></div>
                                              
                                            </figure>
                         </div> 
                  </div>
                             
                             <div id="past_four_week" class="tabcontent mt-5">
                        <div class="row">
                            <figure class="highcharts-figure">
                                                <div id="four_week"></div>
                                              
                                            </figure>
                         </div> 
                  </div>
                             
                                    
                            
                                

                                </div>
                             
                         </div>
                                                
                                            </div>
                                            
                                            <!--begin::Body-->
                                        
                                        </div>
                                        <!--end::Tables Widget 9-->
                                    
                                        
                                    </div>
                                    <!--end::Col-->
                                </div>
                                <!--end::Row-->
                                
                                
                                
                              
                            </div>
                            <!--end::Container-->
                        </div>

                        <!--end::Post-->
@endsection                
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
    $(document).ready(function() {
    Highcharts.chart('tendays', {
    chart: {
        type: 'areaspline',
        renderTo: 'tendays'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
	credits: {
    	enabled: false
	},
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'User Count'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: ''
    },
    series: [{
        name: '',
     data:[ 
        <?php 
     $json_arr=array();
     foreach($tendays as $tenday){
            $change_format=date('d M Y', strtotime($tenday['date']));
            $json_arr[]= "['".$change_format."',".$tenday['ucount']."]";
        } echo implode(',',$json_arr); ?>
         ],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
            
        }
    }]
});
    
    Highcharts.chart('today', {
    chart: {
        type: 'areaspline',
        renderTo: 'today'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    credits: {
    	enabled: false
	},
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Hits'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: ''
    },
    series: [{
        name: '',
     data:[ 
        <?php 
     $json_arr=array();
    foreach($today_beacons_hit as $data){
            
            $json_arr[]= "['".$current_date."',".$data['hit_count']."]";
        }
     
      echo implode(',',$json_arr); ?>
         ],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
            
        }
    }]
});
    
Highcharts.chart('this_week', {
    chart: {
        type: 'areaspline',
        renderTo: 'this_week'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
	credits: {
    	enabled: false
	},
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Hits'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: ''
    },
    series: [{
        name: '',
     data:[ 
        <?php 
     $json_arr=array();
     foreach($last_seven_days_hit as $data){
            $change_format=date('d M Y', strtotime($data['date']));
            $json_arr[]= "['".$change_format."',".$data['count']."]";
        } echo implode(',',$json_arr); ?>
         ],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
            
        }
    }]
});
    
Highcharts.chart('four_week', {
    chart: {
        type: 'areaspline',
        renderTo: 'four_week'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
	credits: {
    	enabled: false
	},
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Hits'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: ''
    },
    series: [{
        name: '',
     data:[ 
        <?php 
     $json_arr=array();
     foreach($four_weeks_hit as $data){
            $change_format=date('d M Y', strtotime($data['date']));
            $json_arr[]= "['".$change_format."',".$data['count']."]";
        } echo implode(',',$json_arr); ?>
         ],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
            
        }
    }]
});
});
</script>

<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script> -->
<script>
function openPage(pageName,elmnt,color) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
    tablinks[i].style.color = color;
  }
  document.getElementById(pageName).style.display = "block";
  elmnt.style.backgroundColor = color;
  elmnt.style.color = 'white';
}

// Get the element with id="defaultOpen" and click on it
// window.onload=function(){
//   document.getElementById("defaultOpen").click();
// }
$(document).ready(function() {
    // alert('hlo');
document.getElementById("defaultOpen").click();
}); 

</script>
