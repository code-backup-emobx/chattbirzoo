@extends('layouts.base')

@section('content')
 <link href="{{ url('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
            <!--begin::Subheader-->
         {{--   <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
              <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-2">
                  <!--begin::Page Title-->
                  <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
                  <!--end::Page Title-->
                  <!--begin::Actions-->
                  <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                  
                  <!--end::Actions-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
        
                <!--end::Toolbar-->
              </div>
            </div> --}}
            <!--end::Subheader-->
            <!--begin::Entry-->
<div class="toolbar" id="kt_toolbar">
							<!--begin::Container-->
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Dashboard</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start mx-4"></span>
									<!--end::Separator-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="/" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
								<!--begin::Actions-->
								<div class="d-flex align-items-center py-1">
									
									<!--end::Wrapper-->
									<!--begin::Button-->
                                Last pull data at : <b> {{ $cd }} </b> &nbsp;&nbsp;&nbsp;
                                 <form action='/' >
                                 <input type="hidden" value="{{rand(515,98989)}}" name="updatedashboard"/>
                                 <button type="submit"  class="btn btn-light-success">Click to pull data now </a>
                                </form>
									
									<!--end::Button-->
								</div>
								<!--end::Actions-->
							</div>
							<!--end::Container-->
						</div>
            <div class="d-flex flex-column-fluid">
              <!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <div class="row">
                <div class="col-xl-3">
                    <!--begin::Stats Widget 29-->
                    <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(assets/media/svg/shapes/abstract-1.svg)">
                      <!--begin::Body-->
                      <div class="card-body">
                        <a href="{{ route('users') }}" >
                        <span class="svg-icon svg-icon-2x svg-icon-info">
                          <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-opened.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                              <polygon points="0 0 24 0 24 24 0 24"></polygon>
                              <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                              <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
                            </g>
                          </svg>
                          <!--end::Svg Icon-->
                        </span>
                        <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $visitorTotal ?? '0'}}</span>
                        <span class="font-weight-bold text-muted font-size-sm">Total Visitors</span>
                      </a>
                      </div>
                      <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 29-->
                  </div>
                  <div class="col-xl-3">
                    <!--begin::Stats Widget 30-->
                    <div class="card card-custom bg-info card-stretch gutter-b">
                      <!--begin::Body-->
                      <div class="card-body">
                          <a href="{{ route('admin') }}" >
                        <span class="svg-icon svg-icon-2x svg-icon-white">
                          <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->
                          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                              <polygon points="0 0 24 0 24 24 0 24" />
                              <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                              <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                            </g>
                          </svg>
                          <!--end::Svg Icon-->
                        </span>
                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $adminTotal ?? '0' }}</span>
                        <span class="font-weight-bold text-white font-size-sm">Admins</span>
                      </a>
                      </div>
                      <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 30-->
                  </div>
                  <div class="col-xl-3">
                    <!--begin::Stats Widget 31-->
                    <div class="card card-custom bg-danger card-stretch gutter-b">
                      <!--begin::Body-->
                      <div class="card-body">    
                      <a href="{{ route('feedback_list') }}" >
                        <span class="svg-icon svg-icon-2x svg-icon-white">
                          <!--begin::Svg Icon | path:assets/media/svg/icons/Media/Equalizer.svg-->
                          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                              <rect x="0" y="0" width="24" height="24" />
                              <rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5" />
                              <rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5" />
                              <rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5" />
                              <rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5" />
                            </g>
                          </svg>
                          <!--end::Svg Icon-->
                        </span>
                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $overall_rating ?? '0'}}</span>
                        <span class="font-weight-bold text-white font-size-sm">Over all rating stars out of 5 </span>
                      </a>
                      </div>
                      <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 31-->
                  </div>
                  <div class="col-xl-3">
                    <!--begin::Stats Widget 32-->
                    <div class="card card-custom bg-dark card-stretch gutter-b">
                      <!--begin::Body-->
                      <div class="card-body">
                        <a href="{{ route('beacon_hits') }}" >
                        <span class="svg-icon svg-icon-2x svg-icon-white">
                          <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group-chat.svg-->
                          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                              <rect x="0" y="0" width="24" height="24" />
                              <path d="M16,15.6315789 L16,12 C16,10.3431458 14.6568542,9 13,9 L6.16183229,9 L6.16183229,5.52631579 C6.16183229,4.13107011 7.29290239,3 8.68814808,3 L20.4776218,3 C21.8728674,3 23.0039375,4.13107011 23.0039375,5.52631579 L23.0039375,13.1052632 L23.0206157,17.786793 C23.0215995,18.0629336 22.7985408,18.2875874 22.5224001,18.2885711 C22.3891754,18.2890457 22.2612702,18.2363324 22.1670655,18.1421277 L19.6565168,15.6315789 L16,15.6315789 Z" fill="#000000" />
                              <path d="M1.98505595,18 L1.98505595,13 C1.98505595,11.8954305 2.88048645,11 3.98505595,11 L11.9850559,11 C13.0896254,11 13.9850559,11.8954305 13.9850559,13 L13.9850559,18 C13.9850559,19.1045695 13.0896254,20 11.9850559,20 L4.10078614,20 L2.85693427,21.1905292 C2.65744295,21.3814685 2.34093638,21.3745358 2.14999706,21.1750444 C2.06092565,21.0819836 2.01120804,20.958136 2.01120804,20.8293182 L2.01120804,18.32426 C1.99400175,18.2187196 1.98505595,18.1104045 1.98505595,18 Z M6.5,14 C6.22385763,14 6,14.2238576 6,14.5 C6,14.7761424 6.22385763,15 6.5,15 L11.5,15 C11.7761424,15 12,14.7761424 12,14.5 C12,14.2238576 11.7761424,14 11.5,14 L6.5,14 Z M9.5,16 C9.22385763,16 9,16.2238576 9,16.5 C9,16.7761424 9.22385763,17 9.5,17 L11.5,17 C11.7761424,17 12,16.7761424 12,16.5 C12,16.2238576 11.7761424,16 11.5,16 L9.5,16 Z" fill="#000000" opacity="0.3" />
                            </g>
                          </svg>
                          <!--end::Svg Icon-->
                        </span>
                        <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 text-hover-primary d-block">{{ $beaconTotal ?? '0'}}</span>
                        <span class="font-weight-bold text-white font-size-sm">Total Beacon Hits</span>
                      </a>
                      </div>
                      <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 32-->
                  </div>
                                </div>
                 <!--end::Dashboard-->
              <br>
              <br>



                                  <div class="row">
                  <div class="col-lg-6">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                      <!--begin::Header-->
                      <div class="card-header h-auto">
                        <!--begin::Title-->
                        <div class="card-title py-5">
                          <h3 class="card-label">Total Beacan Hits</h3>
                           <a href="{{ url('/beacon_hits') }}"  > <button type="button" class="btn btn-primary">{{ $beaconTotal ?? '0'}}</button></a>
                        </div>
                        <!--end::Title-->
                      </div>
                      <!--end::Header-->
                      <div class="card-body">
                        <!--begin::Chart-->
                        <div id="beaconhit_chart"></div>
                        <!--end::Chart-->
                      </div>
                    </div>
                    <!--end::Card-->
                  </div>
                                          <div class="col-lg-6">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                      <!--begin::Header-->
                      <div class="card-header h-auto">
                        <!--begin::Title-->
                        <div class="card-title py-5">
                          <h3 class="card-label">Total Admins</h3>
                                                    <a href="{{ route('admin') }}"><button type="button" class="btn btn-info">{{ $adminTotal ?? '0'}}</button></a>
                        </div>
                        <!--end::Title-->
                      </div>
                      <!--end::Header-->
                      <div class="card-body">
                        <!--begin::Chart-->
                        <div id="admin_chart"></div>
                        <!--end::Chart-->
                      </div>
                    </div>
                    <!--end::Card-->
                  </div>
                
                </div>
              <br><br>
                                  <div class="row">
                  <div class="col-lg-6">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                      <!--begin::Header-->
                      <div class="card-header h-auto">
                        <!--begin::Title-->
                        <div class="card-title py-5">
                          <h3 class="card-label">Total Feedback</h3>
                                                    <a href="{{ route('feedbacks') }}"  > <button type="button" class="btn btn-warning">{{ $feedbackTotal ?? '0'}}  </button></a>
                        </div>
                        <!--end::Title-->
                      </div>
                      <!--end::Header-->
                      <div class="card-body">
                        <!--begin::Chart-->
                        <div id="feedback_chart"></div>
                        <!--end::Chart-->
                      </div>
                    </div>
                    <!--end::Card-->
                  </div>
                  <div class="col-lg-6">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                      <!--begin::Header-->
                      <div class="card-header h-auto">
                        <!--begin::Title-->
                        <div class="card-title py-5">
                          <h3 class="card-label">Total Visitors</h3>
                                                      <a href="{{ route('users') }}" > <button type="button" class="btn btn-danger">{{ $visitorTotal ?? '0'}}</button></a>
                        </div>
                        <!--end::Title-->
                      </div>
                      <!--end::Header-->
                      <div class="card-body">
                        <!--begin::Chart-->
                        <div id="visitors_chart"></div>
                        <!--end::Chart-->
                      </div>
                    </div>
                    <!--end::Card-->
                  </div>
                
                </div> 

           

            {{-- <div class="row">
                  <div class="col-lg-12">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                      <!--begin::Header-->
                      <div class="card-header h-auto">
                        <!--begin::Title-->
                        <div class="card-title py-5">
                          <h3 class="card-label">Top 5 </h3>
                                                     <a href="{{ url('institute.courses_list') }}"> <button type="button" class="btn btn-warning">See All</button></a>
                        </div>
                        <!--end::Title-->
                      </div>
                      <!--end::Header-->
                      <div class="card-body">
                        <!--begin::Chart-->
                        <div id="">
                               <div class="card-body py-3">
                                                <!--begin::Table container-->
                                                <div class="table-responsive">
                                                    <!--begin::Table-->
                                                    <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                                                        <!--begin::Table head-->
                                                        <thead>
                                                            <tr class="fw-bolder text-muted">
                                                                <!-- <th class="w-25px">
                                                                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                                                                        <input class="form-check-input" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-9-check" />
                                                                    </div>
                                                                </th> -->
                                                                <th class="min-w-150px">Course</th>
                                                                <th class="min-w-140px">Student Enrolled</th>
                                                            </tr>
                                                        </thead>
                                                        <!--end::Table head-->
                                                        <!--begin::Table body-->
                                                        <tbody>
                                                          @if(isset($topCourseData))
                                                          @foreach($topCourseData as $topCourseDataVal)
                                                            <tr>
                                                             
                                                                
                                                                <td>
                                                                    <div class="d-flex align-items-center">
                                                                        <div class="symbol symbol-45px me-5">
                                                                          
                                                                            
                                                                        </div>
                                                                        <di v class="d-flex justify-content-start flex-column">
                                                                            <a href="#" class="text-dark fw-bolder text-hover-primary fs-6">{{ $topCourseDataVal->getCourse->name ?? ''}}</a>
                                                                            <span class="text-muted fw-bold text-muted d-block fs-7"></span>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{ $topCourseDataVal->total ?? ''}}</a>
                                                                    <span class="text-muted fw-bold text-muted d-block fs-7"></span>
                                                                </td>
                                                         
                                                               
                                                            </tr>
                                                         
                                                           @endforeach
                                                           @endif
                                                            
                                                        </tbody>
                                                        <!--end::Table body-->
                                                    </table>
                                                    <!--end::Table-->
                                                </div>
                                                <!--end::Table container-->
                                            </div>
                        </div>
                        <!--end::Chart-->
                      </div>
                    </div>
                    <!--end::Card-->
                  </div>
                </div> --}}



              </div>
              <!--end::Container-->
            </div>
            <!--end::Entry-->
          
@endsection
@section('scripts')
 <script src="{{ url('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
 <script src="{{ url('assets/js/pages/widgets.js') }}"></script>
 <script>   
// Shared Colors Definition
const primary = '#6993FF';
const success = '#1BC5BD';
const info = '#8950FC';
const warning = '#FFA800';
const danger = '#F64E60';

// Class definition


var KTApexChartsDemo = function () {
    // Private functions
    var _institutes = function () {
        const apexChart = "#beaconhit_chart";
        var options = {
            series: [ {
                name: 'Total beacon hits on this date :',
              
                 data:  [{{ implode(',',array_values($beaconGraphData))}}] 
              
                
            }],
            chart: {
                height: 350,
                type: 'area'
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth'
            },
            xaxis: {
                
                 categories: [@foreach($beaconGraphData as $k=>$v)'{{ $k }}'@if(!$loop->last),@endif @endforeach]

            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                },
            },
            colors: [primary, success]
        };

        var chart = new ApexCharts(document.querySelector(apexChart), options);
        chart.render();
    }



    var _educators = function () {
        const apexChart = "#feedback_chart";
        var options = {
            series: [ {
                 name: 'Total feedback starts on this date :',
              
                 data:  [{{ implode(',',array_values($feedbackGraphData))}}] 
              
               
            }],
            chart: {
                height: 350,
                type: 'area'
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth'
            },
            xaxis: {
                
                 categories: [@foreach($feedbackGraphData as $k=>$v)'{{ $k }}'@if(!$loop->last),@endif @endforeach]

            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                },
            },
            colors: [warning, success]
        };

        var chart = new ApexCharts(document.querySelector(apexChart), options);
        chart.render();
    }


        var _courses = function () {
        const apexChart = "#admin_chart";
        var options = {
            series: [ {
                   name: 'Total admins till this date are :',
              
                 data:  [{{ implode(',',array_values($adminGraphData))}}] 
              
            }],
            chart: {
                height: 350,
                type: 'area'
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth'
            },
            xaxis: {
                
                categories: [@foreach($adminGraphData as $k=>$v)'{{ $k }}'@if(!$loop->last),@endif @endforeach]
            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                },
            },
            colors: [info, success]
        };

        var chart = new ApexCharts(document.querySelector(apexChart), options);
        chart.render();
    }


        var _students = function () {
        const apexChart = "#visitors_chart";
        var options = {
            series: [ {
                name: 'Total visitors till this date are :',
              
               // data:  [2,32,33,34,35,37,38,40,43,44] 
            data:  [{{ implode(',',array_values($visitorGraphData))}}] 
            }],
            chart: {
                height: 350,
                type: 'area'
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth'
            },
            xaxis: {
                
               // categories: ['03 Jan 2022', '04 Jan 2022', '05 Jan 2022', '08 Jan 2022', '29 Jan 2022', '31 Jan 2022', '10 Feb 2022', '17 Feb 2022', '02 Mar 2022', '03 Mar 2022' ]
                 categories: [@foreach($visitorGraphData as $k=>$v)'{{ $k }}'@if(!$loop->last),@endif @endforeach]
            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                },
            },
            colors: [danger, success]
        };

        var chart = new ApexCharts(document.querySelector(apexChart), options);
        chart.render();
    }





    return {
        // public functions
        init: function () {
            _institutes();
            _courses();
            _students();
            _educators();
        
        
        }
    };
}();
jQuery(document).ready(function () {
    KTApexChartsDemo.init();
});</script>
@endsection
          