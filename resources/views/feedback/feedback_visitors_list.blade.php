@extends('layouts.base')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"/>

						<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Toolbar-->
						<div class="toolbar" id="kt_toolbar">
							<!--begin::Container-->
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Feedback Visitor List</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start mx-4"></span>
									<!--end::Separator-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="/home" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted"><a href="/feedback_list" class="text-muted text-hover-primary">Feedback</a></li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark">Feedback Visitor List</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
								<!--begin::Actions-->
										<div class="d-flex align-items-center py-1">
									<!--begin::Wrapper-->
									<div class="me-4">
										<!--begin::Menu-->
										<a href="#" class="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-flip="top-end">
										<!--begin::Svg Icon | path: icons/duotone/Text/Filter.svg-->
										<span class="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24" />
													<path d="M5,4 L19,4 C19.2761424,4 19.5,4.22385763 19.5,4.5 C19.5,4.60818511 19.4649111,4.71345191 19.4,4.8 L14,12 L14,20.190983 C14,20.4671254 13.7761424,20.690983 13.5,20.690983 C13.4223775,20.690983 13.3458209,20.6729105 13.2763932,20.6381966 L10,19 L10,12 L4.6,4.8 C4.43431458,4.5790861 4.4790861,4.26568542 4.7,4.1 C4.78654809,4.03508894 4.89181489,4 5,4 Z" fill="#000000" />
												</g>
											</svg>
										</span>
										<!--end::Svg Icon-->Filter</a>
										<!--begin::Menu 1-->
										<div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true">
											<!--begin::Header-->
											<div class="px-7 py-5">
												<div class="fs-5 text-dark fw-bolder">Filter Options</div>
											</div>
											<!--end::Header-->
											<!--begin::Menu separator-->
											<div class="separator border-gray-200"></div>
											<!--end::Menu separator-->
											<form action="" method="get">
												<!-- @csrf -->
											<!--begin::Form-->
											<div class="px-7 py-5">
												
                                            <div class="mb-10">
                                            <label class="form-label fw-bold">From Date:</label>
					                        <div class="form-group">
					                          <input type="text"  id="from-datepicker"  data-provide="datepicker" class="form-control from-datepicker" name="date_from" value="{{ request()->date_from }}" placeholder="from date" readonly="from" />
					                        </div>
					                      </div>
                     
					                      <div class="mb-10">
					                      <label class="form-label fw-bold">To Date:</label>
					                        <div class="form-group">
					                          <input type="text"  id="from-datepicker1" data-provide="datepicker" class="form-control from-datepicker" name="date_to" value="{{ request()->date_to }}" placeholder="to date" readonly="to"/>
					                        </div>  
					                      </div>
                                               
												
												<!--begin::Actions-->
                                           
                                               
                                            
												<div class="d-flex justify-content-end">
                                                   
												<a  href="{{ url('feedback_visitor_list/'.$feedback_recent_id) }}" type="reset" class="btn btn-sm btn-light btn-active-light-dark me-2" data-kt-menu-dismiss="true">Reset</a>
													<button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true">Apply</button>
                                               </div>
                                                	
											
                                         
												<!--end::Actions-->
											</div>
											</form>
											<!--end::Form-->
										</div>
										<!--end::Menu 1-->
										<!--end::Menu-->
									</div>
									<!--end::Wrapper-->
									<!--begin::Button-->
									<!-- <a href="#" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_create_app" id="kt_toolbar_primary_button">Create</a> -->
									<!--end::Button-->
								</div>
								<!--end::Actions-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Toolbar-->
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container">
								<!--begin::Navbar-->
								
								<!--end::Navbar-->
								<!--begin::Modals-->
								<!--begin::Modal - View Users-->
								
								<!--end::Modal - View Users-->
								<!--begin::Modal - Users Search-->
								
								<!--end::Modal - Users Search-->
								<!--begin::Modal - New Target-->
								
								<!--end::Modal - New Target-->
								<!--end::Modals-->
								<!--begin::Toolbar-->
								
								<!--end::Toolbar-->
								<!--begin::Tab Content-->
								<div class="tab-content">
									<!--begin::Tab pane-->
									<div id="kt_project_users_card_pane" class="tab-pane fade show active">
										<!--begin::Row-->
										<div class="row g-6 g-xl-9">
											<!--begin::Col-->
											@foreach($lists as $list)
											<div class="col-xl-6">
												<!--begin::Mixed Widget 19-->
												<div class="card card-custom gutter-b" style="height: 250px;">
													<!--begin::Body-->
													<div class="card-body d-flex flex-column">
														<div class="d-flex align-items-center justify-content-between flex-grow-1">
															<div class="mr-2">
																<h3 class="font-weight-bolder">{{ ucfirst($list->getUser->username )  ?? '' }}</h3>
                                                                <div class="text-muted font-size-lg mt-2">{{ date('Y-m-d h:i A', strtotime($list->created_at)) }} </div>
                                                                
															</div>
															<div class="text-muted font-size-lg mt-2">FID<span style="font-size: 15px; font-weight: 600">&nbsp;{{ ucfirst($list->id )  ?? '' }}</span></div>
														</div>
														@foreach($list['getUserFeedback'] as $detail)
                                                        <div class="d-flex align-items-center justify-content-between flex-grow-1" style="margin-top: 20px;">
															<div class="mr-2">
																<h3 style="font-size: 16px">{{ ucfirst($detail->getAnimal->animal_name )  ?? '' }}</h3>
                                                                
															</div>
															
                                                             @php $rating = $detail->rating; @endphp  
                                                             <div class="justify-content-right">
													            @foreach(range(1,5) as $i)
													                <span class="fa-stack" style="width:1em">
													                    <i class="far fa-star fa-stack-1x"></i>

													                    @if($rating >0)
													                        @if($rating >0.5)
													                            <i class="fas fa-star fa-stack-1x" style="color:orange"></i>
													                        @else
													                            <i class="fas fa-star-half fa-stack-1x" style="color:orange"></i>
													                        @endif
													                    @endif
													                    @php $rating--; @endphp
													                </span>
													            @endforeach
													        </div>
														</div>
                                                     	@endforeach
                                                        <span class="btn font-weight-regular w-100 py-3 mt-5" style="border: 1px solid #ccc;color:#a1a5b7;cursor:default;text-align: left;">@if($list->comment)
														{{  $list->comment }}
                                                                        @endif</span>
														
													</div>
                                                    
													<!--end::Body-->
												</div>
												<!--end::Mixed Widget 19-->
											</div>

											@endforeach
											<!--end::Col-->
										</div>
										<!--end::Row-->
										<!--begin::Pagination-->
										
										<!--end::Pagination-->
									</div>
									<!--end::Tab pane-->
									<!--begin::Tab pane-->
									
									<!--end::Step 1-->
									<!--begin::Step 2-->
								</div>
							</div>
						</div>
					</div>

									
@endsection                

@section('scripts')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
<script>
$(document).ready(function() {
    $(".from-datepicker").datepicker({ 
        format: 'yyyy-mm-dd'
    });
    $(".from-datepicker").on("change", function () {
        var fromdate = $(this).val();
    });
}); 



</script>
@endsection  
