@extends('layouts.base')

@section('content')

														
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Toolbar-->
						<div class="toolbar" id="kt_toolbar">
							<!--begin::Container-->
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Mapping List</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start mx-4"></span>
									<!--end::Separator-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="/" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
                                        <a href="/mapping" class="text-muted text-hover-primary">Mapping</a></li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark"><a href="/unmapped">Unmapping List</a></li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
								<!--begin::Actions-->
								<div class="d-flex align-items-center py-1">
									
									<!--end::Wrapper-->
									<!--begin::Button-->
<!-- 									<a href="/unmapped" class="btn btn-sm btn-light-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_create_app">Show Unmapped Animals</a> -->
									<!--end::Button-->
								</div>
								<!--end::Actions-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Toolbar-->
                    @include('layouts.notification')
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container">
								<!--begin::Card-->
								<div class="card">
									<!--begin::Card header-->
									<!-- <div class="card-header border-0 pt-6">
										
										<div class="card-title">
											
											<div class="d-flex align-items-center position-relative my-1">
												
											</div>
										
										</div>
										
										<div class="card-toolbar">
											
											
										</div>
									
									</div> -->
									<!--end::Card header-->
									<!--begin::Card body-->
									<!-- <div class="card-body pt-0 mt-2">
										<h4>Images</h4>
										<div class="row">
										
										 
										</div>
                         			</div> --> 



                                 <div class="card-body pt-0 mt-2">
                                  <h4>Images</h4>
                                  <div class="row">
                                  	 @if($section_media)
										 @foreach($section_media as $media)
										 	@if($media->media_type == 'image')
										  
										   <div class="col-md-4 col-sm-12">
                                     <img style="width: 100%;height: -webkit-fill-available;object-fit: cover;" src="{{env('APP_URL').$media->media_url}}" />
                                   </div>
										  @endif
										  @endforeach
										  @endif
                                  

                                  </div>
                                 </div>

{{ $section_media->links()}}

                  				</div>
                             
                                    
                            
                                 
                                </div>
                             
                         </div>
										
										<!--end::Table-->
										
									</div>
									<!--end::Card body-->
								</div>
								<!--end::Card-->
								  </div>

                                                <!--end::Table container-->
                              </div>
                                            <!--begin::Body-->

                    </div>
                                        <!--end::Tables Widget 9-->


                               	 

@endsection       


@section('scripts')

