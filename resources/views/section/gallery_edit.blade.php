@extends('layouts.base')

@section('content')

					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Toolbar-->
						<div class="toolbar" id="kt_toolbar">
							<!--begin::Container-->
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Content Master</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start mx-4"></span>
									<!--end::Separator-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="{{route('home')}}" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
                                        <a href="/section" class="text-muted text-hover-primary">Content Master</a></li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark">Animal Media</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
								<!--begin::Actions-->
								
								<!--end::Actions-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Toolbar-->
                    @include('layouts.notification')
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container">
								<!--begin::Layout-->
								<div class="d-flex flex-column ">
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<h3 class="card-title">
				Edit Media
			</h3>
           <div class="d-flex align-items-center py-1" style="margin-left: 71.2rem;">
									
									<!--end::Wrapper-->
									<!--begin::Button-->
									<a href="/section/gallery/{{$details->animal_id}}" class="btn btn-sm btn-light-primary" >Gallery List</a>
									<!--end::Button-->
								</div>
		</div>
	</div>
	<!--begin::Form-->
	<form action="{{url('/section/gallery/edit')}}" method="post" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="media_id" value="{{$details->media_id}}">
    <input type="hidden" name="animal_id" value="{{$details->animal_id}}">
		<div class="card-body">
			<div class="form-group row mb-5">
				<label class="col-form-label text-right col-lg-3 col-sm-12">Media</label>
				<div class="col-lg-4 col-md-9 col-sm-12">
					@if($details->media_type == 'image')
					<input type="file" name="media" accept="image/*" class="mb-3" onchange="readURL(this);">
					@elseif($details->media_type == 'audio')
					<input type="file" name="media" accept="audio/*" class="mb-3" onchange="readURL(this);">
					@else
					<input type="file" name="media" accept="video/*" class="mb-3" onchange="readURL(this);">
					@endif
                    <div class="replace_media">
					@if($details->media_type == 'image')
                      	<img src="{{env('APP_URL').$details->media_url}}" alt=""  width="100px" height="100px" id="replace_img" accept="image/*">
                    @elseif($details->media_type == 'audio')
                    	<video controls width="100" height="100" id="replace_img">
								<source src="{{env('APP_URL').$details->media_url}}" accept="audio/*">
							</video>
                    @else
                  		<video controls width="200" height="150" id="replace_img" accept="video/*">
								<source src="{{env('APP_URL').$details->media_url}}" >
							</video>
                    @endif
                </div>
				</div>
			</div>
		</div>

		@if($details->media_type == "image" )
		<div class="card-body">
			<div class="form-group row mb-5">
				<label class="col-form-label text-right col-lg-3 col-sm-12">Text</label>
				<div class="col-lg-4 col-md-9 col-sm-12">
					<input type="text" class="form-control form-control-lg form-control-solid" name="text" placeholder="" value="{{ $details->text }}"/>
                   
				</div>
			</div>
		</div>
		@endif
		<div class="card-footer">
			<div class="row">
				<div class="col-lg-9 ml-lg-auto">
					<button type="submit" class="btn btn-primary mr-2">Submit</button>
					<a href="/section/gallery/{{$details->animal_id}}" class="btn btn-secondary">Cancel</a>
				</div>
			</div>
		</div>
	</form>
	<!--end::Form-->
</div>
                            </div>
							<!--end::Container-->
						</div>
						<!--end::Post-->
					</div>
					<!--end::Content-->
                   
					@endsection                

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
         <script>           
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
   // alert(input.files[0].type);
        reader.onload = function(e) {
           var extension = input.files[0].type;
          if(extension == 'video/mp4' || extension == 'video/mp3')
          {
             $('.replace_media').html('<video controls width="200" height="200" id="replace_img"><source src="'+e.target.result+'" ></video>');
          	
          }else{
       		$('.replace_media').html('<img src="'+e.target.result+'" alt=""  width="200px" height="150px" id="replace_img">');
          }
        }
        reader.readAsDataURL(input.files[0]);
    }
}
         
         function remove_pic(input, cross_input){
    $('#'+input).attr('src', 'assets/media/add.png');
    $('#'+cross_input).remove();
}

function new_readURL(input) {
    
    var total_file=document.getElementById("add_new").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#image_preview').append("<li class='list-inline-item pip'><a href='javascript:void(0)' class='remove_pic' id='cross_profile_"+[i]+"' style='position: absolute;'><i class='far fa-times-circle'></i></a><img src='"+URL.createObjectURL(event.target.files[i])+"' class='mr-3 img-fluid d-block mx-auto' style='width: 100px; height:100px;'></li>");
  $('#cross_profile_'+[i]).show();
 }
}
    

            $(document).on('click','.remove_pic', function(){
                // alert('hi');
            // alert($(this).attr("id"));
                $(this).parent(".pip").remove();
                // $('.hide_img').show();
            });


// $(document).ready(function() {
//     alert('hlo');
// }); 
</script>