@extends('layouts.base')

@section('content')
<style type="text/css">
	#loader {
    display: none;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    background: rgba(0,0,0,0.25) url(assets/media/kk_2.gif) no-repeat center center;
    z-index: 99999;
}
</style>

		<!-- background-image: url(assets/media/svg/shapes/abstract-1.svg)	 -->											
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Toolbar-->
						<div class="toolbar" id="kt_toolbar">
							<!--begin::Container-->
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Content Master</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start mx-4"></span>
									<!--end::Separator-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="/" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
                                        <a href="/section" class="text-muted text-hover-primary">Content Master</a></li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark">Animals Listing</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
								<!--begin::Actions-->
								<div class="d-flex align-items-center py-1">
									
									<!--end::Wrapper-->
									<!--begin::Button-->
									<a href="/section/add" class="btn btn-sm btn-light-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_create_app">Add More</a>
									<!--end::Button-->
								</div>
								<!--end::Actions-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Toolbar-->
                    @include('layouts.notification')
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container">
								<!--begin::Card-->
								<div class="card">
									<!--begin::Card header-->
									<div class="card-header border-0 pt-6">
										<!--begin::Card title-->
										<div class="card-title">
											<!--begin::Search-->
											<div class="d-flex align-items-center position-relative my-1">
												<!--begin::Svg Icon | path: icons/duotone/General/Search.svg-->
												<!-- <span class="svg-icon svg-icon-1 position-absolute ms-6">
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24" />
															<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
															<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
														</g>
													</svg>
												</span> -->
												<!--end::Svg Icon-->
												<!-- <input type="text" data-kt-customer-table-filter="search" class="form-control form-control-solid w-250px ps-15" placeholder="Search Customers" /> -->
											</div>
											<!--end::Search-->
										</div>
										<!--begin::Card title-->
										<!--begin::Card toolbar-->
										<div class="card-toolbar">
											<!--begin::Toolbar-->
											
											<!--end::Toolbar-->
											<!--begin::Group actions-->
											
											<!--end::Group actions-->
										</div>
										<!--end::Card toolbar-->
									</div>
									<!--end::Card header-->
									<!--begin::Card body-->
									<div class="card-body pt-0" style="overflow-x: auto">
										<!--begin::Table-->
										<table class="table table-bordered align-middle table-row-dashed fs-6 gy-5" id="kt_customers_table">
											<!--begin::Table head-->
											<thead>
												<!--begin::Table row-->
												<tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
													<!-- <th class="w-10px pe-2">
														<div class="form-check me-3">
															<input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_customers_table .form-check-input" value="1" />
														</div>
													</th> -->
													
													<th class="min-w-125px">Id</th>
													<!-- <th class="min-w-125px">Profile Picture</th> -->
													<th class="min-w-125px">Animal Breed</th>
													<th class="min-w-125px">Animal Name</th>
													<th class="min-w-125px">Description</th>
													<th class="min-w-125px">Media</th>
													
													<th class="min-w-125px">Status</th>
													<!-- <th class="min-w-125px">Created Date</th> -->
													<th class="text-end min-w-70px">Actions</th>
												</tr>
												<!--end::Table row-->
											</thead>
											<!--end::Table head-->
											<!--begin::Table body-->
											<tbody class="fw-bold text-gray-600">
                                            @php $count=1; @endphp
												@foreach($lists as $list)
												<tr>
													<!--begin::Checkbox-->
												<!-- 	<td>
														<div class="form-check form-check-sm form-check-custom form-check-solid">
															<input class="form-check-input" type="checkbox" value="1" />
														</div>
													</td> -->
													<!--end::Checkbox-->
													<td>
														<a href="" class="text-gray-600 mb-1">{{ $list->animal_id }}</a>
													</td>
													<!-- <td>fghfgjh</td> -->
<!-- 													<td>
														<div class="symbol symbol-45px me-5">
                                                            @if($list->pic)
                                                             <img src="{{ url($list->pic)  ?? '' }}" alt="" />
                                                            @else
                                                            <img src="{{ url('assets/media/avatars/blank.png')}}" alt="" />
                                                            @endif
                                                        </div>
													</td> -->
													
													<!--begin::Name=-->
													<td>
                                                    	
														<span class="text-gray-800 mb-1">{{ ucfirst($list->animal_breed )  ?? '' }}</span>
													</td>
                                                    <td>
                                                    	
														<span class="text-gray-800 mb-1">{{ ucfirst($list->animal_name )  ?? '' }}</span>
													</td>
													<td>
														<span class="text-gray-600 mb-1">{{ substr($list->description,0,20) ?? ''}} .. .  .</span>
													</td>
                                                    <td>
                                                        @if( sizeof($list->getSectionMedia) > 0)
                                                    		<a href="section/gallery/{{$list->animal_id}}" class="btn btn-sm btn-light-primary mb-1">{{ sizeof($list->getSectionMedia)}}</a>
                                                        @else
                                                    		<a href="/section/gallery/add/{{$list->animal_id}}" class="btn btn-sm btn-light-primary mb-1">Add Media</a>
                                                        @endif
													</td>
													
													<td>
														@if($list->status == 'active')
                                                    <span class="btn btn-sm btn-light-success">  {{ $list->status ?? ''}}</span>
														@else
                                                    <span class="btn btn-sm btn-light-danger">  {{ $list->status ?? ''}}</span>
													
														@endif

													</td>
													
													<!--begin::Action=-->
													<td class="text-end" id="show_status_{{$list->animal_id}}">
														
														<a href="/section/view/{{$list->animal_id}}" class="btn btn-sm btn-light-info">View</a>

														<!-- @if($list->status =='active')
															<a href="javascript:void(0);" onclick="change_status('{{$list->id}}','{{$list->status}}');" class="btn btn-sm btn-light-success">Active</a>
														@else
															<a href="javascript:void(0);" onclick="change_status('{{$list->id}}','{{$list->status}}');" class="btn btn-sm btn-light-danger" data-kt-customer-table-filter="delete_row">Inactive</a>
														@endif -->
															<a href="/section/change_status/{{$list->animal_id }}/{{$list->status}}" onclick="return confirm('You Want to Change the Status?')">
															    @if($list->status=='active') 
                                                                     <span class="btn btn-sm btn-light-danger">Inactive</span> 
																
															    @else 
															        	<span class="btn btn-sm btn-light-success">Active</span>
															    @endif
														    </a>
															
													</td>
										
													<!--end::Action=-->
												</tr>
                                           @php $count++; @endphp
												@endforeach
											</tbody>
											<!--end::Table body-->

											
                                    </table>
										<!--end::Table-->
										{{ $lists->links() }}
									</div>
									<!--end::Card body-->
								</div>
								<!--end::Card-->
								  </div>

                                                <!--end::Table container-->
                              </div>
                                            <!--begin::Body-->

                    </div>
                                        <!--end::Tables Widget 9-->

<div class="modal fade" id="kt_modal_create_app" tabindex="-1" aria-hidden="true">
			<!--begin::Modal dialog-->
			<div class="modal-dialog modal-dialog-centered mw-900px">
				<!--begin::Modal content-->
				<div class="modal-content">
					<!--begin::Modal header-->
					<div class="modal-header">
						<!--begin::Modal title-->
						<h2>Create</h2>
						<!--end::Modal title-->
						<!--begin::Close-->
						<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
							<!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
							<span class="svg-icon svg-icon-1">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
										<rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
										<rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
									</g>
								</svg>
							</span>
							<!--end::Svg Icon-->
						</div>
						<!--end::Close-->
					</div>
					<!--end::Modal header-->
                @extends('layouts.notification')
					<!--begin::Modal body-->
					<div class="modal-body py-lg-10 px-lg-10">
						<!--begin::Stepper-->
						<div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_modal_create_app_stepper">
							<!--begin::Aside-->
							
							<!--begin::Aside-->
							<!--begin::Content-->
							<div class="flex-row-fluid py-lg-5 px-lg-15">
								<!--begin::Form-->
								<form action="{{url('/section/add')}}" class="form" method="post" enctype="multipart/form-data">
                                @csrf
									<!--begin::Step 1-->
                                
									<div class="current" data-kt-stepper-element="content">
										<div class="w-100">
											<!--begin::Input group-->
											<div class="fv-row mb-10">
												<!--begin::Label-->
												<label class="d-flex align-items-center fs-5 fw-bold mb-2">
													<span class="required">Animal Breed</span>
													<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify animal breed"></i>
												</label>
												<!--end::Label-->
												<!--begin::Input-->
												<input type="text" class="form-control form-control-lg form-control-solid" name="animal_breed" placeholder="Enter animal breed" value="" required/>
												<!--end::Input-->
											</div>
											<!--end::Input group-->
                                            <!--begin::Input group-->
											<div class="fv-row mb-10">
												<!--begin::Label-->
												<label class="d-flex align-items-center fs-5 fw-bold mb-2">
													<span class="required">Animal Name</span>
													<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify animal name"></i>
												</label>
												<!--end::Label-->
												<!--begin::Input-->
												<input type="text" class="form-control form-control-lg form-control-solid" name="animal_name" placeholder="Enter animal name" value="" required/>
												<!--end::Input-->
											</div>
											<!--end::Input group-->
                                            <!--begin::Input group-->
											<div class="fv-row mb-10">
												<!--begin::Label-->
												<label class="d-flex align-items-center fs-5 fw-bold mb-2">
													<span class="required">Description</span>
													<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify section description"></i>
												</label>
												<!--end::Label-->
												<!--begin::Input-->
												<textarea type="text" class="form-control form-control-lg form-control-solid" name="description" placeholder="Enter description" rows="10" cols="10" required></textarea>
												
												<!--end::Input-->
											</div>
											
										
											<div class="fv-row mb-10">
												<!--begin::Label-->
												<label class="d-flex align-items-center fs-5 fw-bold mb-2">

													<span>Upload Video</span>
													<i class="" data-bs-toggle="tooltip" title="Upload Video"></i>
												</label>
												<!--end::Label-->
												<!--begin::Input-->
                                            
												<div>
                    								<input type="file" name="video" id="add_newvideo" class="form-control" accept="video/*" value="" onchange="viewVideo(this);"
                    								/>
                    								<!-- <label for="add_newvideo">
			                        					<img src="{{asset('assets/media/add.png')}}" class="img-fluid d-block mx-auto" alt="" id="add_img" style="height: 80px;width: 80px;"> 
			                        					<span></span>
			                    					</label> -->
                								</div>
                								<div>(Only Videos Files are acceptable)</div>
                								<div class="replace_media">
                								</div>
												<!--end::Input-->
											</div>

											<div class="fv-row mb-10">
												<!--begin::Label-->
												<label class="d-flex align-items-center fs-5 fw-bold mb-2">

													<span>Upload Thumbnail Of Video</span>
													<i class="" data-bs-toggle="tooltip" title="Upload Thumbnail Of Video"></i>
												</label>
												<!--end::Label-->
												<!--begin::Input-->
                                            
												<div>
                    								<input type="file" name="thumbnail" id="thumbnail" accept="image/*" class="form-control"  value="" onchange="viewVideothumbnail(this);"
                    								/>
                    								
                								</div>
                								<div>(Only Image Thumbnail are acceptable)</div>
                								<div class="replace_thumbnail">
                								</div>
												<!--end::Input-->
											</div>

											<div class="fv-row mb-10">
												<!--begin::Label-->
												<label class="d-flex align-items-center fs-5 fw-bold mb-2">

													<span>Upload Audio</span>
													<i class="" data-bs-toggle="tooltip" title="Upload Audio"></i>
												</label>
												<!--end::Label-->
												<!--begin::Input-->
                                            
												<div>
                    								<input type="file" name="audio" id="add_newaudio" class="form-control" accept="audio/*" value="" onchange="viewAudio(this);" />
                    								<!-- <label for="add_newaudio">
			                        					<img src="{{asset('assets/media/add.png')}}" class="img-fluid d-block mx-auto" alt="" id="add_img" style="height: 80px;width: 80px; margin-top: 2px;">
			                        					<span></span>
			                    					</label> -->
                								</div>
                								<div>(Only Audio Files are acceptable)</div>
                								<div id="replace_audio"></div>
                								
												<!--end::Input-->
											</div>


											<!--end::Input group-->
											<div class="fv-row mb-10">
												<!--begin::Label-->
											<div class="images_div">	
											<div class="form-group row">
												<div class="col-lg-6">
													<label class="d-flex align-items-center fs-5 fw-bold mb-2 required">
												Images
												
												</label>
                                               <div class="add_imgs">
                    				
                            					<div class="form-group">
                                					<div class="add_div">
                                    					<input type="file" name="media[]" id="add_new_1" class="form-control" onchange="readURL(this);" accept="image/png, image/jpeg ,image/svg" required=""/>
                                    				<!-- 	<label for="add_new_1">
                                        					<img src="{{asset('assets/media/add.png')}}" class="img-fluid d-block mx-auto" alt="" id="add_img" style="height: 80px;width: 80px;">
                                        					<span></span>
                                    					</label> -->
                                					</div>
                            					</div>
                                 					
                           						</div>
												</div>
												<div class="col-lg-6">
													<label class="d-flex align-items-center fs-5 fw-bold mb-2  required">Text
													</label>
														<input type="text" class="form-control form-control-lg form-control-solid " name="text[]" value="" placeholder="Enter image text" />
												</div>
												<div id="image_preview_1">
													
												</div>
											</div>
										</div>
												
												<div class="row" id="images_html">
													
												</div>

												<div class="btn-side">
	                    							<a><button type="button" class="btn btn-success" id="add_new_row"style="float:right;">+</button></a>
	                							</div>

											</div>
	                						
											 

											<div class="fv-row mb-10">
												<!--begin::Label-->
												<label class="d-flex align-items-center fs-5 fw-bold mb-2">
													<span class="required">Feeding Time Start</span>
													<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify start feeding time"></i>
												</label>
												<!--end::Label-->
												<!--begin::Input-->
                                            
												<input type="time" class="form-control form-control-lg form-control-solid " name="start_time" value="" required />
												<!--end::Input-->
											</div>

                                            <div class="fv-row mb-10">
												<!--begin::Label-->
												<label class="d-flex align-items-center fs-5 fw-bold mb-2">
													<span class="required">Feeding Time End</span>
													<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify end feeding time" ></i>
												</label>
												<!--end::Label-->
												<!--begin::Input-->
												<input type="time" class="form-control form-control-lg form-control-solid timepicker" name="end_time" value="" required id="session_date"/>
												<!--end::Input-->
											</div>
										</div>
									</div>
									<!--end::Step 1-->
									
									
									<div id='loader'></div>
									
									<!--begin::Actions-->
									<div class="d-flex flex-stack pt-10">
										<!--begin::Wrapper-->
										<div class="me-2">
											<a href="" class="btn btn-lg btn-primary" data-bs-dismiss="modal">
											<!--begin::Svg Icon | path: icons/duotone/Navigation/Left-2.svg-->
											<span class="svg-icon svg-icon-3 me-1">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24" />
														<rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000)" x="14" y="7" width="2" height="10" rx="1" />
														<path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997)" />
													</g>
												</svg>
											</span>
											<!--end::Svg Icon-->Back</a>
										</div>
										<!--end::Wrapper-->
										<!--begin::Wrapper-->
										<div>
											
											<button type="submit" class="btn btn-lg btn-primary" >Save
											<!--begin::Svg Icon | path: icons/duotone/Navigation/Right-2.svg-->
											<span class="svg-icon svg-icon-3 ms-1 me-0">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24" />
														<rect fill="#000000" opacity="0.5" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1" />
														<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
													</g>
												</svg>
											</span>
											</button>
										</div>
										<!--end::Wrapper-->
									</div>
									<!--end::Actions-->
								</form>
								<!--end::Form-->
							</div>
							<!--end::Content-->
						</div>
						<!--end::Stepper-->
					</div>
					<!--end::Modal body-->
				</div>
				<!--end::Modal content-->
			</div>
			<!--end::Modal dialog-->
		</div>
                               	 

@endsection       


@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>

var j = 0;  
function readURL(input) {
j++;
	 if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image_preview_'+j).html('<img src="'+e.target.result+'" style="width: 100px; height:100px;margin-top: 2%"/>');
        };
        reader.readAsDataURL(input.files[0]);
    }

  // if (input.files && input.files[0]) {
  //       var reader = new FileReader();
  //  // alert(input.files[0].type);
  //       reader.onload = function(e) {
  //          var extension = input.files[0].type;
         
  //           $('#image_preview').append('<img src="'+URL.createObjectURL(event.target.files[i])+'" class="mr-3 img-fluid d-block mx-auto" style="width: 100px; height:100px;">');
          	
          
  //       }
  //       reader.readAsDataURL(input.files[0]);
  //   }
  
 
}
 
function viewVideo(input) {
  
 	// alert('working');
  if (input.files && input.files[0]) {
        var reader = new FileReader();
   // alert(input.files[0].type);
        reader.onload = function(e) {
           var extension = input.files[0].type;
         
             $('.replace_media').html('<video controls width="200" height="200" id="replace_img"><source src="'+e.target.result+'" ></video>');
          	
          
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function viewAudio(input) {
  
 	// alert('working');
  if (input.files && input.files[0]) {
        var reader = new FileReader();
   // alert(input.files[0].type);
        reader.onload = function(e) {
           var extension = input.files[0].type;
         
             $('#replace_audio').html('<audio id="audio" style="margin-top: 2%;" controls><source src="'+e.target.result+'" id="src" /></audio>');
          	
          
        }
        reader.readAsDataURL(input.files[0]);
    }
}
  
  function viewVideothumbnail(input) {
  
 	// alert('working');
  if (input.files && input.files[0]) {
        var reader = new FileReader();
   // alert(input.files[0].type);
        reader.onload = function(e) {
           var extension = input.files[0].type;
         
             $('.replace_thumbnail').html('<img src="'+e.target.result+'" style="height: 149px;margin-top: 19px;"/>');
          	
          
        }
        reader.readAsDataURL(input.files[0]);
    }
}  

    
$(document).on('click','.remove_pic', function(){
    // alert('hi');
    
    $(this).parent(".pip").remove();
    // $('.hide_img').show();
});
 var x = 1;  
$(document).on('click','#add_new_row',function(){
	// alert('hii');
	x++;
	 // var maxField = 10;
	var images_div = '<div class="form-group row images_div_'+x+'"><a href="javascript:void(0);"><button type="button" class="btn btn-danger" id="remove_div_'+x+'" style="float:right;">❌</button></a><div class="col-lg-6"><label class="d-flex align-items-center fs-5 fw-bold mb-2"><span class="required">Images</span></label><div class="add_imgs"><div class="form-group"><div class="add_div"><input type="file" name="media[]" id="add_new_'+x+'" class="form-control" onchange="readURL(this);" accept="image/png, image/jpeg ,image/svg" required/></div></div></div></div><div class="col-lg-6"><label>Text:</label><input type="text" class="form-control form-control-lg form-control-solid " name="text[]" value="" placeholder="Enter image text" required /></div><div id="image_preview_'+x+'"></div><div class="btn-side"></div></div>'; 
	
	 
             $("#images_html").append(images_div); //Add field html
     

         //Once remove button is clicked
       $("#images_html").on("click", "#remove_div_"+x, function() {
       	// alert('working');
    		 if (x == 1) {
        		// alert("No more author to remove");
        		return false;
    		}


    		$('.images_div_'+x).remove();
    		x--;

  			//  $(".row" + counter).remove();
    	
		});


});

</script>



<script>
$(function() {
    $( "form" ).submit(function() {
        $('#loader').show();
    });
});
</script>
