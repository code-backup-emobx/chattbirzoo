@extends('layouts.base')

@section('content')

<!--end::Header-->
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
	<!--begin::Toolbar-->
	<div class="toolbar" id="kt_toolbar">
		<!--begin::Container-->
		<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
			<!--begin::Page title-->
			<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
				<!--begin::Title-->
				<h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Content Master</h1>
				<!--end::Title-->
				<!--begin::Separator-->
				<span class="h-20px border-gray-200 border-start mx-4"></span>
				<!--end::Separator-->
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
					<!--begin::Item-->
					<li class="breadcrumb-item text-muted">
						<a href="{{route('home')}}" class="text-muted text-hover-primary">Home</a>
					</li>
					<!--end::Item-->
					<!--begin::Item-->
					<li class="breadcrumb-item">
						<span class="bullet bg-gray-200 w-5px h-2px"></span>
					</li>
					<!--end::Item-->
					<!--begin::Item-->
					<li class="breadcrumb-item text-muted">
                    <a href="/section" class="text-muted text-hover-primary">Content Master</a></li>
					<!--end::Item-->
					<!--begin::Item-->
					<li class="breadcrumb-item">
						<span class="bullet bg-gray-200 w-5px h-2px"></span>
					</li>
					<!--end::Item-->
					<!--begin::Item-->
					<li class="breadcrumb-item text-dark">Animal Media</li>
					<!--end::Item-->
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page title-->
			<!--begin::Actions-->
			<div class="d-flex align-items-center py-1">
				
				<!--end::Wrapper-->
				<!--begin::Button-->
				<a href="/section/gallery/{{$details->animal_id}}" class="btn btn-sm btn-light-primary" >Gallery List</a>
				<!--end::Button-->
			</div>
			<!--end::Actions-->
		</div>
		<!--end::Container-->
	</div>
	<!--end::Toolbar-->
		@include('layouts.notification')
			<!--begin::Post-->
			<div class="post d-flex flex-column-fluid" id="kt_post">
				<!--begin::Container-->
				<div id="kt_content_container" class="container">
					<!--begin::Layout-->
					<div class="d-flex flex-column ">
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-title">
						Add Media
					</h3>
				</div>
			</div>
			<!--begin::Form-->
		  
			<form method="post" action="{{ url('/section/gallery/add_video/'.$details->animal_id) }}" enctype="multipart/form-data" id="form_add_video">
		    @csrf
		        <input type="hidden" name="animal_id" id="video_animal_id" value="{{$details->animal_id}}">
				<div class="card-body">
					<div class="form-group row mb-5">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Upload Video</label>
				            <div class="col-lg-4 col-md-9 col-sm-12">
            					<div class="form-group">
                					<div>
                    					<input type="file" name="video" id="add_new" class="inputfile" accept="video/*" value="" onchange="saveVideo(this);"
                    					/>
                    					<label for="add_new">
                        					<img src="{{asset('assets/media/add.png')}}" class="img-fluid d-block mx-auto" alt="" id="add_img">
                        					<span></span>
                    					</label>
                					</div>
                					@if($exist_video)
                					<div>
                						<video width="220" height="140" controls>
  											<source src="{{ $exist_video->media_url }}" >
										</video><a href="/section/gallery/delete_video/{{ $exist_video->media_id}}/{{$details->animal_id}}"><button type="button" class="btn btn-secondary btn-sm" style="margin-left: -3%;margin-bottom: 15%;position:absolute;"><i class="fa fa-times"></i></button></a>
                					</div>
                					@endif
            					</div>
		   					</div>
					</div>
				</div>
			</form>
			<form method="post" action="{{ url('/section/gallery/add_thumbnail/'.$details->animal_id) }}" enctype="multipart/form-data" id="form_add_thumbnail">
		    @csrf
		        <input type="hidden" name="animal_id" id="thumbnail_animal_id" value="{{$details->animal_id}}">
				<div class="card-body">
					<div class="form-group row mb-5">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Upload Thumbnail Of Video</label>
				            <div class="col-lg-4 col-md-9 col-sm-12">
            					<div class="form-group">
                					<div>
                    					<input type="file" name="thumbnail" id="thumbnail" class="inputfile" accept="image/*" value="" onchange="saveThumbnail(this);"
                    					/>
                    					<label for="thumbnail">
                        					<img src="{{asset('assets/media/add.png')}}" class="img-fluid d-block mx-auto" alt="" id="add_img">
                        					<span></span>
                    					</label>
                					</div>
                					@if($exist_video)
                					<div>
                						<img src="{{ $exist_video->thumbnail }}" style="height:110px;"/>
                					
                					</div>
                					@endif
            					</div>
		   					</div>
					</div>
				</div>
			</form>

			<form method="post" action="{{ url('/section/gallery/add_audio/'.$details->animal_id) }}" enctype="multipart/form-data" id="form_add_audio">
		    @csrf
		      <input type="hidden" name="animal_id" id="video_animal_id" value="{{$details->animal_id}}">
				<div class="card-body">
					<div class="form-group row mb-5">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Upload Audio</label>
				            <div class="col-lg-4 col-md-9 col-sm-12">
            					<div class="form-group">
                					<div>
                    					<input type="file" name="audio" id="add_audio" class="inputfile" accept="audio/*" value="" onchange="saveaudio(this);"/>
                    					<label for="add_audio">
                        					<img src="{{asset('assets/media/add.png')}}" class="img-fluid d-block mx-auto" alt="" id="add_img">
                        					<span></span>
                    					</label>
                					</div>
                					@if($exist_audio)
                					<div>
                						<audio controls>
  											<source src="{{ $exist_audio->media_url }}">
										</audio><a href="/section/gallery/delete_audio/{{ $exist_audio->media_id}}/{{$details->animal_id}}"><button type="button" class="btn btn-secondary btn-sm" style="position:absolute;margin-top: -18px;margin-left: -8px;margin-bottom: -15px;"><i class="fa fa-times"></i></button></a>
                					</div>
                					@endif
            					</div>
		   					</div>
					</div>
				</div>
			</form>

				<section style="border:1px dotted grey;margin:10px;padding:20px">
            <h3>Multiple Images &Text  </h3>
            <br>
           <div class="accordion" id="accordionExample">
           	@foreach($exist_images as $image)

  			<div class="accordion-item">
    			<h2 class="accordion-header" id="heading{{ $image->media_id}}">
			    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $image->media_id}}" aria-expanded="true" aria-controls="collapse{{$image->media_id}}">
			        {{ $image->text}}
			    </button>
    			</h2>
		    <div id="collapse{{ $image->media_id}}" class="accordion-collapse collapse" aria-labelledby="heading" data-bs-parent="#accordionExample">
		    	<div class="btn-side">
                    <a href="/section/delete_row/{{ $image->media_id}}/{{$details->animal_id}}" onclick="return confirm('Are you sure you want to remove this row?');"><button type="button" class="btn btn-danger" id="delete_row_{{$image->media_id}}"style="float:right;"><i class="fa fa-times"></i></button></a>
                </div> 
		      <div class="accordion-body">
		      <img src="{{ $image->media_url}}" style="height:200px;width:200px">
		     	
		      </div>
		    </div>
		  </div>
		  @endforeach
	  
	 
	</div>
          <br>
		<form method="post" action="{{url('/section/gallery/add_images/'.$details->animal_id)}}" enctype="multipart/form-data" id="form_add_images">
		    @csrf
		      <input type="hidden" name="animal__id" value="{{$details->animal_id}}">
				<div class="card-body add_more_images">
				
					<div class="form-group row mb-5">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Upload Image</label>
				            <div class="col-lg-4 col-md-9 col-sm-12">
            					<div class="form-group">
                					<div>
                    					<input type="file" name="image" id="add_images" class="inputfile" accept="image/png, image/jpeg ,image/svg" onchange="readURL(this);" required />
                    					<label for="add_images">
                        					<img src="{{asset('assets/media/add.png')}}" class="img-fluid d-block mx-auto" alt="" id="add_img">
                        					<span></span>
                    					</label>
                    				</div>
            					</div>
							</div>

							<div class="col-lg-4 col-md-9 col-sm-12">
            					<div class="form-group">
            						<label class="col-form-label text-right col-lg-3 col-sm-12">Text</label>
                					<div>
                    					<input type="text" name="text" id="text" class="form-control" placeholder="Enter text of image" value="" required/>
                					</div>
            					</div>
							</div>
							<div class="show_div" style="display:none;">
							 <img src="" id="show_selected_image" style="margin-left:20%;height:150px;width:21%;"><button type="button" class="btn btn-secondary btn-sm" id="delete_image"style="margin-left: -3%;margin-bottom: 15%;"><i class="fa fa-times"></i></button></div>
						</div>

					</div>
			<!-- <div class="btn-side">
                    <button type="submit" class="btn btn-success" id="save_image_text" style="float:right;">Save Images/Text</button>
                </div>  -->
				</section>
					
			</form>	
				
			<div class="card-footer">
					<div class="row">
						<div class="col-lg-9 ml-lg-auto">
<!-- 							<input type="submit" class="btn btn-primary mr-2" value="Submit"> -->
							<a onclick="backurl();" class="btn btn-primary mr-2">Save</a>
							<!-- <a href="/section/gallery/{{$details->animal_id}}" class="btn btn-primary mr-2">Save</a> -->
							<a href="/section" class="btn btn-secondary">Cancel</a>
						</div>
					</div>
				</div>
			<!--end::Form-->
		</div>
		                            </div>
									<!--end::Container-->
								</div>
								<!--end::Post-->
							</div>
					<!--end::Content-->
                   
					@endsection                

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">

function backurl(){
	 event.preventDefault();
	var video_animal_id = $("#video_animal_id").val();
	// alert(video_animal_id);
	var text = $("#text").val();
	 var image_file = $('#add_images').val();
	if(!image_file){
  	 
  	 Swal.fire("Error!", "Please Choose File.", "error");
  	return false;
  	}else if(!text){
  		 Swal.fire("Error!", "Please Enter the text.", "error");
  	return false;
  	}else{

  		 $('#form_add_images').submit();

  		// $.ajax({
             
    //             url: "/section/gallery/final_submit",
    //           type:"POST",
    //           data:{
    //             "_token": "{{ csrf_token() }}",
    //             video_animal_id:video_animal_id,
    //             },
    //             success: function(result){
    //               console.log(result);
    //               if(result.status == 200){
    //               	window.location.href="/section/gallery/"+video_animal_id;
    //               }else if(result.status == 400){

    //               	// alert(result.msg);
    //               	Swal.fire("Error!",result.msg , "error");
    //               }
    //             }

    //       });

  	} 
	
}

function readURL(input) {
var url = input.value;
var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

    var reader = new FileReader();

    reader.onload = function (e) {
    	$(".show_div").show();
        $('#show_selected_image').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);


}


$('#delete_image').on('click',function(){
	// alert('working');
	$("#add_images").val("");
	$(".show_div").hide();

});

 
// $('#save_image_text').click(function(event) {
//     event.preventDefault();

//   var text = $('#text').val();
//   var image_file = $('#add_images').val();
//   // alert(image_file);

//    if(!image_file){
//   	Swal.fire("Error!", "Please Choose File.", "error");
//   	return false;
//   }
//   if(!text){
//   	 Swal.fire("Error!", "Please Enter the text.", "error");
//   	return false;
//   } 
 

//   $('#form_add_images').submit();
  
// });

</script>

<script type="text/javascript">

	function saveVideo(value){
		// alert(video);
		  event.preventDefault();
		// var video = value.value;
		// if(video){
		// 	confirm('Are you sure you want to update the existing video?');
     
		// }
       $('#form_add_video').submit();
	}

</script>

<script type="text/javascript">
	function saveaudio(value){
		// alert('working');
		  event.preventDefault();
		// var video = value.value;
		// if(video){
		// 	confirm('Are you sure you want to update the existing video?');
     
		// }
       $('#form_add_audio').submit();

	}

	function saveThumbnail(value){

		 event.preventDefault();
		  $('#form_add_thumbnail').submit();
	}
</script>

@endsection        
