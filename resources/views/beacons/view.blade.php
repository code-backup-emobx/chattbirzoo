@extends('layouts.base')

@section('content')

					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Toolbar-->
						<div class="toolbar" id="kt_toolbar">
							<!--begin::Container-->
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Beacon Details</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start mx-4"></span>
									<!--end::Separator-->
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
											<a href="{{route('home')}}" class="text-muted text-hover-primary">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-muted">
                                        <a href="/beacon" class="text-muted text-hover-primary">Beacons</a></li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<span class="bullet bg-gray-200 w-5px h-2px"></span>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-dark">Beacon Details</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
								<!--begin::Actions-->
								
								<!--end::Actions-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Toolbar-->
                    
                   @include('layouts.notification')
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container">
								<!--begin::Layout-->
								<div class="d-flex flex-column ">
                                
									<!--begin::Sidebar-->
<!-- 									<div class="flex-column flex-lg-row-auto w-100 w-xl-400px mb-10"> -->
										<!--begin::Card-->
<!-- 										<div class="card mb-5 mb-xl-8"> -->
											<!--begin::Card body-->
<!-- 											<div class="card-body pt-15"> -->
												<!--begin::Summary-->
<!-- 												<div class="d-flex flex-center flex-column mb-5"> -->
                                
                                <div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<h3 class="card-title">
				Edit Beacon
			</h3>
		</div>
	</div>
	<!--begin::Form-->
	<form action="{{url('/beacon/edit')}}" class="form" method="post">
    @csrf
    <input type="hidden" name="id" value="{{$details->beacon_id}}">
        
  <!--   <div class="card-body">
		<div class="form-group row mb-5">
			<label class="col-form-label text-right col-lg-3 col-sm-12">Major Id</label>
			<div class="col-lg-4 col-md-9 col-sm-12">
				<input class="form-control text-right" id="major_id" placeholder="Enter major Id" type="text" value="{{ $details->major_id  ?? '' }}" name="major_id" minlength="1" maxlength="20"/>
            <span class="form-text text-muted major_error"></span>
			</div>
            
		</div>
	</div> -->
    <div class="card-body">
		<div class="form-group row mb-5">
			<label class="col-form-label text-right col-lg-3 col-sm-12">Beacon Id</label>
			<div class="col-lg-4 col-md-9 col-sm-12">
				<input class="form-control text-right" id="unique_id" placeholder="Enter unique Id" type="text" value="{{ $details->unique_id  ?? '' }}" name="unique_id" minlength="1" maxlength="22"/>
            <span class="form-text text-muted unique_error"></span>
			</div>
	</div>
		
	</div>
		<div class="card-body">
			<div class="form-group row mb-5">
				<label class="col-form-label text-right col-lg-3 col-sm-12">Beacon Name</label>
				<div class="col-lg-4 col-md-9 col-sm-12">
					<input class="form-control text-right" placeholder="Enter beacon name" type="text" value="{{ ucfirst($details->beacon_name )  ?? '' }}" name="name"/>
				</div>
			</div>
			
		</div>
		<div class="card-footer">
			<div class="row">
				<div class="col-lg-9 ml-lg-auto">
					<button type="submit" class="btn btn-primary mr-2">Submit</button>
					<a href="/beacon" class="btn btn-secondary">Cancel</a>
<!-- 					<a href="#" onclick="location.href = document.referrer; return false;" class="btn btn-secondary">Cancel</a> -->
				</div>
			</div>
		</div>
	</form>
	<!--end::Form-->
</div>
													
<!-- 												</div> -->
												<!--end::Summary-->
												<!--begin::Details toggle-->
												
												<!--end::Details toggle-->
												
												<!--end::Details content-->
<!-- 											</div> -->
											<!--end::Card body-->
<!-- 										</div> -->
										<!--end::Card-->
										<!--begin::Connected Accounts-->
										
											
									<!--end::Connected Accounts-->
<!-- 									</div> -->
									<!--end::Sidebar-->
									<!--begin::Content-->
									
									<!--end::Content-->
                                    
								</div>
								<!--end::Layout-->
								<!--begin::Modals-->
								
								<!--end::Modals-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Post-->
					</div>
					<!--end::Content-->
                    
					@endsection                

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function () {
      //called when key is pressed in textbox
        $("#major_id").keypress(function (e) {
         //if the letter is not digit then display error and don't type anything
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            // $(".error").addClass("text-danger");
            $(".major_error").html("<font color='red'>Digits Only </font>").show().fadeOut("slow");
                   return false;
        }
        var get_major=$("#major_id").val();
       if(get_major == '0')
       {
       		$(".major_error").html("<font color='red'>This value should be greater than zero  </font>").show().fadeOut("slow");
                   return false;
       }
       });
       $("#minor_id").keypress(function (e) {
         //if the letter is not digit then display error and don't type anything
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            // $(".error").addClass("text-danger");
             
          
            $(".minor_error").html("<font color='red'>Digits Only </font>").show().fadeOut("slow");
                   return false;
           
        }
         var get_minor=$("#minor_id").val();
       if(get_minor == '0')
       {
       		$(".minor_error").html("<font color='red'>This value should be greater than zero </font>").show().fadeOut("slow");
                   return false;
       }
       });

        $(function () {
        $("#unique_id").keypress(function (e) {
            var keyCode = e.keyCode || e.which;
 
            $("#unique_error").html("");
 
            //Regex for Valid Characters i.e. Alphabets and Numbers.
            var regex = /^[A-Za-z0-9]+$/;
 
            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
                $("#unique_error").html("Only Alphabets and Numbers allowed.");
            }
 
            return isValid;
        });
    });
       

    });
</script>

<!--  <script>
$(document).ready(function() {
    alert('hlo');
}); 
</script> -->