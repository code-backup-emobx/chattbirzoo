<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('home');
})->middleware(['auth'])->name('home');

Route::group(['middleware' => 'auth'], function() {
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('users');
Route::get('/user/view/{id}', [App\Http\Controllers\UserController::class, 'user_view']);
Route::post('/user/edit', [App\Http\Controllers\UserController::class, 'update']);
Route::post('/user/add', [App\Http\Controllers\UserController::class, 'store']);

Route::get('/admin', [App\Http\Controllers\UserController::class, 'admin_list'])->name('admin');
Route::get('/admin/view/{id}', [App\Http\Controllers\UserController::class, 'view']);
Route::post('/admin/edit', [App\Http\Controllers\UserController::class, 'update']);
Route::get('/admin/password', [App\Http\Controllers\UserController::class, 'password']);
Route::post('/admin/password', [App\Http\Controllers\UserController::class, 'change_password']);
Route::post('/admin/add', [App\Http\Controllers\UserController::class, 'store']);

Route::get('/admin/change_pswd/{user_id}', [App\Http\Controllers\UserController::class, 'changePassword']);
Route::post('/admin/change_pswd/{user_id}', [App\Http\Controllers\UserController::class, 'savechangePassword']);

Route::get('/users/change_status/{id}/{status}', [App\Http\Controllers\UserController::class, 'change_status']);

Route::get('/section', [App\Http\Controllers\SectionController::class, 'index'])->name('section');
Route::get('/section/view/{id}', [App\Http\Controllers\SectionController::class, 'view']);
Route::post('/section/edit', [App\Http\Controllers\SectionController::class, 'update']);
Route::post('/section/add', [App\Http\Controllers\SectionController::class, 'create']);
Route::get('/section/change_status/{id}/{status}', [App\Http\Controllers\SectionController::class, 'status']);
Route::get('/mapping', [App\Http\Controllers\SectionController::class, 'mapping'])->name('mapping');
Route::post('map', [App\Http\Controllers\SectionController::class, 'beacon_map'])->name('map');
Route::get('/unmapped', [App\Http\Controllers\SectionController::class, 'unmapped']);

Route::post('/section/gallery/add', [App\Http\Controllers\SectionController::class, 'gallery_store']);
Route::get('/section/gallery/add/{id}', [App\Http\Controllers\SectionController::class, 'gallery_create']);
Route::get('/section/gallery/{id}', [App\Http\Controllers\SectionController::class, 'gallery_list']);
Route::get('/section/gallery/edit/{id}', [App\Http\Controllers\SectionController::class, 'gallery_edit']);
Route::post('/section/gallery/edit', [App\Http\Controllers\SectionController::class, 'edit_media']);
Route::get('/section/gallery/delete/{id}', [App\Http\Controllers\SectionController::class, 'gallery_delete']);

Route::post('/section/gallery/add/{animal_id}', [App\Http\Controllers\SectionController::class, 'saveMedia']);

Route::post('/section/gallery/add_images/{animal_id}',[App\Http\Controllers\SectionController::class, 'saveimages']);
Route::get('/section/delete_row/{media_id}/{animal_id}',[App\Http\Controllers\SectionController::class, 'deleteImageRow']);

Route::post('/section/gallery/add_video/{animal_id}',[App\Http\Controllers\SectionController::class, 'saveVideo']);
Route::post('/section/gallery/add_audio/{animal_id}',[App\Http\Controllers\SectionController::class, 'saveAudio']);

Route::get('/section/gallery/delete_video/{media_id}/{animal_id}',[App\Http\Controllers\SectionController::class, 'deleteVideoRow']);
Route::get('/section/gallery/delete_audio/{media_id}/{animal_id}',[App\Http\Controllers\SectionController::class, 'deleteAudioRow']);
Route::post('/section/gallery/final_submit',[App\Http\Controllers\SectionController::class, 'finalSubmit']);

Route::post('/section/gallery/add_thumbnail/{animal_id}',[App\Http\Controllers\SectionController::class, 'saveThumbnail']);

Route::get('/beacon', [App\Http\Controllers\BeaconController::class, 'index'])->name('beacon');
Route::get('/beacon/view/{id}', [App\Http\Controllers\BeaconController::class, 'view']);
Route::post('/beacon/edit', [App\Http\Controllers\BeaconController::class, 'update']);
Route::post('/beacon/add', [App\Http\Controllers\BeaconController::class, 'create']);
Route::get('/beacon/change_status/{id}/{status}', [App\Http\Controllers\BeaconController::class, 'status']);

Route::get('/beacon_hits/{user?}',[App\Http\Controllers\BeaconController::class, 'beaconsHits'])->name('beacon_hits');
Route::get('/user_beacon_hits',[App\Http\Controllers\BeaconController::class, 'userBeaconsHits'])->name('user_beacon_hits');
Route::get('/count_user_beacon_hits/{user?}',[App\Http\Controllers\BeaconController::class, 'countBeaconsHits'])->name('count_user_beacon_hits');

Route::post('map_beacon', [App\Http\Controllers\SectionController::class, 'map_beacon'])->name('map_beacon');

Route::get('view_media/{id}',[App\Http\Controllers\SectionController::class, 'view_media'])->name('view_media');
// Route::get('/mapping', [App\Http\Controllers\BeaconController::class, 'index'])->name('mapping');

Route::get('/feedback-comments', [App\Http\Controllers\FeedbackCommentController::class, 'index'])->name('feedbackComments');
Route::get('/feedbacks', [App\Http\Controllers\FeedbackController::class, 'index'])->name('feedbacks');
Route::get('/feedback_list', [App\Http\Controllers\FeedbackController::class, 'feedbackList'])->name('feedback_list');
Route::get('feedback_visitor_list/{feedback_recent_id}', [App\Http\Controllers\FeedbackController::class, 'feedbackVisitorList'])->name('feedback_visitor_list');
//ajax request
Route::post('/get_data', [App\Http\Controllers\SectionController::class, 'get_data'])->name('get_data');

Route::get('/privacy_policy', [App\Http\Controllers\HomeController::class, 'privacyPolicy'])->name('privacy_policy');
});

require __DIR__.'/auth.php';
