<?php
use App\Http\Controllers\ServiceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

    Route::post('get_city', [App\Http\Controllers\ServiceController::class, 'get_city']);
    Route::get('state', [App\Http\Controllers\ServiceController::class, 'state']);


	Route::post('/user_registration', [App\Http\Controllers\ServiceController::class, 'user_registration']);
	Route::post('/login', [App\Http\Controllers\ServiceController::class, 'login']);
	Route::post('/logout', [App\Http\Controllers\ServiceController::class, 'logout']);
	Route::post('/social_login', [App\Http\Controllers\ServiceController::class, 'social_login']);
	Route::post('/forgot_password', [App\Http\Controllers\ServiceController::class, 'forgot_password']);
	Route::post('/mobile_verification', [App\Http\Controllers\ServiceController::class, 'mobile_verification']);
	Route::post('verified', [App\Http\Controllers\ServiceController::class, 'verified']);
	Route::post('/change_password', [App\Http\Controllers\ServiceController::class, 'change_password']);
	Route::post('/update_profile', [App\Http\Controllers\ServiceController::class, 'update_profile']);
	Route::post('/delete_account', [App\Http\Controllers\ServiceController::class, 'delete_account']);
	Route::post('/contact_us', [App\Http\Controllers\ServiceController::class, 'contact_us']);

	Route::post('/animals_list', [App\Http\Controllers\ServiceController::class, 'animals_list']);
	Route::post('/beacon_list', [App\Http\Controllers\ServiceController::class, 'beacon_list']);
	Route::post('/user_feedback', [App\Http\Controllers\ServiceController::class, 'user_feedback']);
	Route::post('/get_beacons_section', [App\Http\Controllers\ServiceController::class, 'get_beacons_section']);
